import React, { Component } from 'react';
import HeaderTitle from '../components/HeaderTitle';
import { Layout, Row, Col } from 'antd';
import NotificationCard from '../components/user/notifications/NotificationCard';
import { getUserNotifications } from '../demo-data/fake-apis/notifications.api';

export default class NotificationLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            notifications: ['', '', '', ''],
            loading: true
        };
    }
    //need to nest the 'other'  in componentDidMount
    // map undefined, happening in api? or here? How to test where it is occurring

    //Need to mount the api requests in the components that will use / call them
    componentDidMount() {
        getUserNotifications(this.props.id ? this.props.id : 1).then(
            notifications => {
                console.log('loaded');
                this.setState({
                    notifications: notifications[0],
                    loading: false
                });
            }
        );
    }

    renderNotifications() {
        console.log('important-notif', this.state);
        return this.state.notifications.important.map((notif, index) => {
            return (
                <NotificationCard
                    key={index}
                    title={notif.title}
                    notificationText={notif.notificationText}
                    seen={notif.seen}
                    importance={notif.importance}
                    loading={this.state.loading}
                />
            );
        });
    }
    renderOtherNotifications() {
        console.log('other-notif', this.state);

        return this.state.notifications.other.map((notif, index) => {
            return (
                <NotificationCard
                    key={index}
                    title={notif.title}
                    notificationText={notif.notificationText}
                    seen={notif.seen}
                    importance={notif.importance}
                    other={true}
                    actionData={notif.triggerAction.actionData}
                    loading={this.state.loading}
                />
            );
        });
    }

    render() {
        const notificationContainer = {
            height: `calc(100vh - 150px)`,
            overflow: 'scroll'
        };
        return (
            <React.Fragment>
                <HeaderTitle title="NOTIFICATIONS" marginTop="30px" />
                <Layout
                    style={{
                        minHeight: `calc(100vh - 86px)`,
                        justifyContent: '',
                        padding: '0px 40px'
                    }}
                >
                    <Row gutter={30}>
                        <Col span={12}>
                            <h2>IMPORTANT NOTIFICATIONS</h2>
                            <div style={notificationContainer}>
                                {!this.state.loading ? (
                                    this.renderNotifications()
                                ) : (
                                        <NotificationCard
                                            title="Loading"
                                            loading={this.state.loading}
                                        />

                                    )}
                            </div>
                        </Col>
                        <Col span={12}>
                            <h2>OTHER NOTIFICATIONS</h2>
                            <div style={notificationContainer}>
                                {!this.state.loading ? (
                                    this.renderOtherNotifications()
                                ) : (
                                        <NotificationCard
                                            title="Loading"
                                            loading={this.state.loading}
                                        />
                                    )}
                            </div>
                        </Col>
                    </Row>
                </Layout>
            </React.Fragment>
        );
    }
}
