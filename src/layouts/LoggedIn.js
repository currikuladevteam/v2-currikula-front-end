import React, { Component } from 'react';
import { Layout } from 'antd';

import Navbar from '../components/Navbar';


const {
  Sider
} = Layout;


class LoggedIn extends Component {
  render() {
    return (
        <Layout style={{ minHeight: '100vh' }}>
          <Sider collapsed theme="light" >
            <Navbar />
          </Sider>
          <Layout style={{ minWidth: '1000px', overflow: 'scroll' }}>
            {this.props.children}
          </Layout>
        </Layout>
    );
  }
}

export default LoggedIn;
