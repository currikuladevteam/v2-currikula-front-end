import React, { Component } from 'react';
import { Row, Col, Layout, Card } from 'antd';
import ReferralInformationCard from '../components/user/referrals/ReferralInformationCard';
import HeaderTitle from '../components/HeaderTitle';
import ReferralProgressCard from '../components/user/referrals/ReferralProgressCard';
import ReferralCode from '../components/user/referrals/ReferralCode';
import ReferralRewards from '../components/user/referrals/ReferralRewards';

class ReferralLayout extends Component {
    render() {
        return (
            <React.Fragment>
                <HeaderTitle title="REFERRALS" marginTop="30px"/>
                <div id="referralLayout"></div>
                <Layout style={{ minHeight: `calc(100vh - 86px)`, justifyContent: 'center' }}>
                    <Row type="flex" justify="center">
                        <Col span={20} >
                            <Card style={{ padding: 40, marginBottom: 100 }}>
                                <ReferralInformationCard />
                                <ReferralCode />
                                <ReferralProgressCard />
                                <Row style={{ marginTop: 40 }}>
                                    <ReferralRewards />
                                </Row>
                            </Card>
                        </Col>
                    </Row>
                </Layout>
            </React.Fragment>

        );
    }
}

export default ReferralLayout;
