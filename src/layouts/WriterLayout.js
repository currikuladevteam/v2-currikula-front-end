import React, { Component } from "react";
import { Layout, Spin, notification } from "antd";
// import ReactDOM from 'react-dom'

import { getNote } from "../demo-data/fake-apis/notes.api";
// import { getBibliography } from "../demo-data/fake-apis/bibliographies.api";
import { getDraft } from "../demo-data/fake-apis/drafts.api";
import { getProject } from "../demo-data/fake-apis/projects.api";
import { withRouter } from 'react-router-dom';  

import { Value } from "slate";
import SidebarContainer from "../components/writer/sidebar/SideBarContainer";
import CurrikulaEditor from "../components/writer/editor/CurrikulaEditor";

import EssayInJson from "../components/writer/examples/initialExample.json";

function requireCorrectFiles(props) {
  switch (props.type) {
    case "draft":
      return getDraft(props.id);
    case "bibliography":
      return new Promise((resolve => {resolve([{document: { value: EssayInJson}}])}));
    case "note":
      return getNote(props.id);
    default:
      return null;
  }
}

class WriterLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // essay: essay,
      font: "Times New Roman",
      lineHeight: "1.5",
      fontSize: "0.9rem",
      referencingStyle: null
    };
  }

  componentDidMount() {
    requireCorrectFiles(this.props).then(file => {
      file = file[0]
      if (file.project_id) {
        getProject(file.project_id).then(project => {
          this.setState({
            project: project[0],
            file: Value.fromJSON(file.document.value),
            font: file.document.font,
            lineHeight: file.document.line_height,
            fontSize: file.document.font_size,
            referencingStyle: file.document.type === "draft" ? project[0].referencing_style : null
          })
        })
      } else if (file) {
        this.setState({
          project: null,
          file: Value.fromJSON(file.document.value),
          font: file.document.font,
          lineHeight: file.document.line_height,
          fontSize: file.document.font_size
        })
      } 
    }).catch((error) => {
      // This need to be abstracted into a functional component that also makes and API call to log the action
      // the API call to log the action can be handled by yet another file
      // It should also update the users state with the notification and push that change to the notification DB for that user
      notification['error']({
        message: 'File does not exist',
        description: 'You tried to access a file that does not exist or is no longer available to you.',
        placement: 'bottomLeft'
      });
        this.props.history.push('/dashboard');
    })
  }

  changeFontFamily = font => {
    this.setState({
      font
    });
  };
  changeFontSize = fontSize => {
    this.setState({
      fontSize
    });
  };
  changeLineHeight = lineHeight => {
    this.setState({
      lineHeight
    });
  };
  changeReferencingStyle = referencingStyle => {
    this.setState({
      referencingStyle
    });
  };

  // To bypass the loading of components being faster than each other, there is a call back to this function in the editor that fires and sets the state of the editor into this state allowing us to make reference to it in the side bar.
  getEditor = editor => {
    this.setState({
      referencedEditor: editor
    });
    return editor;
  };

  render() {
    const writerStyle = {
      height: 1200,
      width: 800,
      margin: "0 auto",
      marginBottom: 50,
      padding: 50
    };

    const loadingStyle = {
      display: 'flex',
      height: '100vh',
      flexDirection: 'column',
      justifyContent: 'center',
      textAlign: 'center'
    }
    return (
      <Layout style={{ height: "100vh" }}>
        <Layout
          style={{
            minWidth: "700px",
            overflow: "scroll",
            backgroundColor: "#fbfafa",
            padding: "60px 20px"
          }}
          id="scrollbar"
        >
          <div style={this.state.file ? writerStyle: loadingStyle}>
          {this.state.file ? 
          <CurrikulaEditor 
              editorValue={this.state.file}
              getEditor={this.getEditor}
              ref={editor => (this.editor = editor)}
              font={this.state.font}
              lineHeight={this.state.lineHeight}
              fontSize={this.state.fontSize}
            /> : <Spin size="large" tip="Loading document..."/>}
            
          </div>
        </Layout>
        <SidebarContainer
          // Needs a type here to represent the document that is being edited (note, draft, bibliography etc)
          project={this.state.project}
          editor={this.state.referencedEditor}
          changeFontFamily={this.changeFontFamily}
          changeLineHeight={this.changeLineHeight}
          changeFontSize={this.changeFontSize}
          changeReferencingStyle={this.changeReferencingStyle}
          referencingStyle={this.state.referencingStyle}
          font={this.state.font}
          lineHeight={this.state.lineHeight}
          fontSize={this.state.fontSize}
        />
      </Layout>
    );
  }
}

export default withRouter(WriterLayout);
