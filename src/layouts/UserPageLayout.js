import React, { Component } from 'react';
import { Layout } from 'antd';
import UserSidebar from '../components/user/UserSidebar';

const { Sider } = Layout;

class UserPage extends Component {
  // children() {
  //   return React.Children.map(this.props.children, (child, index) => {
  //     const newChild = React.cloneElement(child, {user: User})
  //     return newChild;
  //   })
  // }

  render() {

    return (
        <Layout >
          <Sider>
            <UserSidebar />
          </Sider>
          <Layout style={{ minWidth: '800px', overflow: 'scroll' }}>
            {/* {this.children()} */}
            {this.props.children}
          </Layout>
        </Layout>

    );
  }
}

export default UserPage; 
