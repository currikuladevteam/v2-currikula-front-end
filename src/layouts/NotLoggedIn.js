import React, { Component } from 'react';
import { Layout } from 'antd';

// import AuthorisePortal from '../pages/AuthorisePortal';
import Footer from '../components/general/footer/Footer';
// import ForgotPassword from '../pages/ForgotPassword';


class NotLoggedIn extends Component {
  constructor(props) {
    super(props) 
    this.state = {
      showFooter: true,
      openTutorial: false,
    }

  }
    children() {
    return React.Children.map(this.props.children, (child, index) => {
      const newChild = React.cloneElement(child, {
        toggleFooter: this.toggleFooter,
        showFooter: this.state.showFooter,
        openTutorial: this.openTutorial,
        showTutorial: this.state.openTutorial})
      return newChild;
    })
  }

  toggleFooter = () => {
    this.setState({
      showFooter: false
    })
  }
  openTutorial = e => {
    console.log('Showing Tutorial')
		this.setState({
			openTutorial: true
		})
	};

  render() {
    return (
      <Layout>
        <Layout style={{ minHeight: '100vh' }}>
          {this.children()}

        </Layout>
        {this.state.showFooter ? <Footer /> : ''}
      </Layout>

    );
  }
}

export default NotLoggedIn;
