import React, { Component } from 'react';
import { Layout } from 'antd';

import SettingsSidebar from '../components/settings/SettingsSidebar';
import SettingsMainPage from '../components/settings/SettingsMainPage';


// import Settings from '../demo-data/settings-data/settings.json';

const { Sider } = Layout;



class SettingsPage extends Component {
  render() {
    return (
      <Layout style={{ height: '100vh' }}>
        <Sider>
          <SettingsSidebar /> 
        </Sider>
        <Layout style={{ minWidth: '800px', overflow: 'scroll' }}>
            <SettingsMainPage  />
        </Layout>
      </Layout>
    );
  }
}

export default SettingsPage; 