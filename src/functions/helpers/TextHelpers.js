import React from 'react'

export function truncate(words, length){
  if (typeof length === 'string') {
    const numLength = parseInt(length);
    if (words.split('').length < numLength) {
      return words;
    } else {
      return words.substring(0, numLength) + '...';
    }
  }
};

export function upperCase(str) {
  return str.toUpperCase();
}

export function unSnakeCase(snake) {
  return snake.replace(/_/g, ' ').replace(/\W[a-z]/g, upperCase).replace(/^[a-z]/g, upperCase)
}

export function processEmphasis(string) {
  const sections = string.split(/<b>|<\/b>/);
  return sections.map((string,index) => {
      if (index % 2 !== 0) {
          return <b key={index}>{string}</b>
      } else {
          return <span key={index}>{string}</span> 
      }
  })
}