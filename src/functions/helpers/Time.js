import React from 'react';

export function getFutureDate(timeStamp) {
    timeStamp = new Date(timeStamp)
    const month = timeStamp.toDateString().match(/ [a-zA-Z]*/)[0].replace(" ", "");
    const day = timeStamp.getDate();
    return day + ' ' + month;
}

// Function to find out the percentage of days that have gone since they started the project, and till it is due.
export function calculateDaysLeftPercentage(dueDate, startDate) {
    // Value of one day in seconds
    const oneDay = 24 * 60 * 60 * 1000;

    // The timestamp of the due date
    dueDate = new Date(dueDate);

    // What is the current time
    const currentTime = new Date().getTime();

    // What is the start date, this will be provided by the .createAT attribue
    startDate = new Date('January 01, 2019 03:24:00');

    // What is the difference in days from the startdate and the due date, i.e. total number of days the student has to complete the project
    const diffDays = Math.round(Math.abs((startDate.getTime() - dueDate.getTime()) / (oneDay)));

    // What is the time left (in days) from now till the project is due
    const diffDaysFromToday = Math.round(Math.abs((currentTime - dueDate.getTime()) / (oneDay)));

    // What the percentage of the days that are left out of the total is
    const percentOfDaysUsed = parseInt(diffDaysFromToday / diffDays * 100)

    // Inversed the percentage so that it shows what has gone by already rather than remaining
    return 100 - percentOfDaysUsed;
}

export function timeTill(dueDate) {
    const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    // dueDate = new Date(dueDate);
    const currentTime = new Date().getTime();
    const diffDaysFromToday = Math.round((dueDate - currentTime) / (oneDay));
    // const diffDaysFromToday = Math.round(Math.abs((currentTime - dueDate) / (oneDay)));
    if (diffDaysFromToday > 1) {
       return `Essay is due in ${diffDaysFromToday} days`
    } else if (diffDaysFromToday === 0) {
        return 'Today!'
    } else if (diffDaysFromToday < 0) {
        return `Essay was due ${Math.abs(diffDaysFromToday)} days ago`
    } 
    else {
        return `Essay is due in ${diffDaysFromToday} day`
    }
}

export function timeSince(timeStamp) {
    timeStamp = new Date(timeStamp)
    // console.log('TimeStamp: ', timeStamp)

    var now = new Date(),
        secondsPast = (now.getTime() - timeStamp.getTime()) / 1000;
    if (secondsPast < 60) {
        return (<span><b>{parseInt(secondsPast)}</b>s</span>);
    }
    if (secondsPast < 3600) {
        return (<span><b>{parseInt(secondsPast / 60)}</b>m</span>);
    }
    if (secondsPast <= 86400) {
        return (<span><b>{parseInt(secondsPast / 3600)}</b>h</span>);
    }
    if (secondsPast > 86400) {
        const day = timeStamp.getDate();
        const month = timeStamp.toDateString().match(/ [a-zA-Z]*/)[0].replace(" ", "");
        const year = timeStamp.getFullYear() === now.getFullYear() ? "" : " " + timeStamp.getFullYear();
        return day + " " + month + year;
    }

}

export function convertMsToHours(ms) {
    return Math.floor(ms / 60 / 60)

}