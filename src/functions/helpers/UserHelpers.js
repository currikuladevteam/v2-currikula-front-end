export function getYearString(year) {
	
  if (isNaN(year)) {
    return "year not found";
  } else {
    switch (year) {
      case 1:
        year = "First Year";
        break;
      case 2:
        year = "Second Year";
        break;
      case 3:
        year = "Third Year";
        break;
      case 4:
        year = "Fourth Year";
        break;
      case 5:
        year = "Fifth Year";
        break;
      case 6:
        year = "Sixth Year";
        break;
      case 7:
        year = "Seventh Year";
        break;
      case 8:
        year = "Eighth Year";
        break;
      case 9:
        year = "Ninth Year";
        break;
      case 10:
        year = "Tenth Year";
        break;
      default:
        return "";
	}
	return year;
  }

}
