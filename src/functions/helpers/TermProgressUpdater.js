export default function TermProgressUpdater() {

    const termOneDays = [256, 347];
    const termTwoDays = [6, 89];
    const termThreeDays = [117, 170];
    const termLengths = [91, 83, 53, null];

    const now = new Date();
    const currentMonth = now.toDateString().match(/ [a-zA-Z]*/)[0].replace(" ", "");

    // Start of the year  date object
    const start = new Date(now.getFullYear(), 0, 0);
    const diff = now - start;
    const oneDay = 1000 * 60 * 60 * 24;
    
    // Days since the start of the year
    const day = Math.floor(diff / oneDay);

    if (day > termOneDays[0] && day < termOneDays[1]) {
        const percent = Math.ceil((day-termOneDays[0]) / termLengths[0] * 100);
        return [percent, 0, 0];
    } else if (day > termTwoDays[0] && day < termTwoDays[1]) {
        const percent = Math.ceil((day-termTwoDays[0]) / termLengths[1]  * 100)
        return [100, percent, 0];
    } if (day > termThreeDays[0] && day < termThreeDays[1]) {
        const percent = Math.ceil((day-termThreeDays[0]) / termLengths[2]  * 100)
        return [100, 100, percent];
    } if (currentMonth === 'Sep') {
        return [0, 0, 0]
    } else if (currentMonth === 'Dec' || currentMonth === 'Jan') {
        return [100, 0, 0]
    } else if (currentMonth === 'Apr') {
        return [100, 100, 0]
    } else if (currentMonth === 'Jun' || currentMonth ===  'Jul' ||  currentMonth === 'Aug') {
        return [100, 100, 100]
    }
}


// const monthLengths = {
//     'Jan': 31,
//     'Feb': 28,
//     'Mar': 31,
//     'Apr': 30,
//     'May': 31,
//     'June': 30,
//     'July': 31,
//     'Aug': 31,
//     'Sep': 30,
//     'Oct': 31,
//     'Nov': 30,
//     'Dec': 31
// }

//     function percentageThroughTerm(term) {


//         if (term === 0) {
//             const daysSince = daysSince(now, '')
//             return ['s', 0, 0]
//         } else if (term === 1) {
//             return [100, 's', 0]
//         } else if (term === 2) {
//             return [100, 100, 's']
//         } else {
//             return [100, 100, 100];
//         }
//     }
// }


    // If it is a summer month return full bars
    // if (!summerMonths.indexOf(currentMonth)) {
    //     firstTermProgress = 100;
    //     secondTermProgress = 100;
    //     thirdTermProgress = 100;
    // } else if (!firstTermMonths.indexOf(currentMonth)) {
    //     secondTermProgress = 0;
    //     thirdTermProgress = 0;
    //     if (currentMonth === 'Sep' && currentDay < 14) {
    //         firstTermProgress = 0;
    //     } else if (currentMonth === 'Dec' && currentDay > 14) {
    //         firstTermProgress = 100;
    //     } else {
    //         firstTermProgress = calculateTermPercentage(now);
    //     }
    // } else if (!secondTermMonths.indexOf(currentMonth)) {
    //     firstTermProgress = 100;
    //     thirdTermProgress = 0;
    //     if (currentMonth === 'Jan' && currentDay < 7) {
    //         secondTermProgress = 0;
    //     } else {
    //         secondTermProgress = calculateTermPercentage(now);
    //     }
    // } else if (!thirdTermMonths.indexOf(currentMonth)) {
    //     firstTermProgress = 100;
    //     secondTermProgress = 100;
    //     if (currentMonth === 'April' && currentDay < 28) {
    //         thirdTermProgress = 0;
    //     } else if (currentMonth === 'Jun' && currentDay > 20) {
    //         thirdTermProgress = 100;
    //     } else {
    //         // thirdTermProgress = calculateTermPercentage(now);
    //     }
    //     // If Month is [Sep, Oct, Nov, Dec] -> Sept 14 - Dec 14
    //     // If Month is [Jan, Feb, March] -> Jan 7 - March 31 
    //     // If Month is [April, May, June] -> April 28 - Jun 20
    //     // If Month is [July, August]  -> Year over full bars? 
    //     return {
    //         firstTermProgress,
    //         secondTermProgress,
    //         thirdTermProgress
    //     }
    // }

    // function activeTerm(currentMonth, currentDay) {
    //     // const summerTerm = [100, 100, 100];
    //     // const firstTerm = [calculateTermPercentage(currentDay), 0, 0 ];
    //     // const secondTerm = [100, calculateTermPercentage(currentDay), 0 ];
    //     // const thirdTerm = [100, 100, calculateTermPercentage(currentDay) ];

    //     const termDate = {
    //         'Jan': 1,
    //         'Feb': 1,
    //         'Mar': 1,
    //         'Apr': 2,
    //         'May': 2,
    //         'June': 2,
    //         'July': 3,
    //         'Aug': 3,
    //         'Sep': 0,
    //         'Oct': 0,
    //         'Nov': 0,
    //         'Dec': 0
    //     }
    //     return percentageThroughTerm(termDate[currentMonth])
    // }


    // function calculateTermPercentage(monthsArray, currentMonth, currentDay, termDates) {
    //     if (!monthsArray.indexOf(currentMonth)) {
    //         if (currentDay < termDates[0]) {
    //             return 0
    //         } else if (currentDay < termDates[1]) {
    //             return 100
    //         } else {
    //             return 'This month Percentage goes here'
    //         }
    //     }
    // }