export default function PercentageColourPicker(percentage) {
    if (percentage < 0) {
        return '76BD63';
    }
    const colours = {
        "100": '76BD63',
        "95": '8BBF52',
        "90": '94BF49',
        "85": 'A2C03D',
        "80": 'AEC133',
        "75": 'BEC326',
        "70": 'C8C31D',
        "65": 'E3D306',
        "60": 'E4C505',
        "55": 'E5BB04',
        "50": 'E4B103',
        "45": 'E5A803',
        "40": 'E59D01',
        "35": 'E68F00',
        "30": 'D97700',
        "25": 'CE6400',
        "20": 'C14B00',
        "15": 'B73800',
        "10": 'AA2000',
        "5": '990000',
        "0": '8A8A8A',
    }

    percentage = percentage % 5 < 3 ? (percentage % 5 === 0 ? percentage : Math.floor(percentage / 5) * 5) : Math.ceil(percentage / 5) * 5

    if (percentage > 100) {
        return '76BD63';
    }
   

    percentage = percentage.toString();


    return colours[percentage]
}

//function round(x) { return x%5<3 ? (x%5===0 ? x : Math.floor(x/5)*5) : Math.ceil(x/5)*5 }
