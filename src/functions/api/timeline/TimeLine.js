const TimeLine = {

    createEvent: (log) => {
        if (log.event && log.project_id) {
            console.log('Adding TimeLine: Sending Data to project & creating timeline event')
            // Edit the project data and send PUT request in order to add timeline event
            // This can be created as a specific endpoint /project/addTimeLineEvent (for example)
            // Then edit the local cache
            return true
        } else {
            return ''
        }
    }


}

export default TimeLine;
