export default function Logging() {
    return {
        // id: '#32443mk24n32klnr23',
        type: {
            notification: true,
            timeline: false,
        },
        session_id: '#qqed2ed34wd',
        project_id: '#214sada12423',
        user_id: '#1234423dscdf3',
        event: {
            type: 'create', // create, edit, metadata edit, delete, invoke, view, reminder
            action: 'text about the action', //  Create new note
            content: {

            },
            url: '/dashboard',
            time_stamp: '0919129313',
        }
    }
}

const creating = {
    title: null,
    type: null,
    item_id: null,
    project_title: null,
    project_id: null
}

const editing = {
    title: null,
    type: null,
    item_id: null,
    project_title: null,
    project_id: null
}

const metadata_editing = {
    type: null,
    old_information: null,
    new_information: null
}

const deleting = {
    title: null,
    type: null,
    item_id: null,
    project_title: null,
    project_id: null
}

const invoking = {
    title: null,
    type: null,
    item_id: null,
    project_title: null,
    project_id: null
}
const view = {
    title: null
}
const reminder = {
    title: null,
    type: null,
    item_id: null,
    project_title: null,
    time: null
}


// Perhaps the create, edit, delete, invoke and reminder types all have set content objects. 
// Create
//      - Title
//      - Type
//      - Item Id
//      - Project Title
//      - Project_Id
// "You created 'Lecture Note 3' in the project 'Time in Physics'"
// Edit
//      - Title
//      - Type
//      - Item Id
//      - Project Title
//      - Project_Id
// "You edited 'Lecture Note 3' in the project 'Time in Physics'"
// MetaData Edit
//      - Type (name, title, due  date etc)
//      - Old information
//      - New information
// "You changed the title of your project from Kings of Lion, to Death and Taxes"
// "You updated your name from Sean to Sam."
// Delete
//      - Title
//      - Type
//      - Item Id
//      - Project Title
//      - Project_Id
// Invoke (A function has fired that affects the user)
//   Smart Source has started, improvements are running. 
//      - Type (smart source, improvements etc)
//      - Project Title
//      - Project_Id
// View (What page they viewed)
//      - Title
// Reminder (what a function has reminded them about)
//  "Essay is due in X days, You've got 400 words to go on your essay."
//      - Type (What is the reminder about [due date, progress, study session])
//      - Project Id
//      - Item Id
//      - Time (the time till due)
//
