import Notify from '../notifications/Notifications';

class Log {
    constructor(log) {
        // console.log(log)
        // Destructuring object sent to log
        const { event, error, type, user } = log;
        // console.log(user)
        this.type = type;
        this.error = error;
        this.user_id = user.id;
        this.session_id = user.session_id; // comes from Context
        this.project_id = event.document.project_id;
        this.event = createEvent(event);

        
    }

    getEvent = () => {
        return this.event;
    }

    log = () => {
        // const test = LogContext();
        if (this.event) {
            console.log('Sending Data to session & creating notification', this)
            return this
        } else {
            return ''
        }
    }

    notify = (notification) => {
        return Notify(notification, this)
    }
}



function createEvent(event) {
    if (!event) {
        console.log('No event sent', event)
        return ""
    }
    return {
        message: event.message ? event.message : null, // create, edit, delete
        type: event.type, // create, edit,  metadata, delete
        document: { // create, edit, delete
            type: event.document.type, // create, edit, metadata, delete
            id: event.document.id, // create, edit, delete
            project_id: event.document.project_id, //create, edit, delete
            project_title: event.document.project_title,
            other: {
                ...event.document
            } // create, edit, delete
        },
        change: event.newInformation && event.oldInformation ? {
            oldInformation: event.oldInformation, // metadata
            newInformation: event.newInformation // metadata
        } : null,
        function: event.function ? event.function : null,
        page: event.page ? event.page : null,
        time_stamp: new Date().getTime() // create, edit, metadata, delete

    }
}


//   const event = {
//     type: 'delete',
//     message: `You deleted the ${document.type} - ${doc.title}`,
//     document: {
//       ... doc.document,
//       ... project.title
//     }
//   }

//   const event = {
//     type: 'edit',
//     message: `You edited your ${document.type} - ${doc.title}. ${newInformation - oldInformation > 0 ? "Adding" : "Removing"} ${newInformation - oldInformation} words`,
//     document: {
//       ...doc.document,
//       ...project.title
//     },
//     newInformation:  1400,
//     oldInformation: 1200,
//   }

//   const event = {
//     type: 'view',
//     page: window.location,
//   }

//   const event = {
//     type: 'create',
//     // has the doc type but still needs the title here.
//     message: `You created a ${document.type}`,
//     document: {
//       ...doc.document,
//       ...project.title
//     },
//   }

//   const event = {
//     type: 'invoke', 
//     function: '', // Action (smart source, improvement API )
//     document: {
//       ...doc.document,
//       ...project.title
//     },
//   }

//   const event = {
//     type: 'metadata',
//     message: `You changed ${oldInformation} to ${oldInformation}.`,
//     document: {
//          type: 'type of change'
//       ...doc.document,
//       ...project.title
//     },
//     newInformation: doc.title,
//     oldInformation: doc.title,
//   }


export default Log;