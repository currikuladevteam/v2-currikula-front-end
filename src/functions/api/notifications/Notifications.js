import React from 'react';
import { Icon } from 'antd';


const PlaceIcon = (type, color) => {
    color = color ? color : 'black';
    return <Icon
        type={type}
        style={{ marginTop: 22, marginLeft: 0, color }}
    />
}

const icons = {
    note: 'profile',
    project: 'folder-open',
    bibliography: 'read',
    setting: 'setting'
}


export default function Notify(notification, log) {
    notification.config({
        placement: 'bottomLeft',
        duration: 4
    })
    const style = {
        marginLeft: 65,
        width: '100%',
        maxHeight: 102,
        marginBottom: 0,
        marginTop: 10,
        top: 15
    }

    const { event, error, type } = log;
    if (error) {
        return () => notification.open({
            message: `Error when trying to ${event.type} ${event.document.type}`,
            description: "We cannot complete this request at the moment. Please try again in a little while.",
            icon: PlaceIcon('cross', 'red'),
            style: {
                ...style,
            }
        })
    } else if (type === "delete") {
        return () => notification.open({
            message: "Deleted " + event.document.type,
            description: event.message,
            icon: PlaceIcon(icons[event.document.type], 'red'),
            style: {
                ...style,
            }
        })
    } else if (type === "create") {
        return () => notification.open({
            message: "Created " + event.document.type,
            description: event.message,
            icon: PlaceIcon(icons[event.document.type] || 'plus-circle', 'blue'),
            style: {
                ...style,
            }
        })
    } else if (type === "metadata") {
        return () => notification.open({
            message: "Changed " + event.document.type,
            description: event.message,
            icon: PlaceIcon(event.type, 'grey'),
            style: {
                ...style,
            }
        })
    }

    return () => notification.open({})
}

