import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { BrowserRouter as Router, Switch } from 'react-router-dom';

import LoggedIn from './layouts/LoggedIn';
import NotLoggedIn from './layouts/NotLoggedIn';
import ForgotPassword from './pages/ForgotPassword';
import AuthorisePortal from './pages/AuthorisePortal';

// Provider
import UserProvider from './components/providers/UserProvider'

// Writers
import WriterLayout from './layouts/WriterLayout';
// import BaseWriter from './components/writer/BaseWriter';
// import DraftWriter from './components/writer/DraftWriter';
// import BibliographyWriter from './components/writer/BibliographyWriter';
// import PlanWriter from './components/writer/PlanWriter';
// import NoteWriter from './components/writer/NoteWriter';

// Dashboard & Projects
import Dashboard from './pages/Dashboard';
import Documents from './pages/Documents';
import ProjectOverview from './pages/ProjectOverview';
import Projects from './pages/Projects';

// User
import UserPage from './layouts/UserPageLayout';
import UserProfileLayout from './components/user/profile/UserProfileLayout';
import UniversityStats from './components/user/universityStats/UniversityStatsLayout';
import GradeCalculator from './components/user/gradeCalculator/GradeCalculatorLayout';
import ReferralLayout from './layouts/ReferralLayout';
import Notifications from './layouts/NotificationLayout';

// Settings
import SettingsLayout from './layouts/SettingsLayout';

// Helpers
import PropsRoute from './components/general/PropsRoute'; // Wraps routes with props of their parents
// import LoginCard from './pages/LoginCard';
// import RegisterCard from './pages/RegisterCard';

class App extends Component {

  render() {
    return (
      <Router>
        <Switch>
          <UserProvider>
          {/* <Route exact path="/" component={AuthorisePortal} /> */}
          <Route
            render={props => (
              <React.Fragment>
                <Route
                  render={props => {
                    if (props.location.pathname === "/access" ||
                      props.location.pathname === "/forgot-password" ||
                      props.location.pathname === "/") {
                      return <NotLoggedIn>
                        <PropsRoute
                          component={(props) => (
                            <div>

                              <PropsRoute
                                exact
                                path="/access"
                                component={(props) => <AuthorisePortal toggleFooter={props.toggleFooter} />}
                              />
                              <PropsRoute
                                exact
                                path="/"
                                component={() =><AuthorisePortal 
                                  toggleFooter={props.toggleFooter} 
                                  showFooter={props.showFooter}
                                  openTutorial={props.openTutorial}
                                  showTutorial={props.showTutorial}/>}
                              />
                              <Route
                                exact
                                path="/forgot-password"
                                component={ForgotPassword}
                              />
                            </div>
                          )}
                        />
                      </NotLoggedIn>
                    } else {
                      return <LoggedIn>
                        <Route
                          component={({ match }) => (
                            <div>
                              <Route exact path="/dashboard" component={Dashboard} />
                              <Route exact path="/projects" component={Projects} />
                              <Route
                                path="/projects/:id/overview"
                                render={(props) => <ProjectOverview id={props.match.params.id} />}
                                // component={ProjectOverview}
                              />
                              <Route path="/documents" component={Documents} />
                            </div>
                          )}
                        />
                        <Route
                          path="/user"
                          render={(props) => (
                            // <UserPage>
                            <Route
                              render={(props) => (
                                <UserPage>
                                  <PropsRoute path="/user/profile" component={UserProfileLayout} />
                                  <Route
                                    exact
                                    path="/user/university-stats"
                                    component={UniversityStats}
                                  />
                                  <PropsRoute
                                    exact
                                    path="/user/grade-calculator"
                                    component={GradeCalculator}
                                  />
                                  <PropsRoute path="/user/referrals" exact component={ReferralLayout} />
                                  <Route
                                    exact
                                    path="/user/notifications"
                                    component={Notifications}
                                  />
                                </UserPage>
                              )}
                            />
                            // </UserPage> 
                          )}
                        />
                        <Route path="/settings" component={SettingsLayout} />
                        <Route
                          path="/writer/:id/bibliography"
                          component={(props) => <WriterLayout type={"bibliography"} id={props.match.params.id} />}
                        />
                        <Route
                          path="/writer/:id/draft"
                          component={(props) => <WriterLayout type={"draft"} id={props.match.params.id}  />}
                        />
                        <Route
                          path="/writer/:id/note"
                          component={(props) => <WriterLayout type={"note"} id={props.match.params.id}  />}
                        />
                        <Route
                          path="/writer"
                          render={(props) => (
                            <div>
                              {/* <Route exact path="/writer/*" render={(props)=> ( <div >{console.log(props)}</div>)} /> */}
                            </div>
                          )}
                        />
                        {/* <Redirect to="/dashboard" /> */}

                      </LoggedIn>
                    }
                  }
                  }
                />

              </React.Fragment>
            )}
          />
          </UserProvider>
        </Switch>
      </Router>
    );
  }
}

export default App;