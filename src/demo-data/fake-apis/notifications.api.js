import Notifications from '../notifications'

export function getUserNotifications(user_notification_id) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(Notifications.filter(notification => notification.user_notification_id === user_notification_id ));
        }, Math.floor(Math.random() * 1000));
    })
}



