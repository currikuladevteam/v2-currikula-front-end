import Projects from '../projects-data'

export function getAllProjects() {
    return Projects;
}


export function getProject(id) {
    return new Promise((resolve, reject) => {
        if (isNaN(parseInt(id))) {
            reject({ error: true, msg: "Not a project id." })
        } else {
            setTimeout(() => {
                resolve(Projects.filter(project => project.id === id))
            }, Math.floor(Math.random() * 1000)
            )
        }

    })
}

// returns an array of projects matching the ids sent in an array
export function getProjects(ids) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(Projects.filter(project => ids.indexOf(project.id) > -1))
        }, Math.floor(Math.random() * 1000)
        )
    })
}

export function getProjectTitle(id) {
    return new Promise((resolve) => {
        setTimeout(() => {
            const project = Projects.filter(project => project.id === id)
            resolve(project[0].title)
        }, Math.floor(Math.random() * 500)
        )
    })
}