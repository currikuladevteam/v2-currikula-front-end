import Notes from '../notes-data'

export function getAllNotes() {
    return Notes;
}
export function getProjectNotes(id) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(Notes.filter(note => note.project_id === id));
        }, Math.floor(Math.random() * 1000));
    })
}

export function getNotes(ids, message) {
    if (message) console.log('MSG: ', message) 
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(Notes.filter(note => ids.indexOf(note.id) > -1))
        }, Math.floor(Math.random() * 1000)
        )
    })
}
export function getNote(id) {
    id = typeof id === 'string' ?  parseInt(id) : id
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(Notes.filter(note => note.id === id))
        }, Math.floor(Math.random() * 1000)
        )
    })
}



