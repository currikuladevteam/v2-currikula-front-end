import Bibliographies from "../bibliographies-data";
import Projects from '../projects-data'

export function getBibliography(id) {
  id = typeof id === 'string' ?  parseInt(id) : id
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(Bibliographies.filter(note => note.id === id));
    }, Math.floor(Math.random() * 1000));
  });
}



export function getBibliographies(ids) {
  return new Promise((resolve) => {
      setTimeout(() => {
          resolve(Bibliographies.filter(draft => ids.indexOf(draft.id) > -1))
      }, Math.floor(Math.random() * 1000)
      )
  })
}


// Overly complicated title retriever 
export function getBibliographiesWithTitle(ids) {
  return new Promise((resolve) => {
      setTimeout(() => {
          const bibliographies = Bibliographies.filter(bib => ids.indexOf(bib.id) > -1);
          resolve(bibliographies.map(bib => { 
            if (bib.title) {
              return bib
            } else 
            
            return { ...bib, ...{ title:  Projects.filter(project => project.id === bib.project_id)[0].title || bib.title} } }))
      }, Math.floor(Math.random() * 1000)
      )
  })
}