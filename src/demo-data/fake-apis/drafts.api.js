import Drafts from '../drafts-data';
import Projects from '../projects-data';

export function getDraft(id) {
    id = typeof id === 'string' ? parseInt(id) : id
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(Drafts.filter(draft => draft.id === id))
        }, Math.floor(Math.random() * 1000)
        )
    })
}


export function getDrafts(ids) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(Drafts.filter(draft => ids.indexOf(draft.id) > -1))
        }, Math.floor(Math.random() * 1000)
        )
    })
}

// Overly complicated title retriever 
export function getDraftsWithTitle(ids) {
    return new Promise((resolve) => {
        setTimeout(() => {
            const drafts = Drafts.filter(draft => ids.indexOf(draft.id) > -1);
            resolve(drafts.map(draft => {return { ...draft, ...{ title:  Projects.filter(project => project.id === draft.project_id)[0].title} } }))
        }, Math.floor(Math.random() * 1000)
        )
    })
}
