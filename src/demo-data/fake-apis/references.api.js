import References from "../references-data";

export function getReference(id) {
  id = typeof id === 'string' ?  parseInt(id) : id
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(References.filter(note => note.id === id));
    }, Math.floor(Math.random() * 1000));
  });
}



export function getReferences(ids) {
  return new Promise((resolve) => {
      setTimeout(() => {
          resolve(References.filter(draft => ids.indexOf(draft.id) > -1))
      }, Math.floor(Math.random() * 1000)
      )
  })
}
