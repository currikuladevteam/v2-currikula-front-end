import project1 from './project.1.json';
import project2 from './project.2.json';
import project3 from './project.3.json';
import project4 from './project.4.json';



export default [
    project1,
    project2,
    project3,
    project4
]