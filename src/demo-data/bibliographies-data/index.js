import bibliography1 from "./bibliography.1.json";
import bibliography2 from "./bibliography.2.json";
import bibliography3 from "./bibliography.3.json";
import bibliography4 from "./bibliography.4.json";
import bibliography5 from "./bibliography.5.json";

export default [bibliography1, bibliography2, bibliography3, bibliography4, bibliography5];
