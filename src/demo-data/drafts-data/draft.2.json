{
    "project_id": 2,
    "user_id": 1,
    "id": 2,
    "created_at": 123123123123,
    "edited_at": 12312312312312,
    "project_completed": false,
    "document": {
        "font": "Arial",
        "line_height": "1.5",
        "font_size": "0.9rem",
        "type": "draft",
        "value": {
            "object": "value",
            "document": {
                "object": "document",
                "data": {},
                "nodes": [
                    {
                        "object": "block",
                        "type": "title",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "Analysis of Gordon Brown’s Leadership",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "h1",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "Introduction",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "Upon becoming Prime Minister in June 2007, Gordon Brown gave strong indications that he would change the political culture in Britain. Among those promises, he pledged to govern with a “government of all the talents”, seeking consultation and advice from outside the traditional Labour Party sources. (BBC, 2007) Since then, the Prime Minister has also stated his preference for a more cabinet form of Government as opposed to a strong Prime Ministerial Government common in the Blair and Thatcher periods. (Rentoul, p552) Does Gordon Brown’s record show a preference for consultation, has Parliament been given more power in the running of Government or has Gordon Brown reverted to the strong autocratic style of leadership that he was often accused of during his time as Chancellor?",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "This essay will examine Gordon Brown’s short reign as Prime Minister, looking closely at his use of Prime Ministerial powers in Government. The work will explore the extent to which the present Labour Cabinet has strong influence over decisions and policies, and whether Gordon Brown has strengthened Parliament by transferring some of the traditional Prime Ministerial powers to the legislature. In order to judge Gordon Brown’s record, it is necessary to compare the actions and records of the previous three UK Prime Ministers, Tony Blair, John Major and Margaret Thatcher. Each Prime Minister differed in their use of Prime Ministerial powers, influenced by the political situation and their own style and personality. By giving strong examples of cabinet or Prime Ministerial Government we may better understand their meaning, as well as better judging Gordon Brown’s record and style of Government. Finally we shall attempt to conclude whether Gordon Brown has brought about a more Cabinet form of Government, and if so, judge if this has been a positive or negative development for Government and the Country.",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "h1",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "Chapter One",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "In Gordon Brown’s first address to the House of Commons as Prime Minister, he indicated that his Premiership was one that would increase the power of Parliament in such vital areas as declaring war, signing International treaties and approving public appointments. This was part of his plan to devolve power, which included proposals to start de-centralising power across the UK, allowing UK citizens and councils more influence in Government. The Prime Minister has in many ways delivered on some of his promises to rule with a “government of all the talents” appointing Labour outsiders such as Sir Digby Jones and setting up the National Economic Council. (Bagehot, 2008) These actions however, do not necessarily mean that Gordon Brown has brought in a new period of cabinet Government however. Cabinet Government refers to the UK Prime Minister ruling in conjunction with his or her Cabinet, sharing responsibility and power but remaining first among equals. Prime Ministerial Government refers to the Prime Minister dominating the executive, making all the major policy decisions without always consulting with or taking the advice of the Cabinet. (Jones, 2005, p.27) Does Gordon Brown’s record suggest that he rules in co-operation with his Cabinet or does he make the major decisions without their contribution?",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "The first few months of the Brown era were a relative success for the Government. The change of leader helped Labour recover public support, and Gordon Brown gave a successful image as a strong and experienced leader. Policies were put forward to restore Cabinet Government, the party was united and the PM appears to have consulted the Cabinet whilst remaining firmly in control. This was a change from the Blair period of Government, in which a strong Prime Ministerial form of Leadership existed, with outside unelected advisors exercising more power and influence on the PM than the elected Cabinet members. (Rentoul, 2001, p.536) Although the strong leadership of Blair and the unity of the party had been in many ways responsible for their electoral success, Blair’s weakening of Cabinet power and autocratic leadership led Britain into an unpopular and, for the Labour parties’ popularity, disastrous war in Iraq.",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "Gordon Brown entered office with the promise of restoring Cabinets importance, and lessening the influence of outsiders such as Alistair Campbell, who during the Blair years appeared to be the real deputy Leader, rather than the elected John Prescott. (Stephens, 2004, p.180) Brown then, seemed to be promoting a style of Government personified by John Major. During Majors time as PM Cabinet meetings were transformed from meetings where Mrs Thatcher would inform the cabinet of her policies and demand obedience into genuine forums for debate and deliberation. Important issues were discussed, free debate was encouraged and arguments were common. This was extremely popular within Government, as suggested in Dick Leonards “A Century of Premiers”. “Major has restored Cabinet Government” (Leonard, 2005, p.333)",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "In the first few months, Gordon Brown did successfully lead in a Cabinet style of Government. The aftermath and response to several failed terrorist attacks were handled by Cabinet figures such as Jacqui Smith, who liaised with the public and the Muslim Community. In both the crisis with Northern Rock and the flooding in rural areas Brown employed the full talents of his Ministers rather than attempting to micromanage every situation. (Freedland, 2007) Brown was popular with both the party and the public; it seemed that the return of Cabinet Government was beneficial for the Government and the Nation.",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "In many ways Major and Brown inherited the Leadership under similar situations. Neither man had the commanding Parliamentary majority of Thatcher or Blair at their peak, and had to by necessity rely more on the support of their Cabinet. (Foley, 2002, p.33) Like Major, Brown has also had to deal with a resurgent opposition which for the first time in years appears to be ahead in the polls. This led to Brown’s first major crisis; in September and October 2007 Brown hinted that he would call an election to fully legitimise his Leadership, as he did not face a leadership election to become Prime Minister. However Brown decided not to go ahead with an election, with many suggesting that he was worried by the Tories lead in the opinion polls. (Robinson, 2007) From this point on Brown became steadily less popular with the Country and the party, as subsequent crises and Browns apparent lack of charisma and leadership chipping away at support for the Government.",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "Despite this however, many would suggest that this has not caused Gordon Brown to abandon Cabinet Government. On the contrary, Gordon Brown has continued to consult and debate with his Cabinet on the major issues, allowing such figures as David Milliband and Jacqui Smith to emerge as possible future leaders of the Labour Party. Although Gordon Brown has consulted with many outside forces and attempted to build a more consensual form of British politics, even offering Paddy Ashdown a position in Government, (Woodward, 2007) this has not led to the marginalisation of the Cabinet. Margaret Thatcher’s preference for unelected advisors, such as Sir Alan Walters, over Cabinet Members helped bring about her downfall, making her believe she was infallible and isolating potentially loyal Ministers, as suggested by Leonard. “She acted as a virtual dictator, reducing the role of the Cabinet to that of a supporters club”. (Leonard, 2005, p.313) It did however, give Thatcher the image of a strong Leader, one that could make important decisions decisively without having to consult or procrastinate. Unfortunately some of the decisions she made without consulting the Cabinet, such as the poll tax were disastrous for her and the Tory Party.",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "Tony Blair did not marginalise or reduce his Cabinet to such a lowly level; however he did, as we have mentioned, surround himself with an inner circle of advisors, many of them outsiders like Alistair Campbell, and relied more heavily on their advice than that of his Cabinet. His style of leadership was also similar to Thatcher’s in that he tried to dominate and manage every important situation, with the notable exception of those issues that came under his Chancellors office. (Rentoul, 2001, p.249) Although the most successful Labour leader in history, Blair’s decisions and style of leadership were ultimately responsible for his clearly begrudging resignation. Despite following a more Cabinet orientated approach to Government, Gordon Brown has found that this does not necessarily guarantee loyalty during difficult times. There have been widespread rumours about a possible leadership challenge, with many Labour MPs talking to the press about their desire to see Gordon Brown step down. By giving his Cabinet power and responsibility Gordon Brown has allowed some of the bigger personalities, such as Milliband, to grow in status, so much so that many Labour MPs considered him a possible replacement.",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "h1",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "Chapter Two",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "In our previous chapter we presented the argument that Gordon Brown has brought back a more Cabinet orientated form of Government. In Chapter two we will look at ways in which Gordon Brown has in fact maintained a Prime Ministerial form of Leadership, along with the negative consequences this has brought about. We will also attempt to evaluate Gordon Brown’s record, and come to a conclusion whether it has been a positive or negative development in relation to the three previous Prime Ministers reigns.",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "At the beginning of Brown’s premiership, the Prime Minister made pledges to bring about more Parliamentary powers and increase executive accountability, a clear reference to Tony Blair’s decision to launch an unpopular war. (Stephens, 2005, p239) However so far, these pledges have remained in the formation stage. The Premier still retains ultimate authority on issues of national security, international treaties and appointments. Perhaps in the future these pledges will be passed in Parliament, however till that day the Prime Minister will retain a large degree of executive control. Since Brown has been in office little legislation actually transferring power away from the Executive has passed through Parliament.",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "In many respects Brown has pushed through his own agenda, bypassing the reservations or objections of his Cabinet, as well as ruthlessly disposing of any Ministers that have dared to challenge his authority. (Bagehot, 2008) The ten pence tax rate cut was widely criticised by the media and opposition, and eventually forced the Government to backtrack. It was introduced in the 2007 budget by Brown, who insisted on implementing it despite the fact that it hit those on low incomes, natural Labour supporters. This disaster was a direct result of Gordon Brown forcing an unpopular policy into being despite the opposition of many in the Party.",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "Despite the large size of the Cabinet, the Prime Minister has ensured that those in the top jobs are those with a proven track record for loyally supporting Brown throughout his years as Chancellor. Those supporters or suspected supporters of Tony Blair have largely been marginalised, not allowed near the more important Ministerial positions. Those Ministers who have resisted his policies or have spoken for the need of a new leader have been dropped for more loyal, pliant personnel. (Helm, 2008)) In the most important sphere of influence, the economy, the Chancellor Alistair Darling appears to operate with far less independence and power than Brown did under Blair, indeed although we may say that Tony Blair operated a “Presidential” style of Leadership, he went out of his way to appease his Chancellor. Gordon Brown does not have any figure powerful or independent enough to build a sphere of influence within Government as Brown did previously. (Leonard, 2005, p355-358) Regardless of the press and several Labour MPs promotion of David Milliband, Brown remains without peer in terms of gravitas, influence and experience within the party.",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "Despite his smaller majority, Brown has also shown that he is able to force unpopular legislation through Parliamentary and Cabinet opposition, most notably in the case of the 42 day detention issue. The intense opposition in his Party, with alleged deals being made to get the bill passed, along with opposition condemnation suggests that Gordon Brown was not concerned with giving Parliament new powers related to national security. In relation to Iraq although some British troops have been pulled out of the Country, British troops remain in both Iraq and Afghanistan, again despite the opposition of many in the party. That there is no opposition in the Cabinet is perhaps testament to the loyalty of those Gordon Brown has chosen, rather than there being complete unity.",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "h2",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "Developments in the Brown Government: Positive or Negative?",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "In the last Chapter we have demonstrated that the Brown Leadership is not yet a perfect example of Cabinet Government. The Cabinet consists mainly of Brown loyalists, the Prime Minister has forced through many unpopular policies and as of yet has failed to deliver the legislation that would make a Cabinet Government a long term possibility. However despite these criticisms Gordon Brown has run a more Cabinet Government than either Tony Blair or Margaret Thatcher. It will be difficult for the Prime Minister to backtrack on legislation that will give more power to Parliament, and he has demonstrated in many of the crises that he is willing to share power and responsibility with the more senior Cabinet members. The large size and broad range of the Cabinet also suggests that Brown is less likely to rely on a small clique of outsiders for advice, as did Thatcher and Blair. In recent months Brown has also shown that he is willing to accept Blair loyalists into the Cabinet, as we have seen with the appointment of Peter Mandelson. (Sparrow, 2008)",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "It is the opinion of this essay that despite the serious problems of the Labour Party this has been a positive development. Although there has been limited use of the talents of opposition party personnel, a Government of all the talents has the potential to change the confrontational, winner takes all style of British Politics. In a time of severe economic crisis there is a need to take advantage of all available talent and ability. History has shown that although a strong Prime Ministerial Government can be initially popular, if the Leader believes it is unnecessary to consult with their Cabinet then they can make serious errors of judgement. If Margaret Thatcher had listened to her Cabinet perhaps there would have been no poll tax, nor would relations with Europe be so damaged by Thatcher’s strident anti European speeches. (Riddell, 1991, pp.184-187)",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "Ultimately though, a more Cabinet form of Government means a more democratic, accountable Government, one that is more likely to grant Parliament a greater role in the more important aspects of Government. A larger Cabinet made up of representatives from across the board is also more representative of the nation itself. If ideas and policies can be discussed, deliberated and argued out by a professional, representative and broad Cabinet then perhaps there will be less chance of the Government enacting policies that are popular and logical only to the Prime Minister and a close circle of unrepresentative outsiders.",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "h1",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "Conclusion",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "This essay has demonstrated that since taking office Gordon Brown has led a Cabinet Government similar to that of John Major, rather than the more Prime Ministerial approach of both Margaret Thatcher and Tony Blair. The essay has also shown that Brown’s leadership style does still contain elements of the Prime Ministerial form of Government, especially in regards to forcing through unpopular pet projects originating from when Brown was Chancellor. However, despite his reputation as a “Stalinist” leader Brown has shown surprising flexibility, an ability to consult with those outside the traditional Labour party sphere as well as demonstrating trust in the more senior members of his Cabinet.",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "If Brown remains Prime Minister for the foreseeable future then we are likely to see legislation that will strengthen both Cabinet Government and Parliament itself. In the new economic climate and looming recession these policies might herald a new age of not only Cabinet Government, but a more consensual and cross party form of British Politics. The emergence of a Cabinet Government is undoubtedly then a positive sign for the future.",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "paragraph",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        }
    }
}