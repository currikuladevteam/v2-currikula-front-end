import draft1 from "./draft.1.json";
import draft2 from "./draft.2.json";
import draft3 from "./draft.3.json";
import draft4 from "./draft.4.json"

export default [draft1, draft2, draft3, draft4];
