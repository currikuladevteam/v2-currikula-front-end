import reference1 from './reference.1.json';
import reference2 from './reference.2.json';
import reference3 from './reference.3.json';
import reference4 from './reference.4.json';
import reference5 from './reference.5.json';
import reference6 from './reference.6.json';
import reference7 from './reference.7.json';
import reference8 from './reference.8.json';
import reference9 from './reference.9.json';
import reference10 from './reference.10.json';
import reference11 from './reference.11.json';
import reference12 from './reference.12.json';
import reference13 from './reference.13.json';
import reference14 from './reference.14.json';
import reference15 from './reference.15.json';
import reference16 from './reference.16.json';
import reference17 from './reference.17.json';
import reference18 from './reference.18.json';
import reference19 from './reference.19.json';
import reference20 from './reference.20.json';
import reference21 from './reference.21.json';
import reference22 from './reference.22.json';



export default [
    reference1,
    reference2,
    reference3,
    reference4,
    reference5,
    reference6,
    reference7,
    reference8,
    reference9,
    reference10,
    reference11,
    reference12,
    reference13,
    reference14,
    reference15,
    reference16,
    reference17,
    reference18,
    reference19,
    reference20,
    reference21,
    reference22,
]