import note1 from "./note.1.json";
import note2 from "./note.2.json";
import note3 from "./note.3.json";
import note4 from "./note.4.json";
import note5 from "./note.5.json";
import note6 from "./note.6.json";
import note7 from "./note.7.json";
import note8 from "./note.8.json";
import note9 from "./note.9.json";
import note10 from "./note.10.json";
import note11 from "./note.11.json";
import note12 from "./note.12.json";
import note13 from "./note.13.json";
import note14 from "./note.14.json";
import note15 from "./note.15.json";

export default [note1, note2, note3, note4, note5, note6, note7, note8, note9, note10, note11, note12, note13, note14, note15];
