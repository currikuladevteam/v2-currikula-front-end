{
    "id": 2,
    "title": "Econ 102 - Income and Substitution Effects",
    "created_at": 1542522826761,
    "edited_at":  1546110356761,
    "project_id": 1,
    "project_completed": false,
    "user_id": 1,
    "document": {
        "type": "note",
        "font": "Times New Roman",
        "line_height": "1.5",
        "font_size": "1rem",
        "note_style": "bullets",
        "tags": [
            "Economics 102",
            "Income",
            "Substitution"
        ],
        "value": {
            "object": "value",
            "document": {
                "object": "document",
                "data": {},
                "nodes": [
                    {
                        "object": "block",
                        "type": "title",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "Income and Substitution Effects",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "bulleted-list",
                        "data": {},
                        "nodes": [
                            {
                                "object": "block",
                                "type": "list-item",
                                "data": {},
                                "nodes": [
                                    {
                                        "object": "text",
                                        "leaves": [
                                            {
                                                "object": "leaf",
                                                "text": "Changes in price can affect buyers' purchasing decisions; this effect is called the income effect. Increases in price, while they don't affect the amount of your paycheck, make you feel poorer than you were before, and so you buy less. Decreases in price make you feel richer, and so you may feel like buying more.",
                                                "marks": []
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "object": "block",
                                "type": "list-item",
                                "data": {},
                                "nodes": [
                                    {
                                        "object": "text",
                                        "leaves": [
                                            {
                                                "object": "leaf",
                                                "text": "If the price of hamburgers goes up, but the price of hot dogs stays the same, you might be more inclined to buy a hot dog. This tendency to change your purchase based on changes in relative price is called the substitution effect. When the price of hamburgers goes up, it makes hamburgers relatively expensive and hot dogs relatively cheap, which influences you to buy fewer hamburgers and more hot dogs than you usually would. Likewise, a decrease in hamburger price would cause you to eat more hamburgers and fewer hot dogs, according to the substitution effect.",
                                                "marks": []
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "object": "block",
                                "type": "list-item",
                                "data": {},
                                "nodes": [
                                    {
                                        "object": "text",
                                        "leaves": [
                                            {
                                                "object": "leaf",
                                                "text": "According to the substitution effect, however, hamburger consumption drops, but hot dog consumption rises. Thus, while it is clear what happens to hamburger consumption, since both effects tend to cause a decrease, we cannot be sure what happens to hot dog consumption, since there is both an increase (substitution effect) and a decrease (income effect).",
                                                "marks": []
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "object": "block",
                                "type": "list-item",
                                "data": {},
                                "nodes": [
                                    {
                                        "object": "text",
                                        "leaves": [
                                            {
                                                "object": "leaf",
                                                "text": "Another factor influencing demand is one which marketers and advertisers are always trying to understand and target: buyers' preferences. What do people like? When and how do they like it?",
                                                "marks": []
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "h2",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "Normal, Inferior, and Giffen Goods",
                                        "marks": [
                                            {
                                                "object": "mark",
                                                "type": "underline",
                                                "data": {}
                                            },
                                            {
                                                "object": "mark",
                                                "type": "bold",
                                                "data": {}
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "bulleted-list",
                        "data": {},
                        "nodes": [
                            {
                                "object": "block",
                                "type": "list-item",
                                "data": {},
                                "nodes": [
                                    {
                                        "object": "text",
                                        "leaves": [
                                            {
                                                "object": "leaf",
                                                "text": "Are all goods the same? Is more always better? Up to this point, we have been assuming that when we have more money, or feel like we have more money, we will tend to buy more goods. It makes sense: the more money we have, the more we buy. If we have less money, or if the price goes up, however, we tend to buy less.",
                                                "marks": []
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "object": "block",
                                "type": "list-item",
                                "data": {},
                                "nodes": [
                                    {
                                        "object": "text",
                                        "leaves": [
                                            {
                                                "object": "leaf",
                                                "text": "There are some exceptions, however: not all goods are normal goods. For instance, if an increase in your income causes you to buy less of a good, that good is called an inferior good. For instance, \"poor college students\" often satisfy themselves with generic soda and cheap ramen. When they get jobs and a steady income, however, they might forego the cheap soda and ramen in favour of Coke and pasta. In this example, the generic soda and cheap ramen are inferior goods.",
                                                "marks": []
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "object": "block",
                                "type": "list-item",
                                "data": {},
                                "nodes": [
                                    {
                                        "object": "text",
                                        "leaves": [
                                            {
                                                "object": "leaf",
                                                "text": "Income and substitution effects change demand differently with different types of goods. For instance, we have been looking at income and substitution effects when a buyer is faced with a choice between two normal goods. An increase in the price of good A will cause a decrease in consumption of A, and an increase in consumption of good B (assuming that the substitution effect is stronger than the income effect). If good A is a normal good, and good B is inferior, however, the results will be different.",
                                                "marks": []
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        }
    }
}