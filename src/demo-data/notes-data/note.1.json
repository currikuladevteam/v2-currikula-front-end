{
    "id": 1,
    "title": "Economics 101 - Labour productivity growth",
    "created_at": 1542422826761,
    "edited_at": 1548010356761,
    "project_id": 1,
    "project_completed": false,
    "user_id": 1,
    "deleted": false,
    "document": {
        "type": "note",
        "font": "Times New Roman",
        "line_height": "1.5",
        "font_size": "1rem",
        "note_style": "bullets",
        "tags": [
            "Economics 101",
            "Lecture",
            "Labour"
        ],
        "value": {
            "object": "value",
            "document": {
                "object": "document",
                "data": {},
                "nodes": [
                    {
                        "object": "block",
                        "type": "title",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "Economics 101 - Labour productivity growth",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "bulleted-list",
                        "data": {},
                        "nodes": [
                            {
                                "object": "block",
                                "type": "list-item",
                                "data": {},
                                "nodes": [
                                    {
                                        "object": "text",
                                        "leaves": [
                                            {
                                                "object": "leaf",
                                                "text": "When looking at what makes an economy grow in the long run, it is imperative to begin by examining how output is created. Firms use a combination of labor and capital to produce their output. Labor consists of the workers and employees who produce, manage, and process production. Capital describes both the ideas needed for production and the actual tools and machines used in production. Ideas and other intellectual property are called human capital. Machinery and tools are called physical capital.",
                                                "marks": []
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "object": "block",
                                "type": "list-item",
                                "data": {},
                                "nodes": [
                                    {
                                        "object": "text",
                                        "leaves": [
                                            {
                                                "object": "leaf",
                                                "text": "In order to increase productivity, each worker must be able to produce more output. This is referred to as labor productivity growth. The only way for this to occur is through an in increase in the capital utilised in the production process. This increase can be in the form of either human capital or physical capital.",
                                                "marks": []
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "object": "block",
                                "type": "list-item",
                                "data": {},
                                "nodes": [
                                    {
                                        "object": "text",
                                        "leaves": [
                                            {
                                                "object": "leaf",
                                                "text": "Another example may also be of use. Say there is a chef named Susan. Susan can cook 10 hamburgers in an hour. One day, she decides to go to the Hamburger Cooking School to learn how to cook hamburgers faster. When she returns to work, she is able to cook 40 hamburgers per hour by utilising the new tricks she learned. By attending the cooking school, Susan increased her human capital and thus increased her labor productivity.",
                                                "marks": []
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "h2",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "Growth level vs. growth rate",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "bulleted-list",
                        "data": {},
                        "nodes": [
                            {
                                "object": "block",
                                "type": "list-item",
                                "data": {},
                                "nodes": [
                                    {
                                        "object": "text",
                                        "leaves": [
                                            {
                                                "object": "leaf",
                                                "text": "When discussing growth, there is an important distinction that must be made. The growth level is the starting value of whatever is growing; the growth rate is the change in the growth level from year to year. These distinctions allow for accurate descriptions of economic policies on long-run growth.",
                                                "marks": []
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "object": "block",
                                "type": "list-item",
                                "data": {},
                                "nodes": [
                                    {
                                        "object": "text",
                                        "leaves": [
                                            {
                                                "object": "leaf",
                                                "text": "An example will help to illustrate the level vs. rate distinction. Let's use the idea of capital presented in the preceding section. Say a company owns 50 riveting tools like the one used by Joe. In order to increase output, the company decides to purchase 5 new riveting tools next year. In this case, the level of capital is 50 because this is the amount that the firm began with. The growth rate of capital is 10% because from one year to the next the amount of capital used by Joe's firm increased by 10%.",
                                                "marks": []
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "h2",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "Changes in growth rate vs. changes in the growth level over time",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "bulleted-list",
                        "data": {},
                        "nodes": [
                            {
                                "object": "block",
                                "type": "list-item",
                                "data": {},
                                "nodes": [
                                    {
                                        "object": "text",
                                        "leaves": [
                                            {
                                                "object": "leaf",
                                                "text": "Now that the growth rate vs. growth level distinction is clear, let's apply it to the way that economic policies affect productivity. The most important number in increasing economic productivity is the growth level. The growth level shows where the economy is relative to long term positioning. For instance, we know that the economy tends to grow at about 2% per year in the long run. This is the economy's growth level. When the economy grows at an increased amount, say 6% per year, the 4% difference between this and the growth level is called the growth rate.",
                                                "marks": []
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "object": "block",
                                "type": "list-item",
                                "data": {},
                                "nodes": [
                                    {
                                        "object": "text",
                                        "leaves": [
                                            {
                                                "object": "leaf",
                                                "text": "An economy with a low growth level will not grow very much in the long run even if the growth rate is high at times. For instance, over a 30-year period, an economy that has a steady growth level of 3% will far outgrow an economy that has an unpredictable growth rate but a growth level of 1%. In this way, it is important to keep both the growth rate and the growth level as high as possible, but if one is to be preferred over the other, a stable and high growth level is more desirable than an unpredictably fluctuating growth rate.",
                                                "marks": []
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "h2",
                        "data": {},
                        "nodes": [
                            {
                                "object": "text",
                                "leaves": [
                                    {
                                        "object": "leaf",
                                        "text": "Summary",
                                        "marks": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "object": "block",
                        "type": "bulleted-list",
                        "data": {},
                        "nodes": [
                            {
                                "object": "block",
                                "type": "list-item",
                                "data": {},
                                "nodes": [
                                    {
                                        "object": "text",
                                        "leaves": [
                                            {
                                                "object": "leaf",
                                                "text": "Why is this distinction important? Many people are shortsighted. When politicians manipulate economic variables, they may do so to create desirable short terms effects or to create desirable long-term effects. If they enact policies that temporarily increase economic growth, then they are affecting the growth rate. If, on the other hand, they enact policies that permanently increase economic growth, then they are affecting the growth level. As long as there is not a tradeoff between policies that affect the growth level and those that affect the growth rate, there is no conflict of interest. On the other hand, if increasing economic growth now results in relatively poorer long term economic growth, politicians may be tempted to trade an increase in their approval now for a slightly lower economic growth level. Here is where the difference between growth level and growth rate is most important, as evaluating economic interventions in the long run is difficult without employing this differentiation.",
                                                "marks": []
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        }
    }
}