import React, { Component } from 'react';
import { Row, Col, Layout, Divider, Button, Switch } from 'antd';
import HeaderTitle from '../components/HeaderTitle';
import PreviewDrawer from '../components/preview/PreviewDrawer';
import NoteCard from '../components/cards/NoteCard';
import BibliographyCard from '../components/cards/BibliographyCard';
import NewNoteModal from '../components/modals/NewNoteModal';
import NewBibliographyModal from '../components/modals/NewBibliographyModal';
import ProjectCard from '../components/cards/ProjectCard';

// Context
import { UserContext } from '../components/providers/UserProvider';
import EmptyCard from '../components/cards/EmptyCard';

const { Content } = Layout;

const headerStyle = {
  display: "inline",
  marginRight: 30,
  letterSpacing: '0.3rem'
}

class Documents extends Component {
  constructor(props) {
    super(props)
    this.child = React.createRef();
    this.state = {
      newNoteModalVisible: false,
      newBibliographyModalVisible: false,
      showCompleted: false // to change when there is settings saved
    }
  }

  renderNotes = () => {
    const { notes, projects } = this.context
    if (!notes) {
      return <Col span={6}>
        <NoteCard loading={true} title={"Loading..."} />
      </Col>
    }

    // If there are no notes
    if (notes.length === 0 || notes.filter(note => !note.deleted).length === 0 ) {
      return <Col span={6}>
      <EmptyCard
        icon="profile"
        theme='filled'
        text="You can create notes above, once created they will appear here."
        />
      </Col>
    }

    return notes.map((note, index) => {
      if (note.deleted) return '';
      if (note.project_completed && !this.state.showCompleted) return ''
      return (
        <Col span={6}
          key={index} >
          <NoteCard
            arrayIndex={index}
            projects={projects}
            projectId={note.project_id}
            note={note}
            deleteNote={this.context.deleteNote}
            preview={this.openPreview}
            length="25"
            title={note.title}
            document={note.document} />
        </Col>
      )
    })
  }

  renderDrafts() {
    const { drafts } = this.context;
    if (!drafts) {
      return <Col span={8}>
        <ProjectCard loading={true} title={"Loading..."} />
      </Col>
    }

    // If there are no drafts
    if (drafts.length === 0 || drafts.filter(draft => !draft.deleted).length === 0  ) {
      return <Col span={8}>
      <EmptyCard
        icon="file-text"
        text="Drafts can be created by starting a new project. They will then appear here."
         />
      </Col>
    }

    return drafts.map((draft, index) => {
    if (draft.project_completed && !this.state.showCompleted) return ''
    return <Col
        key={index}
        span={8}>
        <ProjectCard
          id={draft.id}
          preview={this.openPreview}
          document={draft}
          project={this.getProjectInfo(parseInt(draft.project_id))[0]}
          key={index}
          title={this.getProjectInfo(parseInt(draft.project_id))[0].title}
          progress={this.getProjectInfo(parseInt(draft.project_id))[0].progress}>
        </ProjectCard>
      </Col>
    })
  }
  renderBibliographies() {
    const { bibliographies } = this.context;
    if (!bibliographies) {
      return <Col span={8}>
        <ProjectCard loading={true} title={"Loading..."} />
      </Col>
    }

    // if there are no bibliographies
    if (bibliographies.length === 0 || bibliographies.filter(bib => !bib.deleted).length === 0 ) {
      return <Col span={8}>
      <EmptyCard
        icon="read"
        text="You can create bibliographies above, once created they will appear here."
         />
      </Col>
    }

    return bibliographies.map((bibliography, index) => {
      if ( bibliography.deleted ) return ''
      if (bibliography.project_completed && !this.state.showCompleted) return ''
      return (
        <Col
          key={index}
          span={8}>
          <BibliographyCard
            title={
              bibliography.project_id ?
                this.getProjectInfo(parseInt(bibliography.project_id))[0].title : null}
            // preview={this.openPreview}
            key={index}
            bibliography={bibliography}
            deleteBibliography={this.context.deleteBibliography}
          >
          </BibliographyCard>
        </Col>

      )
    }


    )
  }

  openPreview = (content) => {
    this.child.current.showDrawer(content);
  }
  openModal = (modal) => {
    if (modal === 'project') {
      this.setState({
        newProjectModalVisible: true
      })
    } else if (modal === 'note') {
      this.setState({
        newNoteModalVisible: true
      })
    } else if (modal === 'bib') {
      this.setState({
        newBibliographyModalVisible: true
      })
    }

  }

  closeModal = (modal) => {
    if (modal === 'project') {
      this.setState({
        newProjectModalVisible: false
      })
    } else if (modal === 'note') {
      this.setState({
        newNoteModalVisible: false
      })
    } else if (modal === 'bib') {
      this.setState({
        newBibliographyModalVisible: false
      })
    }
  }

  getProjectInfo(project_id) {
    const { projects } = this.context;
    return projects.filter(project => project.id === project_id)
  }

  onSwitchChange(showCompleted) {
    this.setState({
      showCompleted: showCompleted
    })
  }

  render() {
    return (
      <Layout>
        <NewNoteModal openModal={this.openModal} closeModal={this.closeModal} visible={this.state.newNoteModalVisible} />
        <NewBibliographyModal openModal={this.openModal} closeModal={this.closeModal} visible={this.state.newBibliographyModalVisible} />
        <HeaderTitle title="ALL DOCUMENTS" />
        <Content style={{ padding: 40 }}>
          <Row style={{ marginBottom: 20}}>
            <Col span={19}>
              <h2 style={headerStyle}>DRAFTS</h2>
            </Col>
            <Col span={5}>
              <div onPointerDown={() => console.log(this.state)} style={{letterSpacing: 1.1, display: 'inline-block', marginRight: 10}}>Show Completed</div>
              <Switch checked={this.state.showCompleted} onChange={(change) => this.onSwitchChange(change)}/>
            </Col>
          </Row>
          <Row gutter={10} >
            {this.renderDrafts()}
          </Row>
          <Divider />
          <Row style={{ marginBottom: 20 }}>
            <Col span={24}>
              <h2 style={headerStyle}>NOTES</h2>
              <Button type="primary" icon="plus" onClick={() => this.openModal('note')} >Create New Note</Button>
            </Col>
          </Row>
          <Row gutter={10}>
            {this.renderNotes()}
          </Row>
          <Divider />
          <Row style={{ marginBottom: 20 }}>
            <Col span={24}>
              <h2 style={headerStyle}>BIBLIOGRAPHIES</h2>
              <Button type="primary" icon="plus" onClick={() => this.openModal('bib')} >
                Create New Bibliography</Button>
            </Col>
          </Row>
          <Row gutter={10}>
            {this.renderBibliographies()}
          </Row>
          <Divider />
        </Content>
        <PreviewDrawer ref={this.child} />
      </Layout>
    );
  }
}

Documents.contextType = UserContext;


export default Documents;