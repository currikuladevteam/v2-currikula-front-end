import React, { Component } from 'react';
import { Row, Layout } from 'antd';
import PanelLeft from '../components/PanelLeft';
import PanelRight from '../components/PanelRight';
import ProjectOverviewExpandedCard from '../components/project-overview/ProjectOverviewExpandedCard';
import OverviewTabs from '../components/project-overview/OverviewTabs';
import HeaderTitle from '../components/HeaderTitle';
import { withRouter } from 'react-router-dom'

// Fake API's
import { getProject } from '../demo-data/fake-apis/projects.api';


const { Content } = Layout;

class ProjectOverview extends Component {
  constructor(props) {
    super(props)
    this.state = {
      project: null
    }
  }

  // Top of tree API request
  componentDidMount() {
    // Retrieves the project from the  API
    // Defaults to ID of 1 for now
    getProject(this.props.id ? parseInt(this.props.id) : 1).then((project) => {
      this.setState({
        project: project[0],
      })
    }).catch(({error, msg}) => {
      if (error) {
        this.props.history.goBack('/dashboard');
      }

    })
  }


  render() {
    const { project } = this.state;
    return (
      <Layout>
        <HeaderTitle backButton={true} title="PROJECT OVERVIEW" />
        <Content style={{ padding: 20 }}>
          <Row gutter={20}>
            <PanelLeft>
              <ProjectOverviewExpandedCard
                project={this.state.project}
                />
            </PanelLeft>
            <PanelRight>
              <OverviewTabs 
                timeLine={project ? project.timeline :  null } 
                exportedAt={project ? project.exported_at :  null}
              />
            </PanelRight>
          </Row>
        </Content>
      </Layout>
    );
  }
}
export default withRouter(ProjectOverview);