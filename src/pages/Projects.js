import React, { Component } from 'react';
import { Row, Col, Layout, Divider, Button } from 'antd';
import HeaderTitle from '../components/HeaderTitle';
import ProjectCards from '../components/cards/ProjectCard';
import PreviewDrawer from '../components/preview/PreviewDrawer';
import NewProjectModal from '../components/modals/NewProjectModal';


// Context
import { UserContext } from '../components/providers/UserProvider'
import EmptyCard from '../components/cards/EmptyCard';


// Importing Fake API
// import { getDraft } from '../demo-data/fake-apis/drafts.api'


const { Content } = Layout;


const headerStyle = {
  display: "inline",
  marginRight: 30,
  letterSpacing: '0.3rem'
}

class Projects extends Component {
  constructor(props) {
    super(props)
    this.child = React.createRef();
    this.state = {
      newProjectForm: false,
      completedEssay: {
        title: "An Example Completed Essay That Has A Series of Words That Are Of A Certain Length",
        content: "fakeContent",
        type: "Draft",
        id: 'id',
        progress: {
          project: 100,
          due: new Date().getTime() - 1000000,
          improve: {
            total: 145,
            completed: 145
          },
          words: {
            target: 3000,
            current: 3101
          }
        },
      }
    }
  }

  openPreview = (content) => {
    this.child.current.showDrawer(content);
  }

  openModal = (modal) => {
    if (modal === 'project') {
      this.setState({
        newProjectForm: true
      })
    }
  }

  closeModal = (modal) => {
    if (modal === 'project') {
      this.setState({
        newProjectForm: false
      })
    }
  }


  renderProjects(renderCompleted) {
    const { projects, drafts } = this.context
    let projectsToRender
    if (!projects) {
      return <Col span={8}>
        <ProjectCards loading={true} title={"Loading..."} />
      </Col>
    }
    if (projects.length > 0 && renderCompleted) {
      projectsToRender = projects.filter(project => project.exported_at !== null)
    } else {
      projectsToRender = projects.filter(project => project.exported_at === null)
    }

    if (projectsToRender.length === 0 || projects.filter(project => !project.deleted).length === 0) {
      return <Col span={8}>
      <EmptyCard 
        icon="folder-add"
        text="Once you start and complete projects they will appear here."
         />
      </Col>
    }
    return projectsToRender.map((project, index) =>
      <Col key={index} span={8}>
        <ProjectCards
          id={project.id}
          project={project}
          // Does this need to be requesting a new draft? Or is the data available here
          // preview={() => getDraft(project.draft.id).then(draft => this.openPreview(draft[0].document))}
          document={drafts.filter(draft => draft.project_id === project.id)[0]}
          preview={this.openPreview}
          title={project.title}
          progress={project.progress}>
        </ProjectCards>
      </Col>
    )
  }

  render() {

    return (
      <Layout>
        <NewProjectModal openModal={this.openModal} closeModal={this.closeModal} visible={this.state.newProjectForm} />
        <HeaderTitle title="PROJECTS" />
        <Content style={{ padding: 40 }}>
          <Row style={{ marginBottom: 20 }}>
            <Col span={24}>
              <h2 style={headerStyle}>CURRENT PROJECTS</h2>
              <Button type="primary" icon="plus" onClick={() => this.openModal('project')}>Start New Project</Button>
            </Col>
          </Row>
          <Row gutter={10}>
            {this.renderProjects(false)}
          </Row>
          <Divider />
          <Row>
            <h2 style={headerStyle}>COMPLETED PROJECTS</h2>
          </Row>
          <Row gutter={10}>
            {this.renderProjects(true)}
          </Row>
        </Content>
        <PreviewDrawer ref={this.child} />
      </Layout>
    );
  }
}

Projects.contextType = UserContext;


export default Projects;