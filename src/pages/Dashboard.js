import React, { Component } from 'react';
import { Row, Layout } from 'antd';

import StartHere from '../components/dashboard/StartHere';
import YearProgress from '../components/dashboard/YearProgress';
import RecentNotes from '../components/dashboard/RecentNotes';
import CurrentProjectProgress from '../components/dashboard/CurrentProjectProgress';

import PanelLeft from '../components/PanelLeft';
import PanelRight from '../components/PanelRight';
import HeaderTitle from '../components/HeaderTitle';
import PreviewDrawer from '../components/preview/PreviewDrawer';

// Context
import { UserContext } from '../components/providers/UserProvider';

const {
  Content,
} = Layout;


class Dashboard extends Component {
  constructor() {
    super()
    this.child = React.createRef();
  }
  openPreview = (document) => {
    this.child.current.showDrawer(document);
  }

  render() {
    const { projects, notes, drafts } = this.context;
    return (
      <Layout>
        <HeaderTitle title="DASHBOARD" />
        <Content style={{ padding: '20px' }}>
          <Row gutter={20}>
            <PanelLeft>
              <StartHere />
              <YearProgress />
              <RecentNotes
                preview={this.openPreview}
                notes={notes}
                projects={projects} />
            </PanelLeft>
            <PanelRight>
              <CurrentProjectProgress
                preview={
                  (draft_id) =>  this.openPreview(drafts.filter(draft => draft.id === draft_id)[0])
                }
                projects={projects} />
            </PanelRight>
          </Row>
        </Content>
        <PreviewDrawer ref={this.child} />
      </Layout>
    );
  }
}
Dashboard.contextType = UserContext;


export default Dashboard;


