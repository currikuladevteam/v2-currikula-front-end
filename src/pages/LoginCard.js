import React, { Component } from 'react';
// import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { Link } from 'react-router-dom';
import { UserContext } from '../components/providers/UserProvider'


const FormItem = Form.Item;


class LoginCard extends Component {

  render() {
    console.log(this.context)
    return (
      <Form>
        <FormItem>
          <h1 style={{ textAlign: "center", letterSpacing: '0.3rem', margin: 10 }}>LOGIN</h1>
          <p style={{ textAlign: "center" }}>Use the credentials that you registered with to login here.</p>
          <Input addonBefore={<div style={{ width: 80, textAlign: 'left' }}><Icon type="mail" /> Email</div>} placeholder="Enter your email address" />
          <Input addonBefore={<div style={{ width: 80, textAlign: 'left' }}><Icon type="lock" /> Password</div>} type="password" placeholder="Enter your password here" />
          <Checkbox style={{ marginLeft: 8 }}>Remember me</Checkbox>
          <FormItem style={{ marginTop: 30 }}>
            <Button type="primary" block onClick={() => this.context.loginUser(1)} ><Link to="/dashboard">Log in</Link></Button>
            <Link to="/forgot-password"><Button block type="danger" ghost>Forgot password</Button></Link>
          </FormItem>
        </FormItem>
      </Form>
    )
  }
}
LoginCard.contextType = UserContext;



export default LoginCard;
