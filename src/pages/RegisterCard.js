import React, { Component } from 'react';
// import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { Link } from 'react-router-dom';
import TutorialCarousel from '../components/tutorial/TutorialCarousel';

const FormItem = Form.Item;




class RegisterCard extends Component {

	constructor() {
		super();
		this.state = {
			openComponent: false
		};
	}
	handleSubmit = e => {
		e.preventDefault();
		// redirect to dashboard
	};

	//activates the openTutorial function 
	handleOpenTutorial = e => {
		e.preventDefault();
		this.props.openTutorial();
	}

	openTutorial = e => {
		console.log('Setting State')


	};


	render() {
		if (this.state.openComponent === true) {
			return <TutorialCarousel toggleFooter={this.props.toggleFooter} showFooter={this.props.showFooter} />
		}
		return (
			<React.Fragment>
				<Form onSubmit={this.handleSubmit} className="login-form">
					<FormItem>
						<h1
							style={{ textAlign: 'center', letterSpacing: '0.3rem', margin: 10 }}
						>
							ACCOUNT REGISTRATION
					</h1>
						<p style={{ textAlign: 'center' }}>
							Fill out the information below to quickly create an account.
					</p>
						<Input
							addonBefore={
								<div style={{ width: 80, textAlign: 'left' }}>
									<Icon type="user" /> Name
							</div>
							}
							placeholder="Enter your full name"
						/>
						<Input
							addonBefore={
								<div style={{ width: 80, textAlign: 'left' }}>
									<Icon type="mail" /> Email
							</div>
							}
							placeholder="Enter your email address"
						/>
					</FormItem>
					<FormItem>
						<Input
							addonBefore={
								<div style={{ width: 80, textAlign: 'left' }}>
									<Icon type="lock" /> Password
							</div>
							}
							type="password"
							placeholder="Enter your password here"
						/>
						<Input
							addonBefore={
								<div style={{ width: 80, textAlign: 'left' }}>
									<Icon type="lock" /> Password
							</div>
							}
							type="password"
							placeholder="Enter your password again to confirm"
						/>
					</FormItem>
					<Checkbox style={{ marginLeft: 8 }}>
						I am over 16 years of age.
				</Checkbox>
					<Checkbox>I agree to Currikula's Terms and Conditions</Checkbox>
					<Checkbox>I agree to Currikula's Privacy Policy</Checkbox>
					<Checkbox>I consent to very occasional marketing emails</Checkbox>

					<FormItem style={{ marginTop: 30 }}>
						<Button
							type="primary"
							block
							// htmlType="submit"
							onPointerDown={e => this.handleOpenTutorial(e)}
						> Create Account
							{/* <Link to="/dashboard">Create Account</Link> */}
						</Button>
					</FormItem>
					<p style={{ textAlign: 'center', width: '80%', margin: '0 auto' }}>
						You can read our Terms & Conditions
					<Link to="/terms-and-conditions">here</Link> and see our Privacy
					Policy <Link to="/privacy-policy">here</Link>.
				</p>

				</Form>

			</React.Fragment>
		);
	}
}

export default RegisterCard;
