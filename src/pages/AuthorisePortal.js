import React, { Component } from 'react';
import { Icon, Tabs, Layout, Card } from 'antd';
import LoginCard from './LoginCard';
import RegisterCard from './RegisterCard';
import TutorialCarousel from '../components/tutorial/TutorialCarousel';

const TabPane = Tabs.TabPane;

class AuthorisePortal extends Component {
	constructor() {
		super();
		this.state = {
			openComponent: false,
			test: 'test1'
		};
	}

	handleSubmit = e => {
		e.preventDefault();
		// redirect to dashboard
	};

	openTutorial = e => {
		console.log('Setting State')
		this.setState({
			openComponent: true,
			test: 'test2'
		})
	};

	render() {
		if (this.props.showTutorial) {
			return <TutorialCarousel toggleFooter={this.props.toggleFooter} showFooter={this.props.showFooter} /> 
		}
		return (

			<React.Fragment>
				<Layout style={{ minHeight: '100vh', padding: '60px 0px' }}>
					<Card
						hoverable
						style={{ width: 500, margin: '0 auto', cursor: 'default' }}
					>
						<Tabs defaultActiveKey="1">
							<TabPane
								tab={
									<span>
										<Icon type="lock" />
										Login
									</span>
								}
								key="1"
							>
								<LoginCard />
							</TabPane>
							<TabPane
								tab={
									<span>
										<Icon type="user" />
										Register
									</span>
								}
								key="2"
							>
								<RegisterCard 
									openTutorial={this.props.openTutorial} />
							</TabPane>
						</Tabs>
					</Card>

				</Layout>
			</React.Fragment>
		);
	}
}

export default AuthorisePortal;

/* <Route exact path="/" component={Home} /> */
