import React, { Component } from 'react';
import { withRouter } from 'react-router-dom'
import { Card, Steps, Input, Button } from 'antd';


const Step = Steps.Step;

const steps = [
  {
    title: 'FORGOT PASSWORD',
    description:
      'If you have forgotten you password, enter your email below to get sent a reset code.',
    input: true,
    placeholder: 'example@example.com',
    proceed: true,
    proceedText: 'SEND CODE',
    back: true,
    backText: 'TAKE ME BACK'
  },
  {
    title: 'ENTER CODE',
    description:
      "Enter the code you received in the email from us. If you can't find it, check in 								the spam folder. If there is still no email, then you have made a mistake with your email address.",
    input: true,
    placeholder: '',
    proceed: true,
    proceedText: 'PROCEED',
    back: true,
    backText: 'RESEND EMAIL'
  },
  {
    title: 'ENTER NEW PASSWORD',
    description: 'Enter a new password below.',
    input: true,
    placeholder: '',
    proceed: true,
    proceedText: 'SAVE PASSWORD',
    back: false
    // backText: 'RESEND EMAIL',
  },
  {
    title: 'PASSWORD CHANGED',
    description:
      'Congratulations your password has been reset and changed. You can login from the homepage whenever you like, or you can straight away by clicking the link below.',
    input: true,
    placeholder: '',
    proceed: true,
    proceedText: 'LOG ME IN',
    back: true,
    backText: 'TAKE ME BACK TO THE HOMEPAGE'
  }
];

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0
    };
  }

  next() {
    if (this.state.current === steps.length -1) {

      return this.props.history.push('/dashboard')
    };
    // console.log(this.state.current)
    // console.log(steps.length)
    const current = this.state.current + 1;
    this.setState({ current });
  }

  prev() {
     if (this.state.current === 0){

      return this.props.history.goBack()
    } else if(this.state.current === steps.length - 1) {
      return this.props.history.push('/access')

    }
    console.log(this.state.current)
    console.log(steps.length)
    const current = this.state.current - 1;
    this.setState({ current });
  }

  render() {
    const { current } = this.state;

    return (
      <div
        style={{ background: '#ECECEC', padding: '40px 30px', height: '100vh', paddingTop: '200px'}}
      >
        <Card bordered={false} style={{ width: '800px', margin: 'auto' }}>
          <Steps current={current}>
            {steps.map(item => (
              <Step key={item.title} />
            ))}
          </Steps>

          <div style={{ width: '280px', margin: 'auto' }}>
            <div className="steps-content">
              <h1 style={{ textAlign: 'center', margin: '30px 0 10px 0' }}>
                {steps[current].title}
              </h1>

              <p style={{ textAlign: 'center', marginBottom: '30px ' }}>
                {steps[current].description}
              </p>

              {steps[current].input ? (
                <Input placeholder={steps[current].placeholder} />
              ) : (
                ''
              )}





              {steps[current].proceed ? (
                <Button
                onClick={() => this.next()}
                  block
                  style={{
                    marginTop: '20px',
                    marginBottom: '5px',
                    backgroundColor: 'green',
                    color: 'white',
                    fontWeight: 'bold',
                  }}
                >
                  {steps[current].proceedText}
                </Button>

    
              )
              
              
               : 
               
               ''
               
               }





              {steps[current].back ? (
                <Button
                onClick={() => this.prev()}
                  block
                  style={{
                    marginTop: '5px',
                    marginBottom: '20px',
                    backgroundColor: 'grey',
                    color: 'white',
                    fontWeight: 'bold'
                  }}
                >
                  {steps[current].backText}
                </Button>
              ) : (
                ''
              )}
            </div>
          </div>
          {/* <div className="steps-action">
            {current < steps.length - 1 && (
              <Button type="primary" onClick={() => this.next()}>
                Next
              </Button> */}
            
            {/* {current === steps.length - 1 && (
              <Button
                type="primary"
                onClick={() => message.success('Processing complete!')}
              >
                Done
              </Button>
            )} */}
            {/* {current > 0 && (
              <Button style={{ marginLeft: 8 }} onClick={() => this.prev()}>
                Previous
              </Button>
            )}
          </div> */}
        </Card>
      </div>
    );
  }
}

export default withRouter(ForgotPassword);
