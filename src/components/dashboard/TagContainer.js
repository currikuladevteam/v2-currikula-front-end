import React, { Component } from 'react';
import { Tag } from 'antd';

class TagContainer extends Component {

    truncateTags = (words, length) => {
        if (typeof length === 'string') {
            const numLength = parseInt(length);
            if (words.split("").length < numLength) {
                return words;
            } else {
                return words.substring(0, numLength) + "...";
            }
        }
    }

    randomTagGenerator = () => {
        const tags = [
            {
                title: 'History',
                colour: 'green'
            },
            {
                title: 'Lecture-2',
                colour: 'blue'
            },
            {
                title: 'French Class',
                colour: 'pink'
            },
            {
                title: 'French Lectures',
                colour: 'yellow'
            },
            {
                title: 'Maths Study',
                colour: 'darkred'
            },
            {
                title: 'Economics Research',
                colour: 'orange'
            },
            {
                title: 'English Essay',
                colour: 'purple'
            },
            {
                title: 'Bio Lab Notes',
                colour: 'black'
            },
            {
                title: 'Phil101',
                colour: 'grey'
            }]

        const chosenTags = [];
        for (var x in tags) {
            const randomTagNumber = Math.floor(Math.random() * 9);

            if (chosenTags.indexOf(tags[randomTagNumber]) === -1 && chosenTags.length < 5) {
                chosenTags.push(tags[randomTagNumber]);
            }
            if (parseInt(x) === tags.length - 1) {
                return (chosenTags);
            }
        }
    }


    render() {
        return (
            <div style={{
                padding: 10,
                height: 70,
                overflow: 'scroll'
            }}>
                {this.randomTagGenerator().map((tag, index) => <Tag key={index} style={{ margin: 4 }} color={tag.colour}>{tag.title}</Tag>)}
                {this.randomTagGenerator().map((tag, index) => <Tag key={index} style={{ margin: 4 }} color={tag.colour}>{tag.title}</Tag>)}
            </div>
        );
    }
}

export default TagContainer;
