import React, { Component } from 'react';
import { Row, Col } from 'antd';
import NoteCard from '../cards/NoteCard';
import { UserContext } from '../providers/UserProvider';
import EmptyCard from '../cards/EmptyCard';

class RecentNotes extends Component {
  constructor(props) {
    super(props)
    this.state = {
    };
  }

  // handleDeleteNote = (noteIndex) => {
  //   const deletedNote = Object.assign({}, this.state.notes[noteIndex], { deleted: true });
  //   this.context.deleteNote([
  //     ...this.state.notes.slice(0, noteIndex),
  //     deletedNote,
  //     ...this.state.notes.slice(noteIndex + 1)
  //   ])
  // }

  componentDidUpdate(state) {
    if (this.props.notes && this.props.notes !== this.state.notes) {
      this.setState({
        notes: this.props.notes
      })
    }
  }
  componentDidMount(state) {
    if (this.props.notes && this.props.notes !== this.state.notes) {
      this.setState({
        notes: this.props.notes
      })
    }
  }

  renderNoteCards() {

    const { notes } = this.props; 
    if (!notes) {
      return ['', '', '', ''].map((note, index) => {
        return (
          <Col span={12}
            key={index}
            style={{ marginBottom: 10}}
          >
            <NoteCard
              loading={true} />
          </Col>
        )
      })

      // If there are no notes
    } else if (notes.length === 0 || notes.filter(note => !note.deleted).length === 0 ) {
      return <Col span={12}>
      <EmptyCard
        icon="profile"
        theme='filled'
        text="You can create notes above, once created they will appear here."
        />
      </Col>
      
    } else if (notes) {

      // Sorts the notes into most recent by the edited_at property
      var count = 0;
      const maxNotesToDisplay = 4;

      const unsortedNotes = this.props.notes;
      let sortedNotes
      if (unsortedNotes.length > 4) {
        sortedNotes = unsortedNotes.sort((noteA, noteB) =>
        noteA.edited_at > noteB.edited_at ? -1 : 1
        );
      } else {
        sortedNotes = unsortedNotes;
      }
     
      return sortedNotes.map((note, index) => {
        if (count >= maxNotesToDisplay) return '';
        if (!note.deleted && !note.project_completed) {
          count = count + 1;
        } else return '';
        return (
          <Col span={12}
            key={index}
          >
            <NoteCard
              projects={this.props.projects}
              projectId={note.project_id}
              arrayIndex={index}
              note={note}
              deleteNote={this.context.deleteNote}
              preview={this.props.preview}
              length="25"
              title={note.title}
              document={note.document} />
          </Col>
        )
      })

    }

  }



  render() {
    const CardContainer = {
      height: '50%'
    }

    return (
      <div style={CardContainer}>
        <h3 style={{ letterSpacing: '0.2rem' }}> RECENT NOTES </h3>
        <Row gutter={10}>
          {this.renderNoteCards()}
        </Row>
      </div>
    );
  }
}

RecentNotes.contextType = UserContext;


export default RecentNotes;
