import React, { Component } from 'react';
import { Card, Icon } from 'antd';


// Modals
import NewProjectModal from '../modals/NewProjectModal';
import NewNoteModal from '../modals/NewNoteModal';
import NewBibliographyModal from '../modals/NewBibliographyModal';

class IconContainer extends Component {
  state = {
    newProjectModalVisible: false,
    newNoteModalVisible: false,
    newBibliographyModalVisible: false,
  };


  openModal = (modal) => {
    if (modal === 'project') {
      this.setState({
        newProjectModalVisible: true
      })
    } else if (modal === 'note') {
      this.setState({
        newNoteModalVisible: true
      })
    } else if (modal === 'bib') {
      this.setState({
        newBibliographyModalVisible: true
      })
    }

  }

  closeModal = (modal) => {
    if (modal === 'project') {
      this.setState({
        newProjectModalVisible: false
      })
    } else if (modal === 'note') {
      this.setState({
        newNoteModalVisible: false
      })
    } else if (modal === 'bib') {
      this.setState({
        newBibliographyModalVisible: false
      })
    }
  }


  render() {
    const gridStyle = {
      height: '140px',
      width: '33.3%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      textAlign: 'center',
      margin: 0,
    };

    const titleStyle = {
      margin: '0 auto',
      width: '100%',
      marginTop: '10px'
    }

    return (
      <Card style={{ width: '100%', cursor: 'pointer' }}>
        <NewProjectModal 
          openModal={this.openModal} 
          closeModal={this.closeModal} 
          visible={this.state.newProjectModalVisible} 
        />
        <NewNoteModal 
          openModal={this.openModal} 
          closeModal={this.closeModal} 
          visible={this.state.newNoteModalVisible} 
          />
        <NewBibliographyModal 
          openModal={this.openModal} 
          closeModal={this.closeModal} 
          visible={this.state.newBibliographyModalVisible} 
        />
        <Card.Grid style={gridStyle} onClick={() => this.openModal('project')}>

          <div style={titleStyle}>
            <Icon type="folder-add" style={{fontSize: '2rem'}}/>
            <div style={titleStyle}>
              <h5>NEW</h5>
              <h5>PROJECT</h5>
            </div>
          </div>

        </Card.Grid>
        <Card.Grid style={gridStyle} onClick={() => this.openModal('note')}>
          <div style={titleStyle}>
            <Icon type="profile" style={{fontSize: '2rem'}}/>
            <div style={titleStyle}>
              <h5>NEW</h5>
              <h5>NOTE</h5>
            </div>
          </div>
        </Card.Grid>
        <Card.Grid style={gridStyle} onClick={() => this.openModal('bib')}>
          <div style={titleStyle}>
            <Icon type="read" style={{fontSize: '2rem'}}/>
            <div style={titleStyle}>
              <h5>NEW</h5>
              <h5>BIBLIOGRAPHY</h5>
            </div>
          </div>
        </Card.Grid>
      </Card>

    );
  }
}

export default IconContainer;