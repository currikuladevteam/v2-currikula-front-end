import React, { Component } from 'react';
import ProjectCard from '../cards/ProjectCard';

class CurrentProjectProgress extends Component {

  renderProjects() {
    const { projects } = this.props;
    return projects ? projects.map((project, index) => {
      // Should be sorted by most recent edits)
      if (project.exported_at) return ''
      return (
        <ProjectCard
          id={project.id}
          project={project}
          preview={() => this.props.preview(project.draft.id)}
          key={index}
          title={project.title}
          progress={project.progress}>
        </ProjectCard>
      )
    }) :
      <ProjectCard
        loading={true}
      />
  }

  render() {
    return (
      <div>
        <h2 style={{
          letterSpacing: '0.2rem', maxWidth: '500px',
          margin: 'auto'
        }}> CURRENT PROJECTS </h2>
        {this.renderProjects()}
      </div>
    );
  }
}
export default CurrentProjectProgress;
