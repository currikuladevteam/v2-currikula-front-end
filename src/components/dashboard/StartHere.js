import React, { Component } from 'react';

import IconContainer from './IconContainer';

class StartHere extends Component {
  render() {
	const HeaderTitleStyle = {
    // height: '200px'
  }
 
    return (
      <div style={HeaderTitleStyle} >
        <h2 style={{letterSpacing: '0.2rem'}}>START HERE</h2>
        <IconContainer />
      </div>
    );
  }
}

export default StartHere;
