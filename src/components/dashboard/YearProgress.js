import React, { Component } from 'react';
import { Progress, Col, Row, Card } from 'antd';
import TermProgressUpdater from '../../functions/helpers/TermProgressUpdater';


class YearProgress extends Component {
  render() {
    const YearProgress = TermProgressUpdater();
    const YearProgressStyle = {
      margin: '20px 0'
    };
    return (
      <div style={YearProgressStyle}>
        <h3 style={{ letterSpacing: '0.2rem' }}> YEAR PROGRESS </h3>

        <Card
          hoverable
          style={{ cursor: 'default' }}
        >
          <Row>
            <Col span={8}>
              <h5>First Term</h5>
              <Progress percent={YearProgress[0]} status="active" showInfo={false} />
            </Col>
            <Col span={8}>
              <h5>Second Term</h5>
              <Progress percent={YearProgress[1]} status="active" showInfo={false} />
            </Col>
            <Col span={8}>
              <h5>Third Term</h5>
              <Progress percent={YearProgress[2]} status="active" showInfo={false} />
            </Col>
          </Row>
        </Card>
      </div>

    );
  }
}
export default YearProgress;

