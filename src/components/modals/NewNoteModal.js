import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import NoteStyleCarousel from './NoteStyleCarousel';
import Log from '../../functions/api/logs/Log';
import { UserContext } from '../providers/UserProvider';

import { Modal, Form, Input, Radio, Select, notification } from 'antd';

const NoteCreateForm = Form.create({ name: 'newNoteModal' })(
	// eslint-disable-next-line

	class extends React.Component {
		// for (let i = 10; i < 36; i++) {
		//   children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
		// }

		handleTagsChange(value) {
			console.log(`selected ${value}`);
		}

		child = React.createRef();
		render() {
			const { visible, onCancel, onCreate, form } = this.props;

			const { getFieldDecorator } = form;
			// note style selection
			const RadioButton = Radio.Button;
			const RadioGroup = Radio.Group;

			function handleChange(value) {
				console.log(`you selected: ${value}`);
			}

			const tags = [];

			const { Option } = Select;

			return (
				<Modal
					visible={visible}
					title={
						<h3
							style={{
								textAlign: 'center',
								letterSpacing: '0.3px',
								marginBottom: 0
							}}
						>
							CREATE NEW NOTE
						</h3>
					}
					okText="Create"
					onCancel={onCancel}
					onOk={onCreate}
				>
					<Form layout="vertical">
						<Form.Item label="Title">
							{getFieldDecorator('title', {
								rules: [
									{
										required: true,
										message: 'Please input the title of note!'
									}
								]
							})(<Input placeholder="Please input a title for your note" />)}
						</Form.Item>

						<Form.Item label="Project">
							{getFieldDecorator('essay', {
								initialValue: 'noneUnattached'
							})(
								<Select
									// defaultValue="noneUnattached"
									style={{ width: 200 }}
									onChange={handleChange}
								>
									<Option value="noneUnattached">None (unattached)</Option>
									<Option value="essayTitle1">Essay title 1</Option>
									<Option value="essayTitle2">Essay title 2</Option>
									<Option value="essayTitle3"> Essay title 3</Option>
								</Select>
							)}
						</Form.Item>

						<Form.Item label="Tags">
							{getFieldDecorator('tags')(
								<Select
									mode="tags"
									style={{ width: '100%' }}
									onChange={handleChange}
									tokenSeparators={[',']}
								>
									{tags}
								</Select>
							)}
						</Form.Item>

						<Form.Item label="Style" style={{ textAlign: 'center' }}>
							{/* Options carousel for note type */}
							<NoteStyleCarousel innerRef={this.child} />
							{getFieldDecorator('style', {
								initialValue: 'sentenceMethod'
							})(
								<RadioGroup
									style={{ margin: '0 auto' }}
									buttonStyle="solid"
									size="large"
								>
									<RadioButton
										style={{}}
										value="sentenceMethod"
										onClick={() => {
											this.child.current.goTo(0);
										}}
									>
										Sentence
									</RadioButton>
									<RadioButton
										value="outlineMethod"
										onClick={() => {
											this.child.current.goTo(1);
										}}
									>
										Outline
									</RadioButton>
									<RadioButton
										value="cornellMethod "
										onClick={() => {
											this.child.current.goTo(2);
										}}
									>
										Cornell
									</RadioButton>
									<RadioButton
										value="freeformMethod"
										onClick={() => {
											this.child.current.goTo(3);
										}}
									>
										Freeform
									</RadioButton>
								</RadioGroup>
							)}
						</Form.Item>
					</Form>
				</Modal>
			);
		}
	}
);

class NewNoteModal extends Component {
	state = {
		sending: false
	};

	// showModal = () => {
	//   this.setState({ visible: true });
	// }

	handleCancel = () => {
		this.props.closeModal('note');
	};

	handleCreate = () => {
		const form = this.formRef.props.form;

		// this.props.history.push(`/writer/id/note`);

		form.validateFields((err, values) => {
			if (err) {
				return;
			}
			setTimeout(() => {
				this.setState({
					sending: true
				});

				this.handleCancel();
				const { user } = this.context;
				const event = {
					type: 'create',
					message: (
						<p>
							You created the note - <b>{values.title}</b>
						</p>
					),
					document: {
						type: 'note',
						project_id: values.project_id ? values.project_id : null,
						title: values.title,
						tags: values.tags ? values.tags : null,
						style: values.style
					}
				};
				const log = new Log({ event, type: 'create', user }).log();
				// Fire off the notification (passing in the notification object)
				// This is required to give a location to the notification
				log.notify(notification)();
				form.resetFields();
			}, 500);
		});
	};

	saveFormRef = formRef => {
		this.formRef = formRef;
	};

	render() {
		return (
			<div>
				<NoteCreateForm
					wrappedComponentRef={this.saveFormRef}
					visible={this.props.visible}
					onCancel={this.handleCancel}
					onCreate={this.handleCreate}
					confirmLoading={this.state.sending}
				/>
			</div>
		);
	}
}





const WrappedNoteModal = withRouter(NewNoteModal);

WrappedNoteModal.WrappedComponent.contextType = UserContext;

export default WrappedNoteModal;




