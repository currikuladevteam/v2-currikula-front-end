import React, { Component } from 'react';
import { Modal, Form, Input, InputNumber, Upload, Icon, Row, Col, Avatar, Card, Button, Select } from 'antd';

const { Meta } = Card;
const { Option } = Select;

const EditProfileCardForm = Form.create({ name: 'editProfileCard' })(
	class extends React.Component {
		componentDidMount() {
			if (this.props.profileData) {
				console.log('profile data: ', this.props.profileData);
				this.props.form.setFieldsValue({
					firstName: this.props.profileData.profile.name.first,
					middleName: this.props.profileData.profile.name.middle,
					lastName: this.props.profileData.profile.name.last,
					university: this.props.profileData.university.name,
					yearOfStudy: this.props.profileData.profile.year,
					studying: this.props.profileData.profile.course
				});
			}
		}


		//if there is a middle name present in the data being handed to the componentDidMount, then it can be added to first name

		render() {
			const { visible, form, onCancel, onCreate, uniData } = this.props;

			const { getFieldDecorator } = form;



			return (
				<Modal
					visible={visible}
					title={
						<h3
							style={{
								textAlign: 'center',
								letterSpacing: '0.3px',
								marginBottom: 0
							}}
						>
							EDIT PROFILE CARD
						</h3>
					}
					okText="Confirm"
					onCancel={onCancel}
					onOk={onCreate}
				>
					{/* <------------------ first name --------------------> */}

					<Form layout="vertical">
						<Form.Item label="First Name">
							{getFieldDecorator('firstName', {
								rules: [{ required: true, message: 'What is your name/s?' }]
							})(<Input placeholder="First Name" />)}
						</Form.Item>
						{/* <------------------ middle name --------------------> */}

						{/* <Form.Item label="Middle Name">
							{getFieldDecorator('middleName', {
								rules: [
									{ required: true, message: 'What is your middle name/s?' }
								]
							})(<Input placeholder="Middle Name" />)}
						</Form.Item> */}

						{/* <------------------ last name --------------------> */}
						<Form.Item label="Last Name">
							{getFieldDecorator('lastName', {
								rules: [{ required: true, message: 'What is your name/s?' }]
							})(<Input placeholder="Last Name" />)}
						</Form.Item>
						{/* <------------------ University --------------------> */}

						<Form.Item label="University">
							{getFieldDecorator('university', {
								rules: [{ required: true, message: 'What is your university?' }]
							})(
								<Select
									showSearch
									style={{ width: 200 }}
									placeholder="Select a University"
									optionFilterProp="children"
									onChange={this.props.handleChange}
									onFocus={this.props.handleFocus}
									onBlur={this.props.handleBlur}
									filterOption={(input, option) =>
										option.props.children
											.toLowerCase()
											.indexOf(input.toLowerCase()) >= 0
									}
								>
									{uniData.map((uni, index) => (
										<Option key={index} value={uni.name}>{uni.name}</Option>
									))}
								</Select>
							)}
						</Form.Item>

						{/* <------------------ Year of Study --------------------> */}

						<Form.Item label="Year of Study">
							{getFieldDecorator('yearOfStudy', {
								initialValue: 0,
								rules: [{ required: true }]
							})(<InputNumber min={1} max={10} />)}
							<span className="ant-form-text" />
						</Form.Item>

						{/* <------------------ Studying --------------------> */}

						<Form.Item label="Studying">
							{getFieldDecorator('studying', {
								rules: [{ required: true, message: 'What are you studying?' }]
							})(<Input placeholder="Studying" />)}
						</Form.Item>

						{/* <------------------ Photo --------------------> */}

						<Form.Item label="Profile Photo" />
						{this.props.editState === true ? (
							<div style={{ height: '170px' }}>
								<Form.Item>
									<div >
										{getFieldDecorator('photo', {
											valuePropName: 'fileList',
											getValueFromEvent: this.normFile,
											rules: [{ required: true }]
										})(
											<Upload.Dragger name="files" action="/upload.do">
												<p className="ant-upload-drag-icon">
													<Icon type="user" />
												</p>
												<p className="ant-upload-text">
													Click or drag file to this area to upload
												</p>
											</Upload.Dragger>
										)}
									</div>
								</Form.Item>
							</div>
						) : (
								<div style={{ height: '170px' }}>
									<div
										style={{ color: 'rgba(0, 0, 0, 0.85)', marginBottom: 20 }}
									/>
									<Row
										type="flex"
										justify="center"
										align="middle"
										style={{ margin: '20px 0' }}
									>
										<Col>
											<Meta
												/* title="Current Profile Photo" */
												avatar={
													<Avatar
														style={{
															width: 100,
															height: 100,
															display: 'flex',
															flexDirection: 'column',
															justifyContent: 'center',
															backgroundColor: 'rgba(0,0,0,0)',
															border: this.props.goldProfile
																? '3px solid gold '
																: '3px solid green'
														}}
													><Icon type="user" style={{ fontSize: '3rem', color: 'green' }} /></Avatar>
												}
											/>
										</Col>
									</Row>
								</div>
							)}

						{this.props.editState === true ? (
							<Row>
								<Col span={4} offset={10}>
									<Button
										block
										style={{ margin: 'auto' }}
										onClick={() => this.props.editToggle()}
									>
										Cancel
									</Button>
								</Col>
							</Row>
						) : (
								<Row>
									<Col span={4} offset={10}>
										<Button
											block
											style={{ margin: 'auto' }}
											onClick={() => this.props.editToggle()}
										>
											Edit
									</Button>
									</Col>
								</Row>
							)}
					</Form>
				</Modal>
			);
		}
	}
);

class EditProfileCardModal extends Component {
	state = {
		visible: false,
		edit: false,
		uniData: [
			{
				name: 'University of Hull'
			},
			{
				name: 'Kings College London'
			},
			{
				name: 'Cambridge University '
			}
		]
	};

	showModal = () => {
		this.setState({
			visible: true
		});
	};

	handleOk = e => {
		// console.log(e);
		this.setState({
			visible: false
		});
	};

	handleCancel = () => {
		this.props.closeModal('profileCardEdit');
	};

	editToggle = e => {
		// console.log(e);
		if (this.state.edit === false) {
			this.setState({
				edit: true
			});
		} else if (this.state.edit === true) {
			this.setState({
				edit: false
			});
		}
	};

	handleChange(value) {
		console.log(`selected ${value}`);
	}

	handleBlur() {
		console.log('blur');
	}
	handleFocus() {
		console.log('focus');
	}
	// confirmLoading={this.state.sending}
	// wrappedComponentRef={this.saveFormRef}

	render() {
		return (
			<div>
				<EditProfileCardForm
					visible={this.props.visible}
					onCancel={this.handleCancel}
					onCreate={this.handleCreate}
					profileData={this.props.profileData}
					editToggle={this.editToggle}
					editState={this.state.edit}
					handleChange={this.handleChange}
					handleBlur={this.handleBlur}
					handleFocus={this.handleFocus}
					uniData={this.state.uniData}
				/>
			</div>
		);
	}
}

export default EditProfileCardModal;
