import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Modal, Form, Input, Select, notification } from 'antd';
import FormHelperText from '../general/FormHelperText';
import Log from '../../functions/api/logs/Log';
import { UserContext } from '../providers/UserProvider';


const BibliographyCreateForm = Form.create({ name: 'form_in_modal' })(
	// eslint-disable-next-line
	class extends React.Component {
		render() {
			const { visible, onCancel, onCreate, form } = this.props;

			const { getFieldDecorator } = form;

			function handleChange(value) {
				console.log(`you selected: ${value}`);
			}

			const { Option } = Select;

			return (
				<Modal
					visible={visible}
					title={
						<h3
							style={{
								textAlign: 'center',
								letterSpacing: '0.3px',
								marginBottom: 0
							}}
						>
							CREATE NEW BIBLIOGRAPHY
						</h3>
					}
					okText="Create"
					onCancel={onCancel}
					onOk={onCreate}
				>
					<Form layout="vertical">
						<Form.Item label="Project">
							<FormHelperText text="Choose a project to attach this bibliography to. Or make it independent of a project." />
							{getFieldDecorator('essay', {
								initialValue: 'noneUnattached'
							})(
								<Select style={{ width: 200 }} onChange={handleChange}>
									<Option value="noneUnattached">None (unattached)</Option>
									<Option value="essayTitle1">Essay title 1</Option>
									<Option value="essayTitle2">Essay title 2</Option>
									{/* Disabled because it has a bibliography attached or created already. This can be checked with a bibliography id value on a project. */}
									<Option disabled value="essayTitle3">
										Essay title 3
									</Option>
								</Select>
							)}
						</Form.Item>

						{/* Need to input logic that means if a project is selected then it doesn't create a title for the bibliography */}
						<Form.Item label="Title">
							{getFieldDecorator('title', {
								rules: [
									{
										required: true,
										message: 'You have to input a title.'
									}
								],
								initialValue: ''
							})(
								<Input placeholder="Please enter the title of your Bibliography" />
							)}
						</Form.Item>

						<Form.Item label="Referencing Style">
							<FormHelperText text="Choose your referencing style here." />
							{getFieldDecorator('refStyle', {
								initialValue: 'Harvard'
							})(
								<Select style={{ width: 200 }} onChange={handleChange}>
									<Option value="Harvard">Harvard</Option>
									<Option value="APA">APA</Option>
									<Option value="MLA">MLA</Option>
									<Option disabled value="Chicago">
										Chicago (coming soon)
									</Option>
								</Select>
							)}
						</Form.Item>
					</Form>
				</Modal>
			);
		}
	}
);

class NewBibliographyModal extends Component {
	state = {
		sending: false
	};

	handleCancel = () => {
		this.props.closeModal('bib');
	};

	handleCreate = () => {
		const form = this.formRef.props.form;
		form.validateFields((err, values) => {
			if (err) {
				return;
			}
			setTimeout(() => {
				this.setState({
					sending: true
				});

				this.handleCancel();
				const { user } = this.context;
				const event = {
					type: 'create',
					message: (
						<p>
							You created the bibliography - <b>{values.title}</b>
						</p>
					),
					document: {
						type: 'bibliography',
            project_id: values.project_id ? values.project_id : null,
            project_title: values.title,
            title: values.title,
            reference_style: values.referenceStyle,

					}
				};
				const log = new Log({ event, type: 'create', user }).log();
				// Fire off the notification (passing in the notification object)
				// This is required to give a location to the notification
				log.notify(notification)();
				form.resetFields();
			}, 500);
		});
	};

	// handleCreate = () => {
	//   const form = this.formRef.props.form;

	//   // this.props.history.push(`/writer/id/note`);

	//   form.validateFields((err, values) => {
	//     if (err) {
	//       return;
	//     }
	//     setTimeout(() => {
	//       this.setState({
	//         sending: true
	//       });

	//       this.handleCancel();
	//       const { user } = this.context;
	//       const event = {
	//         type: 'create',
	//         message: <p>You created the project - <b>{values.title}</b></p>,
	//         document: {
	//           type: 'note',
	//           project_id: values.project_id ? values.project_id : null,
	//           title: values.title,
	//           tags: values.tags ? values.tags : null,
	//           style: values.style
	//         }}
	// 		  const log = new Log({ event, type: "create", user }).log();
	// 			// Fire off the notification (passing in the notification object)
	// 			// This is required to give a location to the notification
	// 			log.notify(notification)();
	// 			form.resetFields();
	//     }, 500);
	//   });
	// };

	saveFormRef = formRef => {
		this.formRef = formRef;
	};

	render() {
		return (
			<div>
				<BibliographyCreateForm
					wrappedComponentRef={this.saveFormRef}
					visible={this.props.visible}
					onCancel={this.handleCancel}
					onCreate={this.handleCreate}
					confirmLoading={this.state.sending}
				/>
			</div>
		);
	}
}





const WrappedBibliographyModal = withRouter(NewBibliographyModal);

WrappedBibliographyModal.WrappedComponent.contextType = UserContext;

export default WrappedBibliographyModal;



