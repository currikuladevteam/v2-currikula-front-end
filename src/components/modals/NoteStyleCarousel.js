import React, { Component } from "react";
import { Carousel } from "antd";

import NoteStyleCarouselPane from "./NoteStyleCarouselPane";

import bullets from "../../images/styles/bullets.svg";
import sentences from "../../images/styles/sentences.svg";
import cornell from "../../images/styles/cornell.svg";
import freeform from "../../images/styles/freeform.svg";

const referralLevels = [
  {
    style: "Sentence Method",
    background: "#8f00a0ad",
    component: (
      <img
        src={sentences}
        style={{ width: 130, height: 130, margin: "0 auto" }}
        alt="Sentence Method Icon"
      />
    ),
    description:
      "Possibly the simplest system this encourages concise sentences underneath topic headings to explain topics. There needs to be a minimum of three sentences for each topic in before you can create another topic header."
  },
  {
    style: "Outlining Method",
    background: "#004fa0ad",
    component: (
      <img
        src={bullets}
        style={{ width: 130, height: 130, margin: "0 auto" }}
        alt="Bullet Method Icon"
      />
    ),
    description:
      "More focused than the Sentence method, in this style you are aiming to extract the key information from topics into short bullet points. This type of notes is most effective for fact heavy note-taking sessions."
  },
  {
    style: "Cornell Method",
    background: "#03d071ad",
    component: (
      <img
        src={cornell}
        style={{ width: 130, height: 130, margin: "0 auto" }}
        alt="Cornell Method Icon"
      />
    ),
    description:
      "By dividing the page into notes (right), key notes (left) and summary (bottom), you are forced to engage with the subject, increasing retention and improving overall quality of your notes. Best for lecture notes."
  },
  {
    style: "Freeform",
    background: "#3ea000ad",
    component: (
      <img
        src={freeform}
        style={{ width: 130, height: 130, margin: "0 auto" }}
        alt="Freeform Icon"
      />
    ),
    description:
      "Make your notes your own way, choose between bullet points, headers, and standard text and craft in your own style."
  }
];

export default class NoteStyleCarousel extends Component {
  renderPanes() {
    return referralLevels.map(pane => {
      return (
        <div key={pane.style}>
          <NoteStyleCarouselPane
            level={pane.title}
            reward={pane.reward}
            style={pane.style}
            // background={pane.background}
            component={pane.component}
            description={pane.description}
          />
        </div>
      );
    });
  }

  render() {
    return (
      <Carousel ref={this.props.innerRef} afterChange={this.onChange}>
        {this.renderPanes()}
      </Carousel>
    );
  }
}
