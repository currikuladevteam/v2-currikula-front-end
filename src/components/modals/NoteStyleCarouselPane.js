import React, { Component } from 'react';

export default class NoteStyleCarouselPane extends Component {
    render() {
        const { component, style, description, background } = this.props;
        const paneStyle = {
            // height: '150px',
            background: background,
            textAlign: 'center',
            padding: '0px 40px',
            // color: 'white',
            borderRadius: 4
        }
        return (
            <div style={paneStyle}>
                <div style={{ padding: 0 }}>
                    {component}
                </div>
                <h1 style={{ }}>{style}</h1>
                {/* <h4 style={{ color: 'white' }}>{reward}</h4> */}
                <p>{description}</p>
            </div>
        )
    }
}
