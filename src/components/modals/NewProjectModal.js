import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import moment from 'moment';

import { Modal, Form, Input, DatePicker, InputNumber, Select, notification } from 'antd';
import FormHelperText from '../general/FormHelperText';
import { UserContext } from '../providers/UserProvider';
import Log from '../../functions/api/logs/Log';

const ProjectCreateForm = Form.create({ name: 'form_in_modal' })(
	// eslint-disable-next-line
	class extends React.Component {
		//Handle submit for date picker
		handleSubmit = e => {
			e.preventDefault();
			this.props.form.validateFields((err, fieldsValue) => {
				if (err) {
					return;
				}

				// Should format date value before submit.
				// const values = {
				// 	...fieldsValue,
				// 	'date-picker': fieldsValue['date-picker'].format('YYYY-MM-DD')
				// };
				// console.log('Received values of form1: ', values);
			});
		};

		// ----------- Fills in form with current project data if it is present in props ---------

		componentDidMount() {
			// console.log(this.props.CurrentProjectData);
			// console.log(a);
			if (this.props.CurrentProjectData) {
				this.props.form.setFieldsValue({
					title: this.props.CurrentProjectData.title,
					wordCount: this.props.CurrentProjectData.progress.words.target,
					'date-picker': moment(this.props.CurrentProjectData.due_date),
					referenceStyle: this.props.CurrentProjectData.referencing_style,
					subject: this.props.CurrentProjectData.subject,
					topic: this.props.CurrentProjectData.topic
				});
			}
		}

		render() {
			//for the wordcount
			const { Option } = Select;

			const number = this.props.number;
			// const tips = 'Please enter your projects word count';

			const { visible, onCancel, onCreate, form } = this.props;

			const { getFieldDecorator } = form;

			//picker destructuring
			// const { MonthPicker, RangePicker } = DatePicker;

			//config vars for form items
			const dateConfig = {
				rules: [
					{
						type: 'object',
						required: true,
						message:
							"Please select due date, if you don't know precisely, please estimate to the best of your ability"
					}
				]
			};

			const titleConfig = {
				rules: [
					{ required: true, message: 'Please input a title of the project' }
				]
			};

			// const formItemLayout = {
			//   labelCol: {
			//     xs: { span: 24 },
			//     sm: { span: 12 }
			//   },
			//   wrapperCol: {
			//     xs: { span: 24 },
			//     sm: { span: 12 }
			//   }
			// };

			// var inputProps = {
			//   value: 'foo',
			//   onChange: this.handleChange
			// };

			// if (condition) inputProps.disabled = true;

			// <input
			//     value="this is overridden by inputProps"
			//     {...inputProps}
			//     onChange={overridesInputProps}
			//  />

			return (
				<Modal
					visible={visible}
					title={
						<h3
							style={{
								textAlign: 'center',
								letterSpacing: '0.3px',
								marginBottom: 0
							}}
						>
							CREATE NEW PROJECT
						</h3>
					}
					okText="Create"
					onCancel={onCancel}
					onOk={onCreate}
				>
					{/* ------------ title ---------- */}
					<Form layout="vertical">
						<Form.Item label="Title">
							{getFieldDecorator('title', titleConfig)(
								<Input placeholder="Please enter the title of your new project" />
							)}
						</Form.Item>

						{/* ------------- data ---------- */}
						<Form.Item label="Due Date">
							<FormHelperText text="Set your assignment due date here." />
							{getFieldDecorator('date-picker', dateConfig)(<DatePicker />)}
							{/* {console.log(this.props.form.getFieldValue('date-picker'))} */}
						</Form.Item>

						{/* ------word-count ------- */}

						<Form.Item
							label="Word Count"
							validateStatus={number.validateStatus}
							help={number.errorMsg}
						>
							<FormHelperText text="Set your projects total word count here. This helps keep track of your progress." />
							{getFieldDecorator('wordCount', {
								setFieldsValue: number.value,
								rules: [
									{
										required: true,
										message: 'Please enter the subject of your assignment'
									}
								]
							})(
								<InputNumber
									min={500}
									max={50000}
									onChange={this.props.handleNumberChange}
								/>
							)}
						</Form.Item>

						{/* -------------------------------------------------ref style ------------------------------------------------------------- */}

						<Form.Item
							label="Referencing style"
						/* validateStatus="success" */
						>
							<FormHelperText text="Choose your referencing style here." />

							{getFieldDecorator('referenceStyle', {
								initialValue: 'Harvard'
							})(
								<Select style={{ width: 200 }}>
									<Option value="harvard">Harvard</Option>
									<Option value="mla">MLA</Option>
									<Option value="apa">APA</Option>
									<Option disabled value="chicago">
										Chicago (coming soon)
									</Option>
								</Select>
							)}
						</Form.Item>
						{/* -------------------------------------------------Subject ------------------------------------------------------------- */}

						<Form.Item label="Subject">
							{getFieldDecorator('subject', {
								rules: [
									{
										required: false,
										message: 'Please enter the subject of your assignment'
									}
								]
							})(<Input placeholder="What is the subject?" />)}
						</Form.Item>

						{/* -------------------------------------------------Topic ------------------------------------------------------------- */}

						<Form.Item label="Topic">
							{getFieldDecorator('topic', {
								rules: [
									{
										required: false,
										message: 'Please enter the topic of your assignment'
									}
								]
							})(<Input placeholder="What is the topic?" />)}
						</Form.Item>
					</Form>
				</Modal>
			);
		}
	}
);

function validateWordCount(number) {
	if (number > 1) {
		return {
			validateStatus: 'success',
			errorMsg: null
		};
	}
	return {
		validateStatus: 'error',
		errorMsg: 'Please enter a number'
	};
}

class NewProjectModal extends Component {
	state = {
		sending: false,
		number: {
			value: 10
		}
	};

	// showModal = () => {
	//   this.setState({ visible: true });
	// }

	handleCancel = () => {
		this.props.closeModal('project');
	};

	handleCreate = () => {
		const form = this.formRef.props.form;
		form.validateFields((err, values) => {
			if (err) {
				return;
			}
			setTimeout(() => {
				this.setState({
					sending: true
				});
				this.handleCancel();

				const { user } = this.context;

				const event = {
					type: 'create',
					message: <p>You created the project - <b>{values.title}</b></p>,
					document: {
						type: 'project',
						project_title: values.title,
						subject: values.subject,
						topic: values.topic,
						reference_style: values.referenceStyle,
						word_count: values.wordCount
					}
				}

				const log = new Log({ event, type: "create", user }).log();
				// Fire off the notification (passing in the notification object)
				// This is required to give a location to the notification
				log.notify(notification)();
				form.resetFields();
				// Redirect to the page with the new project
				// this.props.history.push(`/writer/id/project`);
			}, 500);
		});
	};

	saveFormRef = formRef => {
		this.formRef = formRef;
	};

	handleNumberChange = value => {
		this.setState({
			number: {
				...validateWordCount(value),
				value
			}
		});
		// console.log(value)
	};

	render() {
		return (
			<div>
				<ProjectCreateForm
					wrappedComponentRef={this.saveFormRef}
					visible={this.props.visible}
					onCancel={this.handleCancel}
					onCreate={this.handleCreate}
					number={this.state.number}
					handleNumberChange={this.handleNumberChange}
					confirmLoading={this.state.sending}
					CurrentProjectData={this.props.projectData}
				/>
			</div>
		);
	}
}



const WrappedProjectModal = withRouter(NewProjectModal);

WrappedProjectModal.WrappedComponent.contextType = UserContext;

export default WrappedProjectModal;

