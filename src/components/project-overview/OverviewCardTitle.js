import React, { Component } from 'react';


 class OverviewCardTitle extends Component {
  render() {
    return (
        <h3>{this.props.title}</h3>
    );
  }
}
export default OverviewCardTitle;