import React, { Component } from "react";
import { Icon, Card } from "antd";
import { withRouter } from "react-router-dom";

const disabledCSS = {
  opacity: 0.3,
};

const style = {
  width: "100%",
  padding: "0px !important",
  cursor: "default",
};

const bodyStyle = {
  padding: "10px 0",
  textAlign: "center",
  fontWeight: "600"
}

class MainDocumentCard extends Component {
  openEdit = () => {
    if (!this.props.disabled) {
      const id = this.props.document.id
      const type =this.props.document.document.type
      this.props.history.push(`/writer/${id}/${type}`);
    }
  };
  openPreview = (document) => {
    if (!this.props.disabled) {
      this.props.openPreview(this.props.document.document)
    }
  };
  render() {
    return (
      <Card
        className="mainDocumentCards"
        hoverable={!this.props.disabled}
        size="small"
        style={this.props.disabled ? { ...style, ...disabledCSS } : style}
        bodyStyle={bodyStyle}
        cover={
          <Icon 
              onClick={() =>this.openEdit()}
              type={this.props.icon}
              style={{
              fontSize: '3rem', 
              padding: "30px 0 0 0",
              margin: "0 auto",
              cursor: this.props.disabled ? "default" : "pointer"}} />
        }
        actions={[
          <Icon
            onClick={() =>
              this.openEdit()
            }
            style={{ margin: 1, cursor: this.props.disabled ? "default" : "pointer" }}
            type="edit"
          />,
          <Icon style={{ margin: 1, cursor: this.props.disabled ? "default" : "pointer" }}
            onClick={() => this.openPreview()}
            type="eye" />
        ]}
      >
        <span style={{fontSize: 11}}>{this.props.documentTitle}</span>
      </Card>
    );
  }
}

export default withRouter(MainDocumentCard);
