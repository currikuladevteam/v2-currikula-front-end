import React, { Component } from "react";
import { Col, Row } from "antd";
import MainDocumentCard from "./MainDocumentCard";
import PlanIcon from "../../images/overview/plan.svg";
import DraftIcon from "../../images/overview/draft.svg";
import BibIcon from "../../images/overview/bibliography-overview.svg";
import PreviewDrawer from "../preview/PreviewDrawer";
import { getDraft } from "../../demo-data/fake-apis/drafts.api.js";
import { getBibliography } from "../../demo-data/fake-apis/bibliographies.api";

class OverviewMainDocuments extends Component {
  constructor(props) {
    super(props)
    this.state = {
      disableDocumentCards: true
    }
  }

  child = React.createRef();

  openPreview = doc => {
    this.child.current.showDrawer(doc);
  };

  openResourcePreview = docType => {
    if (docType === "draft") {
      this.props.openResourcePreview(this.state.draft.document.value);
    } else if (docType === "bibliography") {
      this.props.openResourcePreview(this.state.draft);
    }
  };


  componentDidMount() {
    const { project } = this.props;
    if (project && !this.state.draft && !this.state.bibliography) {
      getDraft(project.draft.id).then(draft => {
        getBibliography(project.bibliography.id).then(bibliography => {
          this.setState({
            bibliography: bibliography[0],
            draft: draft[0],
            disableDocumentCards: false
          });
        });
      });
    }

  }
  componentDidUpdate() {
    const { project } = this.props;
    if (project && !this.state.draft && !this.state.bibliography) {
      getDraft(project.draft.id).then(draft => {
        getBibliography(project.bibliography.id).then(bibliography => {
          this.setState({
            bibliography: bibliography[0],
            draft: draft[0],
            disableDocumentCards: false
          });
        });
      });
    }
  }

  
  render() {
    return (
      <div>
        <Row gutter={20}>
          <Col span={8}>
            <MainDocumentCard
              openPreview={
                this.props.openResourcePreview
                  ? this.openResourcePreview
                  : this.openPreview
              }
              disabled={true}
              documentTitle="PLAN"
              imageSrc={PlanIcon}
              icon="database"
              imageAlt="Plan Icon"
              document={""}
            />
          </Col>
          <Col span={8}>
            <MainDocumentCard
              disabled={this.state.disableDocumentCards}
              openPreview={
                this.props.openResourcePreview
                  ? () => this.openResourcePreview("draft")
                  : this.openPreview
              }
              documentTitle="DRAFT"
              imageSrc={DraftIcon}
              icon="file-text"
              imageAlt="Draft Icon"
              document={this.state.draft}
            />
          </Col>
          <Col span={8}>
            <MainDocumentCard
              disabled={this.state.disableDocumentCards}
              openPreview={
                this.props.openResourcePreview
                  ? () => this.openResourcePreview("bibliography")
                  : this.openPreview
              }
              documentTitle="BIBLIOGRAPHY"
              imageSrc={BibIcon}
              icon="read"
              imageAlt="Bibliography Icon"
              document={this.state.bibliography}
            />
          </Col>
        </Row>
        <PreviewDrawer ref={this.child} />
      </div>
    );
  }
}

export default OverviewMainDocuments;
