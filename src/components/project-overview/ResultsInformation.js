import React, { Component } from 'react';


class Results extends Component {  

  render() {
    return (
      <React.Fragment>
        <p>Here you can input the results for you assignment. The more information you provide about how it went, what your markers feedback was and what they think you can improve on will help us create better services for you. Allowing us to one day check for those things before you hand in your assignment.</p>
        <p>Entering your results here will also allow them to be automatically populated in the Grade Calculator. This will help you keep track of how you are doing during the academic year and during your time in higher education. The Grade Calculator can also be used to help you figure out what results you need to get in future assignments and exams in order to get your desired grade at the end of the year.</p>
      </React.Fragment>
    );
  }
}


export default Results;