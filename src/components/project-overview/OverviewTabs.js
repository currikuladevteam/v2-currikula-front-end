import React, { Component } from 'react';
import { Card, Tabs } from 'antd';
import Timeline from './TimeLine'
import ResultsInformation from './ResultsInformation';
import WrappedResultInput from '../forms/ResultInput'

class OverviewTabs extends Component {
  render() {
    if (!this.props.timeLine) {
      return <Card loading={true} style={{ height: 720 }} />
    }
    return (
      <Card style={{ height: 720 }}>
        <Tabs>
          <Tabs.TabPane key="1" tab="PROJECT TIMELINE" >
            <h3 style={{ textAlign: 'center', marginBottom: '20px', letterSpacing: '0.3rem' }}>
              PROJECT TIMELINE
            </h3>
            <Timeline timeLine={this.props.timeLine} />
          </Tabs.TabPane>
          <Tabs.TabPane key="2" tab="RESULTS" disabled={this.props.exportedAt ? false : true}>
            <h3 style={{ textAlign: 'center', marginBottom: '20px', letterSpacing: '0.3rem' }}>
              RESULTS
            </h3>
            <div style={{ height: 580, overflow: 'scroll', padding: 2 }}>
              <ResultsInformation />
              <WrappedResultInput />

            </div>

          </Tabs.TabPane>
        </Tabs>
      </Card>
    );
  }
}


export default OverviewTabs;