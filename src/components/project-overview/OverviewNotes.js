import React, { Component } from "react";
import { Icon, List, Skeleton, Avatar, Row, Button } from "antd";
import PreviewDrawer from "../preview/PreviewDrawer";
import { Link } from "react-router-dom";
import NewNoteModal from '../modals/NewNoteModal';
import { UserContext } from '../providers/UserProvider'


class OverviewNotes extends Component {
  constructor(props) {
    super(props);
    this.child = React.createRef();
    this.state = {
      newNoteModalVisible: false,
      sorted: true,
      loadingNotes: true,
      notes: ["",""]
    };
  }
  openPreview = note => {
    this.child.current.showDrawer(note);
  };

  handleSortClick() {
    this.setState({ sorted: !this.state.sorted });
  }

  renderSort(sorted) {
    const divStyle = {
      minWidth: 50,
      display: "inline-block",
      textAlign: "center"
    };

    const selected = {
      fontWeight: 800
    };

    const containerDivStyle = {
      float: "right",
      fontSize: 12,
      display: "inline-block",
      padding: 10
    };
    return (
      <div onClick={() => this.handleSortClick()} style={containerDivStyle}>
        <div
          style={
            !sorted
              ? { ...selected, ...divStyle, ...{ minWidth: 70 } }
              : { ...divStyle, ...{ minWidth: 70 } }
          }
        >
          RECENT
        </div>
        <span> | </span>
        <div style={sorted ? { ...selected, ...divStyle } : divStyle}>A-Z</div>
      </div>
    );
  }

  openModal = (modal) => {
    if (modal === 'note') {
      this.setState({
        newNoteModalVisible: true
      })
    }
  }

  getNotes() {
    const { notes } =  this.context;
    const { projectId } = this.props;
    const endOfNotes = {endOfNotes: true}
    if (notes) {
      const projectNotes = notes.filter(note => note.project_id === projectId)
      if (!this.state.sorted) {
        return [...projectNotes, endOfNotes]
      } else {
        return [...projectNotes, endOfNotes]
      }
    } else {
      return this.state.notes
    }
  }

  renderNotes = () => {
    const { notes } = this.context;
    return note => {
      if (note.deleted) return <div></div>;
      if (note.endOfNotes) return <div style={{ textAlign: 'center', marginTop: 10 }}> End of Project Notes</div>;
      return(
      <List.Item
        actions={[
          <Icon onClick={() => this.openPreview(note)} type="eye" />
        ]}
      >
        <Skeleton
          avatar
          title={false}
          loading={!notes}
          active
        >
          <Link to={`/writer/${note.id}/note`}>
            <List.Item.Meta
              avatar={
                <Avatar
                  style={{ backgroundColor: "rgba(0,0,0,0)", color: 'black'}}
                  icon="profile"
                  size="small"
                />
              }
              title={<p>{note.title}</p>}
            />
          </Link>
        </Skeleton>
      </List.Item>
    )}
  }

  closeModal = (modal) => {
   if (modal === 'note') {
      this.setState({
        newNoteModalVisible: false
      })
  }
}
  render() {
    return (
      <React.Fragment>
      <NewNoteModal openModal={this.openModal} closeModal={this.closeModal} visible={this.state.newNoteModalVisible} />
        <Row style={{ marginBottom: 10 }}>
          <Button 
          onClick={() => this.openModal('note')} 
          icon="profile" 
          style={{ float: "left" }}
          >
            NEW NOTE
          </Button>
          {this.renderSort(this.state.sorted)}
        </Row>
        <List
          itemLayout="horizontal"
          dataSource={ this.getNotes()} 
          style={{ height: 200, overflow: "scroll" }}
          renderItem={this.renderNotes()}
        />
        <PreviewDrawer ref={this.child} />
      </React.Fragment>
    );
  }
}
OverviewNotes.contextType = UserContext;

export default OverviewNotes;


