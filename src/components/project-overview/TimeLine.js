import React, { Component } from 'react';
import { Timeline, Icon } from 'antd';
import { timeSince, timeTill } from '../../functions/helpers/Time';
import SubTimeLineItem from './SubTimeLineItem'


const oneDay = 24 * 60 * 60 * 1000;

class TimeLine extends Component {
  renderEvents(events) {
    return events.map((event, index) => {
      return <li
        key={index}
      >
        <Icon type="minus-circle"
        />{event.title}</li>
    })

  }

  renderTimeLine(timeLineData) {
    return timeLineData.map((item, index) => {
      return <Timeline.Item
        color="green"
        key={index}
      >
        <b>{timeSince(item.time_stamp)}</b>

        <SubTimeLineItem
          events={item.events}
          rightAlign={index % 2 === 0}
        />
      </Timeline.Item>
    })
  }


  render() {
    return (
      <React.Fragment>
        <Timeline mode="alternate" style={{ height: 580, overflow: 'scroll', padding: 10 }} >
          {this.renderTimeLine(this.props.timeLine)}
          <Timeline.Item>
            <b>{timeTill(new Date().getTime() + 2 * oneDay)}</b>
          </Timeline.Item>
        </Timeline>
      </React.Fragment>
    );
  }
}


export default TimeLine;