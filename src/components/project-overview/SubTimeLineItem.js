import React, { Component } from 'react'
import { Icon } from 'antd';



const colorSelector = {
    creation: 'blue',
    edit: '#00a598',
    delete: 'red',
    finish: 'green',
    important: '#ffb500',
    minor: 'grey'
  }


export default class SubTimeLineItem extends Component {


    renderEvents(events) {
        return events.map((event, index) => {
            return <li
                key={index}
            >
                {this.props.rightAlign ? 
                <Icon 
                style={{marginRight: 10, color: colorSelector[event.type]}}
                type="right-circle" /> : ''}
                {event.title}
                {this.props.rightAlign ? "" : 
                <Icon 
                style={{marginLeft: 10, color: colorSelector[event.type]}}
                type="left-circle" />}
            </li>
        })

    }
    render() {
        return (
            <ul style={{ listStyle: 'none', paddingLeft: 0 }}>
                {this.renderEvents(this.props.events)}
            </ul>
        )
    }
}
