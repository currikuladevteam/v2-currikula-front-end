import React, { Component } from 'react';
import { Card, Icon, Divider, Row, Col } from 'antd';
import ProgressBars from '../general/ProgressBars';
import OverviewMainDocuments from './OverviewMainDocuments';
import OverviewNotes from './OverviewNotes';

// modals
import NewProjectModal from '../modals/NewProjectModal';

class ProjectOverviewExpandedCard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			project: null,
			newProjectModalVisible: false
		};
	}

	// Key function to update component with API Request Data
	// Must check to see if updating the same data otherwise will loop indefinitely
	componentDidUpdate() {
		if (this.state.project !== this.props.project) {
			this.setState({
				project: this.props.project
			});
		}
	}

	openModal = modal => {
		if (modal === 'project') {
			this.setState({
				newProjectModalVisible: true
			});
		}
	};
	closeModal = modal => {
		if (modal === 'project') {
			this.setState({
				newProjectModalVisible: false
			});
		}
	};


  openModal = (modal) => {
    if (modal === 'project') {
      this.setState({
        newProjectModalVisible: true
      })
    }
  }
  closeModal = (modal) => {
    if (modal === 'project') {
      this.setState({
        newProjectModalVisible: false
      })
    }
  }


  render() {
		if (!this.state.project) 
		return <Card style={{ height: 720 }} loading={true} />;
    const titleCss = {
      width: '100%',
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    }
    return (
      <Card
        loading={!this.state.project}
        style={{ height: 720 }}
      >
        <NewProjectModal
          projectData={this.props.project}
          openModal={this.openModal}
          closeModal={this.closeModal}
          visible={this.state.newProjectModalVisible} />
        <Row>
          <Col span={23}>
            <h2 style={titleCss}>{this.state.project ? this.state.project.title : ""}</h2>
          </Col>
          <Col span={1}>
            <Icon
              onClick={() => this.openModal('project')}
              style={{ margin: 1, color: 'green' }}
              type="form"
              theme="outlined" />
          </Col>
        </Row>
        <Divider style={{ marginTop: 5 }} />
        <ProgressBars progress={this.state.project.progress} />
        <Divider style={{ marginTop: 20 }}>MAIN DOCUMENTS</Divider>
        <OverviewMainDocuments
          project={this.state.project}
          draft={this.state.project.draft}
          bibliography={this.state.project.bibliography} />
        <Divider>NOTES</Divider>
        <OverviewNotes projectId={this.state.project.id} />
      </Card>
    );
  }
}

export default ProjectOverviewExpandedCard;
