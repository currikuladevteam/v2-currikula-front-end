import React, { Component } from 'react';
import { List, Card, Row, Col, Icon, Button, Divider } from 'antd'

export default class PlagiarismCard extends Component {
    render() {
        const cardCard = {
            backgroundColor: '#F8F8F8',
            borderRadius: 4,
            padding: 5

        }
        const plagi = this.props.plagiarismData;
        return (
            <List.Item
                hoverable="true"
                className="wider-list-item"
                style={{
                    margin: '8px',
                    padding: '5px',
                    borderRadius: 2,
                    borderBottom: 'none'
                }} >
                <Card style={cardCard}
                    hoverable
                    bodyStyle={{ padding: 0 }}
                >
                    <Row style={{ margin: '5px 0', display: 'flex', flexDirection: 'vertical' }}>
                        <Col span={2} className="vert-center" style={{ justifyContent: 'center' }}>
                            <Icon type="user" />
                        </Col>
                        <Col span={22}>
                            <p style={{ margin: 0, fontSize: 9, padding: '0 5px' }}>{plagi.flaggedText}</p>

                        </Col>
                    </Row>

                    <Row>
                        <Col span={2} className="vert-center">
                            {plagi.type === "web" ? <Icon type="global" /> : plagi.type === "academic" ? <Icon type="book" /> : null}
                        </Col>
                        <Col span={22}>
                            <p style={{ margin: 0, fontSize: 9, padding: '0 5px' }}>{plagi.sourceText}</p>
                        </Col>
                    </Row>
                    <Divider style={{ margin: '6px 0' }} />
                    <Row>
                        <Col span={2} className="vert-center">
                            <Icon type="search" />
                        </Col>
                        <Col span={18}>
                            {/* Bottom With Buttons */}
                            <p style={{ margin: 0, fontSize: 9, padding: '0 5px' }}>{plagi.compiledReference || plagi.adviceText}</p>
                        </Col>
                        <Col span={4} style={{ float: 'right', textAlign: 'center' }}>
                            <Row>
                                <Button style={{ margin: 2 }} size="small" type="primary" shape="circle" icon="check" />
                                <Button style={{ margin: 2 }} size="small" type="danger" shape="circle" icon="delete" />
                            </Row>
                        </Col>
                    </Row>
                </Card>
            </List.Item>
        )
    }
}
