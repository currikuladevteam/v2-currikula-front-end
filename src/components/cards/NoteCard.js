import React, { Component } from "react";
import { Card, Icon, Menu, Modal, Tag, notification } from "antd";
import { Link } from "react-router-dom";
import EllipsesMenu from "../general/EllipsesMenu";
import EllipsesMenuMetaData from "../general/EllipsesMenuMetaData";
import Log from '../../functions/api/logs/Log'
import { UserContext } from "../providers/UserProvider";
import TimeLine from "../../functions/api/timeline/TimeLine";

const confirm = Modal.confirm;

class NoteCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      note: {
        id: "id",
        title: "Lecture 4 - History Ethics"
      }
    };
  }
  renderMenu() {
    const { created_at, edited_at } = this.props.note;
    const { projects, projectId, note } = this.props;
    return (
      <Menu style={{ width: 200 }}>
        <Menu.Item key="0">
          <Link to={`/writer/${note ? note.id : "id"}/note`}>
            Open Note
          </Link>
        </Menu.Item>
        <Menu.Item onClick={() => this.showConfirm()} key="1">
          Delete Note
        </Menu.Item>
        <Menu.Divider />
        <EllipsesMenuMetaData
          projectId={projectId}
          createdAt={created_at}
          editedAt={edited_at}
          projects={projects}
        />
      </Menu>
    );
  }

  openEdit = id => {
    this.props.history.push(
      `/writer/${this.props.id ? this.props.id : "id"}/note`
    );
  };

  showConfirm = () => {
    const { deleteNote, projects, note } = this.props;
    const deletedNote = { ...this.props.note, ...{ deleted: true } }
    const project = projects.filter(project => deletedNote.project_id === project.id )
    const { user } = this.context;
    // Logging Event 
    const event = {
      type: 'note',
      message: <p>You deleted the {note.document.type} - <b>{note.title}</b></p>,
      document: {
        ...deletedNote,
        ...deletedNote.document,
        project_title: project.length > 0 ? project[0].title : null
      }
    }
    confirm({
      title: `Do you want to delete this note?`,
      content: (
        <span>
          Deleting <b>{this.props.note.title}</b> will be a permanent action.
        </span>
      ),
      onOk() {
        return new Promise((resolve, reject) => {
          setTimeout(() => {

            // Make request to server to delete note
            // Make request to local cache to update
            resolve();
          }, 1000)
        })
          .then((error) => {
            if (error) console.log(error)
            // Log the events and the type of events
            const log = new Log({event, type: "delete", user }).log(); 

            // Send the log to the Time Line Create event function to add to project if there is a project
            TimeLine.createEvent(log)
            
            // Fire off the notification (passing in the notification object)
            // This is required to give a location to the notification
            log.notify(notification)();

            // Delete the note on the front end
            deleteNote(deletedNote) // Delete the Note
          })
          .catch(error => {
            
            // If there is an error log the event and attempt
            const log = new Log({error, event}, "error").log(); // log the events and the type of events
            log.notify(notification)();
            return false
          })
      },
      onCancel() { }
    });
  };


  getExtraIcons() {
    const { note } = this.props;
    const FolderIcon = <Icon type="folder" theme="filled" /> 
    const CompletedIcon = <Icon type="check"/> 
    const wrapperStyle = {marginRight: -10, cursor: 'default', marginLeft: 5}
    if (note.project_id && note.project_completed) {
      return <div style={wrapperStyle}>
        {CompletedIcon} | {FolderIcon}
      </div>
    } else if (note.project_id) {
      return <div
      style={wrapperStyle}
      >{FolderIcon}</div>
    } else {
      return <div style={{height: 21, width: 1}}></div>
    }
  }

  render() {
    const cardStyle = {
      width: "50",
      height: "50",
      marginTop: "10px",
      cursor: "default"
    };
    const completedCardStyle = {
      opacity: '0.6'
    };
    const IconSize = {
      textSize: "10px"
    };
    // At a later date this will be the id of the note
    const { note, loading, title, document } = this.props;
    return (
      <Card
        loading={loading}
        style={!loading ? note.project_completed ? { ...completedCardStyle, ...cardStyle } : cardStyle : {}}
        title={
          !loading ?
            <Link
              to={`/writer/${note ? note.id : "id"}/note`}>
              <h5 className="ellipsis"
                style={{ margin: 0 }}>{title}</h5>
            </Link> : "Loading..."
        }
        hoverable
        extra={!loading ? this.getExtraIcons() : <div  />}
        size="small"
        bodyStyle={{ padding: 0 }}
        actions={!loading ? [
          <Link to={`/writer/${note ? note.id : "id"}/note`}>
            <Icon type="edit" theme="filled" />
          </Link>,
          <Icon
            onClick={() => this.props.preview(note)}
            type="eye"
            theme="filled"
          />,
          <EllipsesMenu menu={this.renderMenu()}>
            <Icon style={IconSize} type="ellipsis" />
          </EllipsesMenu>
        ] : ''}
      >
        <div
          style={{
            padding: 10,
            height: 70,
            overflow: "scroll"
          }}
        >
          {!loading && document.tags ? document.tags.map((tag, index) => {
            return (
              <Tag key={index} style={{ margin: 4 }} color={"red"}>
                {tag}
              </Tag>
            );
          }) : <h4>No Tags</h4>}
        </div>
      </Card>
    );
  }
}

NoteCard.contextType = UserContext;

export default NoteCard;
