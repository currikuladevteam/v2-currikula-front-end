import React, { Component } from 'react';
import { Card, Icon, Menu, Modal, notification } from 'antd';
import { Link, withRouter } from 'react-router-dom';
import EllipsesMenu from "../general/EllipsesMenu";
import EllipsesMenuMetaData from "../general/EllipsesMenuMetaData";
import { UserContext } from '../providers/UserProvider';
import TimeLine from '../../functions/api/timeline/TimeLine'
import Log from '../../functions/api/logs/Log'

const { confirm } = Modal;

class BibliographyCard extends Component {

  showConfirm = () => {
    const { deleteBibliography, bibliography } = this.props;
    const deletedBibliography = { ...this.props.bibliography, ...{ deleted: true } }
    const { user, projects } = this.context; 
    const project = projects.filter(project => deletedBibliography.project_id === project.id )

    const event = {
      type: 'bibliography',
      message: <p>You deleted the {bibliography.document.type} - <b>{bibliography.title}</b></p>,
      document: {
        ...deletedBibliography,
        ...deletedBibliography.document,
        project_title: project.length > 0 ? project[0].title : null
      }
    }


    confirm({
      title: `Do you want to delete this bibliography?`,
      content: (
        <span>
          Deleting <b>{this.props.bibliography.title}</b> will be a permanent action.
        </span>
      ),
      onOk() {
        return new Promise((resolve, reject) => {
          setTimeout(() => {

            // Make request to server to delete note
            // Make request to local cache to update
            resolve();
          }, 1000)
        })
          .then((error) => {
            if (error) console.log(error)
            // Log the events and the type of events
            const log = new Log({event, type: "delete", user }).log(); 

            // Send the log to the Time Line Create event function to add to project if there is a project
            TimeLine.createEvent(log)
            
            // Fire off the notification (passing in the notification object)
            // This is required to give a location to the notification
            log.notify(notification)();

            // Delete the note on the front end
            deleteBibliography(deletedBibliography) // Delete the Note
          })
          .catch(error => {
            
            // If there is an error log the event and attempt
            const log = new Log({error, event}, "error").log(); // log the events and the type of events
            log.notify(notification)();
            return false
          })
      },
      onCancel() { }
    });
  };


  renderMenu() {
    if (!this.props.bibliography) return <div />
    const { created_at, edited_at, id, project_id } = this.props.bibliography;
    return (
      <Menu style={{ width: 200 }}>
        <Menu.Item key="0">
          <Link to={`/writer/${id}/bibliography`} >Open Bibliography</Link>
        </Menu.Item>
        {project_id ?
         <Menu.Item key="1">
         <Link to={`/projects/${project_id}/overview`} >View Project</Link>
        </Menu.Item> : ''}
        {!project_id ?
          <Menu.Item
            onClick={() => this.showConfirm()}
            key="2"
          >
            Delete Bibliography
        </Menu.Item> : ''
        }
        <Menu.Divider />
        <EllipsesMenuMetaData
          createdAt={created_at}
          editedAt={edited_at}
        // Should set up a  completed date here so that if the project is finished it gives you an export date.
        // exportedAt={completed ? exportedAt : null}
        />
      </Menu>
    )
  }

  getExtraIcons() {
    const { bibliography } = this.props;
    const FolderIcon = <Icon type="folder" theme="filled" /> 
    const CompletedIcon = <Icon type="check"/> 
    const wrapperStyle = {marginRight: -10, cursor: 'default'}
    if (bibliography.project_id && bibliography.project_completed) {
      return <div style={wrapperStyle}>
        {CompletedIcon} | {FolderIcon}
      </div>
    } else if (bibliography.project_id) {
      return <div
      style={wrapperStyle}
      >{FolderIcon}</div>
    } else {
      return <div></div>
    }
  }


  openEdit = (id) => {
    this.props.history.push(`/writer/${id}/bibliography`);
  }

  render() {
    const { bibliography, loading, title } = this.props;
    const cardStyle = {
      width: '100%',
      maxWidth: '500px',
      margin: '10px auto'
    };

    const completedCardStyle = {
      opacity: '0.6'
    };

    return (
      <Card 
        style={!loading ? bibliography.project_completed ? { ...completedCardStyle, ...cardStyle } : cardStyle : {}}
        title={
          <div
            className="ellipsis"
            onClick={() => this.openEdit(bibliography.id)}>
            {`BIBLIOGRAPHY: ${bibliography.title || title}`}
          </div>
        }
        bodyStyle={{cursor: 'default'}}
        hoverable
        extra={!loading ? this.getExtraIcons() : <div />}
        actions={
          !loading ? [
            <Link to={`/writer/${bibliography.id}/bibliography`}><Icon type="edit" theme="filled" /></Link>,
            // <Icon onClick={() => this.props.preview(bibliography)} type="eye" theme="filled" />,
            <EllipsesMenu menu={this.renderMenu()} >
              <Icon style={{ fontSize: 15 }} type="ellipsis" />
            </EllipsesMenu>
          ] : ''}
      >
        <div style={{ display: 'flex', justifyContent: 'space-around' }}>
          <div><Icon type="file-search" /> {bibliography.document.referencing_style.toUpperCase()}</div>
          <div><Icon type="search" /> {bibliography.document.reference_ids.length} references</div>
        </div>
      </Card >
    );
  }
}

const WrappedBibliographyCard = withRouter(BibliographyCard);

WrappedBibliographyCard.WrappedComponent.contextType = UserContext;

export default WrappedBibliographyCard
