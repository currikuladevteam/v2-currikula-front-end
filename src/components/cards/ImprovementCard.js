import React, { Component } from 'react';
import { List, Card, Row, Col, Icon, Button, Divider } from 'antd'
import { unSnakeCase, processEmphasis } from '../../functions/helpers/TextHelpers';


const typeIcons = {
    spelling: "API",
    grammar: "API",
    referencing: "search",
    content: "build",
    structure: "layout",
    argument: "fork"
}

const priorityResponses = [
    "General Advice: Highly recommend looking into this issue.",
    "General Advice: We recommend looking into potential issue.",
    "General Advice: Would be good to analyse the potential issue.",
    "General Advice: Check for changes you could make to improve this.",
    "General Advice: Have a quick look.",
]


export default class ImprovementCard extends Component {
    render() {
        const cardCard = {
            backgroundColor: '#F8F8F8',
            borderRadius: 4,
            padding: 5,
            minWidth: '100%'

        }
        const data = this.props.data;
        return (
            <List.Item
                hoverable="true"
                className="wider-list-item"
                style={{
                    margin: '8px',
                    padding: '5px',
                    borderRadius: 2,
                    borderBottom: 'none'
                }} >
                <Card style={cardCard}
                    hoverable
                    bodyStyle={{ padding: "5px 0 0 0" }}
                >   
                {data.feedback.original_text ? 
                <Row style={{ margin: '0 0 5px 0', display: 'flex', flexDirection: 'vertical' }}>
                <Col span={2} className="vert-center" style={{ justifyContent: 'center' }}>
                    <Icon type="user" />
                </Col>
                <Col span={22}>
                    <p style={{ margin: 0, fontSize: 9, padding: '0 5px', maxHeight: 100, overflow: 'scroll' }}>{processEmphasis(data.feedback.original_text)}</p>
                </Col>
            </Row> : ""
            }
                    
                    <Row>
                        <Col span={2} className="vert-center">
                        <Icon type={typeIcons[data.type] || "fire"} />
                        </Col>
                        <Col span={22}>
                            <p style={{ margin: 0, fontSize: 9, padding: '0 5px' }}>{data.feedback ? data.feedback.advice : ""}</p>
                        </Col>
                    </Row>
                    <Divider style={{ margin: '6px 0' }} />
                    <Row>
                        <Col span={2} className="vert-center">
                        <Icon type="exception" />
                           
                        </Col>
                        <Col span={18}>
                            {/* Bottom With Buttons */}
                            <h6 style={{marginBottom: 0}}>{data.error ? unSnakeCase(data.error) : "Error"}</h6>
                            <p style={{ margin: 0, fontSize: 9 }}>{priorityResponses[data.feedback ? data.feedback.priority-1 : 4]}</p>
                        </Col>
                        <Col span={4} style={{ float: 'right', textAlign: 'center' }}>
                            <Row>
                                <Button style={{ margin: 2 }} size="small" type="primary" shape="circle" icon="check" />
                                <Button style={{ margin: 2 }} size="small" type="danger" shape="circle" icon="delete" />
                            </Row>
                        </Col>
                    </Row>
                </Card>
            </List.Item>
        )
    }
}
