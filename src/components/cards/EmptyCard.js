import React, { Component } from 'react';
import { Card, Icon } from 'antd';


class EmptyCard extends Component {
  render() {
    const emptyCardStyle = {
      width: '100%',
      maxWidth: '500px',
      margin: '10px auto',
      cursor: 'default',
      minHeight: 200
    };
    const bodyStyle = {
      padding: '20px 40px',
      height: 200,
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-around',
      textAlign: 'center'
    }
    return (
      <Card
        style={emptyCardStyle}
        bodyStyle={bodyStyle}
        hoverable
      >
        <Icon 
          type={this.props.icon} 
          style={{fontSize: '1.5rem'}} 
        />
        <h3> {this.props.text}</h3>
      </Card>
    );
  }
}



export default EmptyCard;
