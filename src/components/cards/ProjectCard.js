import React, { Component } from 'react';
import { Card, Icon, Menu } from 'antd';
import ProgressBars from '../general/ProgressBars';
import { Link, withRouter } from 'react-router-dom';
import EllipsesMenu from '../general/EllipsesMenu';
import EllipsesMenuMetaData from '../general/EllipsesMenuMetaData';
import { getProjectNotes } from '../../demo-data/fake-apis/notes.api';


class ProjectCards extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  returnProjectNotes() {
    getProjectNotes(this.state.project.id).then(notes => {
      this.setState({
        project: {
          notes: notes,
          ...this.state.project,
        }
      })
    })
  }

  componentDidMount() {
    if (this.props.project && this.props.project !== this.state.project) {
      getProjectNotes(this.props.project.id).then(notes => {
        this.setState({
          project: this.props.project,
          notes: notes,
        })
      })
    }
  }

  renderMenu() {
    if ( !this.state.project ) return <div/>
    const { created_at, edited_at, completed, exportedAt } = this.state.project;
    return (
      <Menu style={{ width: 200 }}>
        <Menu.Item key="0">
          <Link to={`/projects/${this.state.project.id}/overview`} >View Project</Link>
        </Menu.Item>
        <Menu.Item key="1">
          <Link to={`/writer/${this.state.project.draft.id}/draft`} >Open Draft</Link>
        </Menu.Item>
        <Menu.Item key="2">
          <Link to={`/writer/${this.state.project.bibliography.id}/bibliography`} >Open Bibliography</Link>
        </Menu.Item>
        <Menu.SubMenu key="sub4" title={<span> Project Notes</span>}>
          {this.state.notes ? this.state.notes.map(note => {
            return <Menu.Item key={note.id}>
            <Link to={`/writer/${note.id}/note/type`}>
            <Icon type="profile" style={{marginRight: 10}}/> 
            {note.title}
            </Link> 
            </Menu.Item>
          }) : <Menu.Item>None Found</Menu.Item>}
        </Menu.SubMenu>
        <Menu.Divider />
        <EllipsesMenuMetaData
          createdAt={created_at}
          editedAt={edited_at}
          // Should set up a  completed date here so that if the project is finished it gives you an export date.
          exportedAt={completed ? exportedAt : null}
        />
      </Menu>
    )
  }


  openPreview = (value) => {
    this.props.preview(value);
  }

  openEdit = (id) => {
    this.props.history.push(`/writer/${id}/draft`);
  }
  render() {
    const projectCardStyle = {
      width: '100%',
      maxWidth: '500px',
      margin: '10px auto'
    };

    const completedCardStyle = {
      opacity: '0.6'
    };

    const IconSize = {
      textSize: '10px'
    }
    const { project, loading, id, progress, title, document } = this.props;
    return (
      <Card
        loading={loading}
        title={
            !loading ?
            <Link style={{ color: 'black' }} to={"projects/" + id + "/overview"} >{title} </Link> : 
            "Loading..."}
        style={!loading ? project.exported_at ? { ...completedCardStyle, ...projectCardStyle } : projectCardStyle : projectCardStyle}
        bodyStyle={{ padding: 15 }}
        extra={!loading ? project.exported_at ? <Icon type="check" style={{cursor: 'default'}}/> : '' : ''}
        hoverable
        actions={
          !loading ? [
            <Icon onClick={() => this.openEdit(id)} type="edit" theme="filled" />,
            <Icon onClick={() => this.openPreview(document)} type="eye" theme="filled" />,
            <EllipsesMenu menu={this.renderMenu()} >
              <Icon style={IconSize} type="ellipsis" />
            </EllipsesMenu>
          ] : ''}>
        < Link to={"projects/" + id + "/overview"} >
          <ProgressBars progress={progress} />
        </ Link>
      </Card>
    );
  }
}



export default withRouter(ProjectCards);
