import React, { Component } from 'react';
import { Col } from 'antd';

 class PanelLeft extends Component {
  render() {
		const Container = {
			height: '100%',
		}
	
	return (
	  <Col span={12} style={Container}>
		{this.props.children}
	  </Col>
	);
  };
};

export default PanelLeft;
