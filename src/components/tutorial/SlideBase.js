import React from 'react';
import { Row, Col } from 'antd';

export default function SlideBase(props) {

    if (props.hideArrows) {
        return <Row style={{ background: 'white', maxHeight: '100vh', overflow: 'hidden' }}>
            <Col
                span={2}>
            </Col>
            <Col span={20} >
                <Row style={{ height: '100vh', overflow: 'scroll', }}>
                    <div style={{ maxWidth: 700, margin: '0 auto' }}>
                        {props.children}
                    </div>
                </Row>
            </Col>
            <Col
                span={2}>
            </Col>
        </Row>
    }

    return (
        <Row style={{ background: 'white', maxHeight: '100vh', overflow: 'hidden' }}>
            <Col
                onClick={() => props.goBack()}
                className="slider-navigation"
                span={2}>
                <h1 > {"<"}</h1>
            </Col>
            <Col span={20} >
                <Row style={{ height: '100vh', overflow: 'scroll', }}>
                    <div style={{ maxWidth: 700, margin: '0 auto', textAlign: 'center' }}>
                        {props.children}
                    </div>
                </Row>
            </Col>
            <Col
                onClick={() => props.goForward()}
                className="slider-navigation"
                span={2}>
                <h1   > {">"}</h1>
            </Col>
        </Row>

    )
}
