import React, { Component } from 'react';
import { Carousel, Button } from 'antd';
import MainTutorialContent from './tutorialContent/MainTutorialContent';
import WelcomeSlideContent from './tutorialContent/WelcomeSlideContent';
import SubmissionSlide from './tutorialContent/SubmissionSlide';
import SlideBase from './SlideBase';

// Importing the images
import Dashboard from '../../images/tutorial/dashboard.png'
import AllDocuments from '../../images/tutorial/all-documents.png'
import Projects from '../../images/tutorial/projects.png'
import ProjectOverview from '../../images/tutorial/project-overview.png'
import YourProfile from '../../images/tutorial/your-profile.png'
import WriterImprovements from '../../images/tutorial/writer-improvements.png'
import WriterResources from '../../images/tutorial/writer-resources.png'


class TutorialCarousel extends Component {
	state = {
		currentSlide: 0,
		showingFooter: true,
		slides: [
			{
				title: <h1>Welcome To Currikula</h1>,
				image: false,
				moveOn: <Button onClick={() => this.handleClick()}>Get Started</Button>,
				content: <WelcomeSlideContent />
			},
			{
				title: <h1>Your Dashboard</h1>,
				image: Dashboard,
				content: (
					<div>
						<p>
							This is your Dashboard, the nerve center of your new academic experience. You can easily access current projects, recent notes or start fresh. We've included a handy term tracker to let you know at a glance how the term is progressing.
					</p>
						<p>
							At a glance you can easily see your projects progress. This includes things like overall project progress (calculated on how far your essay is away from being able to be submitted), how far out the due date is, whether you've completed your edits and how far along you are to completing your first draft.
					</p>
					</div>

				)
			},
			{
				title: <h1>Your Projects</h1>,
				image: Projects,
				navText: 'PROJECTS',
				content: (
					<p>
						The fist option on the navigation side bar after the Dashboard is the Projects screen. This where you can see all your current projects, and those that you have already completed. If you have lots of projects on the go at once this will be the easiest place to get a birds eye view of everything that is going on.
					</p>
				)
			},
			{
				title: <h1>Project Overview</h1>,
				image: ProjectOverview,
				content: (
					<div>
						<p>
							Each project has a Project Overview screen which contains the vital information for that project. On the left you can get another look at the progress of the project and quick access to the main documents in your essay (draft and bibliography) and see all of the notes that you have made in relation to the project so far.
						</p>
						<p>
							On the right is your Timeline which chronicles the steps and actions you have taken on the project so far. From editing your draft to beginning your bibliography or from adding references to creating new notes. It is easy to see how and when your project evolved from here. This is also the place where you can enter the results of your assignment when you get them.
						</p>
					</div>
				)
			},
			{
				title: <h1>All Documents</h1>,
				image: AllDocuments,
				content: (
					<p>
						Below Projects on your nav bar is the All Documents page. Here you can access all your documents (drafts, notes and bibliographies). You can also see those you've marked as completed by toggling at the top.
					</p>
				)
			},
			{
				title: <h1>Writer (Overview)</h1>,
				image: WriterResources,
				content: (
					<div>
						<p>
							Probably where you will spend most of your time, Currikula's writer is a unique essay writing, note taking and bibliography creating experience. It uses common sense and simplicity to eliminate much of the needless complexity that other writing systems have.
					</p>
						<p>
							With the ability to view documents or notes that you've already created minimising the time needed to get words on the page. This resources tab also allows you to quickly switch between documents and notes. 
						</p>
						<p>
							Some of its unique referencing features include the ability to quickly insert references directly from your bibliography, reference tracking to make sure you never leave things unreferenced and automatically updating style if you change referencing systems. 
						</p>
					</div>

				)
			},
			{
				title: <h1>Writer (Improve and Plagiarism)</h1>,
				image: WriterImprovements,
				content: (
					<div>
						<p>
							With feedback on your writing and plagiarism detection built in, this system is constantly working to help improve your writing. 
						</p>
						<h4>Improve</h4>
						<p>
							Using InnerText our unique essay processing engine we can provide feedback on over 24 different points on your essay and we'll keep adding different feedback points as time goes on. This can help you refine your drafts into perfect form and achieve grades of the highest calibre. 
						</p>
						<h4>Plagiarism</h4>
						<p>
							Using our unique Smart Source v2 software we analyse your essay for originality inconsistencies, helping to protect you from accidental plagiarism. We can billions of web pages and hundreds of thousands of resources to determine the originality of your essay. Going one step further we look at samples of your writing over time to determine whether your unique authorship linguistic print in order to check your own writing against yourself.
						</p>


					</div>

				)
			},
			{
				title: <h1>Your Profile and Account</h1>,
				image: YourProfile,
				navText: 'PROFILE',
				content: (
					<p>
						Almost done! Your Profile is where you can see your overall stats, track your progress in the Currikula rewards and referral scheme, calculate your current grade and what you need to get the grade above and where you can see your notifications.
					</p>
				)
			},
			{
				title: <h1>Final Information</h1>,
				image: false,
				moveOn: " ",
				content: <SubmissionSlide
							goBack={() => this.handleClickReverse()}
						 />
			}
		]
	};
	child = React.createRef();
	onChange = slide => {
		this.setState({
			currentSlide: slide
		});
	};
	handleClick = () => {
		this.child.current.next();
	};

	handleClickReverse = () => {
		this.child.current.prev();
	};
	titleSelector = () => {
		const { slides, currentSlide } = this.state;
		const title = slides[currentSlide] ? slides[currentSlide].navText : ''
		return title
	};

	removeFooter() {
		if (this.props.showFooter) {
			this.setState({
				showingFooter: false
			})
			this.props.toggleFooter()
		}

	}

	componentWillMount() {
		this.removeFooter()
	}

	render() {
		const backgroundForCarousel = {
			zIndex: 10,
			backgroundColor: 'white',
		};

		return (
			<div style={backgroundForCarousel}>
				<Carousel
					dots={false}
					initialSlide={this.state.currentSlide}
					ref={this.child}
					afterChange={this.onChange}>
					{this.state.slides.map(slide => {
						return <SlideBase
							key={slide.title}
							goBack={this.handleClickReverse}
							goForward={this.handleClick}
							hideArrows={slide.moveOn}
						>
							<MainTutorialContent
								title={slide.title}
								image={slide.image}
								content={slide.content}
								moveOn={slide.moveOn}
								goBack={this.handleClickReverse}
							/>
						</SlideBase>

					})}
				</Carousel>

			</div>
		);
	}
}

export default TutorialCarousel;