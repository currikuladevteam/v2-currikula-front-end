import React, { Component } from 'react';
import { Row, Col, Form, InputNumber, Select, Slider, Button } from 'antd';
import { Link } from 'react-router-dom';

const formItemLayout = {
	labelCol: { span: 4 },
	wrapperCol: { span: 20 }
};

class SubmissionSlide extends Component {
	state = {
		courseLength: [1, 2, 3],
		resetCourseWeights: false
	};

	renderYearCredits() {
		const { getFieldDecorator } = this.props.form;
		return this.state.courseLength.map(year => {
			return (
				<Col key={year} span={6}>
					<Form.Item label={`Year ${year}`}>
						{getFieldDecorator('input-number', {
							initialValue: 1,
							rules: [
								{
									required: true
								}
							]
						})(<InputNumber min={0} max={100} />)}
					</Form.Item>
				</Col>
			);
		});
	}

	onChange = value => {
		this.setState({
			courseLength: Array.from(new Array(value), (x, i) => i + 1),
			resetCourseWeights: true
		});
	};

	renderYearWeight() {
		const {
			getFieldDecorator,
			getFieldValue,
			setFieldsValue
		} = this.props.form;
		const years = this.state.courseLength;

		return years.map(year => {
			let yearWeightChange = {};
			if (this.state.resetCourseWeights) {
				yearWeightChange['yearWeight-' + year] = year === 1 ? 0 : [0, 0];
				setFieldsValue(yearWeightChange);
				if (year === years.length && this.state.resetCourseWeights) {
					this.setState({
						resetCourseWeights: false
					})
				}
				return ""
			} else {

				// console.log('Year ', year, ' Iteration')
				let year0 = year - 1;
				let year2 = year + 1;
				if (year === years.length) {
					// console.log('Reducing Year');
					year2 = year;
				}


				if (year === 1) {
					// console.log('Nulling 0');
					year0 = year;
				}

				const year0Value = getFieldValue('yearWeight-' + year0);
				const year1Value = getFieldValue('yearWeight-' + year);
				const year2Value = getFieldValue('yearWeight-' + year2);

				if (year === 1 && year1Value) {
					// console.log('Changing Year 1')
					if (year1Value > year2Value[0] || year1Value < year2Value[0]) {
						yearWeightChange['yearWeight-' + year2] = [year1Value, year1Value];
						setFieldsValue(yearWeightChange);
					}
				} else if (year > 1 && year2Value && year !== year2) {
					// console.log('Year ', year0, ' Value0 ', year0Value, year0Value[0], year0Value[1])
					// console.log('Year ', year, ' Value ', year1Value[0], year1Value[1])
					// console.log('Year ', year2, ' Value2 ', year2Value[0], year2Value[1])
					// console.log(year1Value[1] > year2Value[0])
					if (year1Value[1] > year2Value[0] || year1Value[1] < year2Value[0]) {
						if (year1Value[1] > year2Value[1] || year2Value[0] < year2Value[1]) {
							// console.log(year1Value[1], year2Value[1], year1Value[1], year2Value[1], year2Value[0], year2Value[1])
							// console.log(year1Value[1] > year2Value[1])
							// console.log(year1Value[1] < year2Value[1],year1Value[1],  ' < ', year2Value[1] )
							// console.log(year2Value[0] < year2Value[1])
							// console.log('Changing Year!', 'yearWeight-' + year2);
							yearWeightChange['yearWeight-' + year2] = [year1Value[1], year1Value[1]];
							setFieldsValue(yearWeightChange);
						}
					}

					// if the value of year2[0] < year[1] then set value to year[1], year2[1]
					// Need to look back so that it can alter the year behind it
					// Need to make the first of year1 = year0 effectively
				}

				// Drags the value back with the previous slider rather than just resetting it. 
				if (year1Value && year2Value && (year1Value < year2Value[0] || year1Value[1] < year2Value[0]) && (year2Value[1] !== 0)) {

					// console.log(year1Value < year2Value[0], year1Value[1] < year2Value[0] )
					// console.log(year2Value[1] !== 0)
					if (year1Value.length > 1) {
						yearWeightChange['yearWeight-' + year2] = [year1Value[1], year2Value[1]];
					} else {
						yearWeightChange['yearWeight-' + year2] = [year1Value, year2Value[1]];
					}
					setFieldsValue(yearWeightChange);
				}

				// Back checking to see if the previous slider has the correct values
				if (year !== year0 && year !== year2 && year1Value) {
					// console.log('Passing Year: ', year, year0Value, year1Value)
					if (year0 > 1 && year1Value[1] !== year0Value[1]) {
						yearWeightChange['yearWeight-' + year] = [year1Value[1], year1Value[1]];
						setFieldsValue(yearWeightChange);
					} else if (year1Value[0] !== year0Value && year0 === 1) {
						yearWeightChange['yearWeight-' + year] = [year0Value === 0 ? year0Value : year0Value[1], year1Value[1]];
						setFieldsValue(yearWeightChange);
					}
				}


				// if (year 1 === 0 || year 1 [0] === 0 ) && year 2 [1] !== 0 then set year 2 to [year[1], year[1]]

			}

			return (
				<Row key={year} type="flex">
					<Col span={20} style={{ margin: '0 auto' }}>
						<Form.Item
							style={{ marginBottom: 0, paddingBottom: 0 }}
							label={<h4 style={{ marginTop: '8px' }}>Year {year}</h4>}
							{...formItemLayout}
						>
							{getFieldDecorator('yearWeight-' + year, {
								initialValue: year === 1 ? 0 : [0, 0]
							})(
								<Slider
									marks={{
										0: '0%',
										100: '100%'
									}}
									min={0}
									range={year === 1 ? false : true}
								/>
							)}
						</Form.Item>
					</Col>
				</Row>
			);
		});
	}

	render() {
		const { Option } = Select;

		const { getFieldDecorator } = this.props.form;

		return (
			<div style={{
				marginBottom: 100
			}}>
				<Row style={{ margin: '10px 0px' }}>
					<p>
						We hope you enjoyed your whirl wind tour of Currikula - if you have any questions or seek more information check out our FAQ, our help directory or message us directly.
					</p>
					<p>
						In order to continue we need you to enter the following information.
					</p>
				</Row>
				<Row style={{}}>
					<Form onSubmit={this.handleSubmit} layout="vertical">
						<Form.Item label="University Name" hasFeedback>
							{getFieldDecorator('university_number', {
								initialValue: 'What University do you attend?',
								rules: [
									{
										required: true
									}
								]
							})(
								<Select>
									<Option value="1">University of Hull</Option>
									<Option value="2">University of Lincoln</Option>
									<Option value="3">University of Wales</Option>
									<Option value="4">Bangor University</Option>
								</Select>
							)}
						</Form.Item>
						<Form.Item label="Select Your Course">
							{getFieldDecorator('university_course', {
								rules: [
									{
										required: true,
										message: 'Please select your course!',
										type: 'array'
									}
								]
							})(
								<Select
									mode="multiple"
									placeholder="Please select favourite colors"
								>
									<Option value="science">Science</Option>
									<Option value="maths">Maths</Option>
									<Option value="economics">Economics</Option>
								</Select>
							)}
						</Form.Item>
						<Row>
							<Col span={12}>
								<Form.Item label="Course Length">
									{getFieldDecorator('course_length', {
										initialValue: 3,
										rules: [
											{
												required: true
											}
										]
									})(<InputNumber min={1} max={10} onChange={this.onChange} />)}
									<span className="ant-form-text"> years</span>
								</Form.Item>
							</Col>
							<Col span={12}>
								<Form.Item label="Current Year">
									{getFieldDecorator('current_year', {
										initialValue: 1,
										rules: [
											{
												required: true
											}
										]
									})(<InputNumber min={1} max={10} />)}
								</Form.Item>
							</Col>
						</Row>

						{/* iterate number of form.item inputs based on length of course. */}
						{/* {generateFormItem()} */}

						<Row style={{ margin: '20px 0px' }}>
							<h4>Year Weights</h4>
							<p>Use the sliders to drag how much each year of your course is worth towards your overall grade. Typically your first year (in a 3 year degree) does not count towards your degree.</p>
							{this.renderYearWeight()}
						</Row>
						<Row>
							<h4>Year Credits</h4>
							<p>This is how many credits each year is worth over all. For example, each year might be out of a total of 120 credits. And your modules might count for 15 credits or 30 credits.</p>
							{this.renderYearCredits()}
						</Row>
					</Form>
				</Row>
				<Row style={{ textAlign: 'center', padding: 50 }} >
					<Col style={{margin: 10}}>
						<Link to="/dashboard"><Button type="primary" size="large"> Completed </Button></Link>
					</Col>
					<Col style={{margin: 10}}>
						<Button onClick={() => this.props.goBack()}> Take me back </Button>
					</Col>
				</Row>
			</div>
		);
	}
}

const WrappedFormSubmissionSlide = Form.create({ name: 'submission_slide' })(
	SubmissionSlide
);

export default WrappedFormSubmissionSlide;
