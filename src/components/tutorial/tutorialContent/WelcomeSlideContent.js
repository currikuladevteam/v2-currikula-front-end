import React from 'react';
import { Row, Col, Icon } from 'antd';
const marginAround = {
	margin: '30px'
};
const marginAroundParagraph = {
	margin: '0px 40px'
};
const IconStyle = {
	height: 150,
	borderRadius: 20,
	fontSize: 100,
	textAlign: 'center'
};
const IconText = {
	fontSize: 20,
	textAlign: 'center'
}

export default function WelcomeSlideContent(props) {


	return (
		<Row style={{textAlign: 'center'}}>
			<Row style={marginAroundParagraph}>
				<p>
					Congratulations on taking the wise step of upgrading your academic experience. If at any point you are unhappy with the Currikula service please do not hesitate to reach out and contact us using the message icon in the bottom right hand corner.
	
				</p>
				<p>
					This is a brief on boarding process to show you how the product works and finalise your account information. Below we've highlighted the core functionality of Currikula.
				</p>
			</Row>
			<Row style={marginAround}>
				<Col span={6}>
					<div style={IconStyle}><Icon type="profile" alt="Take Notes" /></div>
					<div style={IconText}>Take Notes</div>

				</Col>
				<Col span={6}>
					<div style={IconStyle}><Icon type="file-text" alt="Write Drafts" /></div>
					<div style={IconText}>Write Drafts</div>

				</Col>
				<Col span={6}>
					<div style={IconStyle}><Icon type="safety-certificate" alt="Check and Improve" /></div>
					<div style={IconText}>Improve</div>


				</Col>
				<Col span={6}>
					<div style={IconStyle}><Icon type="read" alt="Reference" /></div>
					<div style={IconText}>Reference</div>

				</Col>
			</Row>
			<Row style={marginAround}>
				<p>
				Currikula is designed to help you achieve greater efficiency and better grades on your assignments. It does this through helping you take better notes, write quicker and more comprehensive drafts, offer feedback and plagiarism checks and super quick referencing help. 
				</p>
			</Row>

		</Row>
	)
}