import React, { Component } from 'react';

import { Row } from 'antd';

class MainTutorialContent extends Component {
	render() {
		const largeImageStyle = {
			// height: 350,
			width: 700,
			borderRadius: 20,
			padding: 10
		};
		const carouselSlide = {
			minWidth: '100%',
			backgroundColor: '#ffffff',
			padding: '60px 0'
		};

		const { title, image, content, moveOn } = this.props;
		return (
			<div style={carouselSlide}>
				<Row>
					<Row style={{ textAlign: 'center' }}>{title}</Row>
					<Row style={{ margin: '0 auto', marginBottom: 15 }}>
					{image ? <img style={largeImageStyle} src={image} alt="Large Dashboard" /> : ""}
					</Row>
					<Row style={{margin: '0px 10px'}}>{content}</Row>
					<Row style={{margin: '0px 10px', textAlign: 'center'}}>{moveOn ? moveOn : ''}</Row>
				</Row>
			</div>
		);
	}
}
export default MainTutorialContent;
