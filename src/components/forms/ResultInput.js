import React from 'react'
import {
    Form, Switch, Radio,
    Slider, Button, Input,
    Row, Col,
} from 'antd';


const { TextArea } = Input;

class ResultInput extends React.Component {
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }

    normFile = (e) => {
        console.log('Upload event:', e);
        if (Array.isArray(e)) {
            return e;
        }
        return e && e.fileList;
    }



    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            // labelCol: { span: 0 },
            // wrapperCol: { span: 24 },
        };
        return (
            <Form onSubmit={this.handleSubmit} layout='vertical'>
                <Row gutter={20}>
                    <Col span={12}>
                        <Form.Item
                            {...formItemLayout}
                            style={{ textAlign: 'center' }}
                            label={
                                renderLabel(
                                    "Counts towards Grade",
                                    "If this project counts towards your final module grade then toggle yes, if it is just a practise assignment, or formative assessment don't toggle.")
                            }
                        >
                            {getFieldDecorator('switch', { valuePropName: 'checked' })(
                                <Switch />
                            )}
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item
                            {...formItemLayout}
                            label={
                                renderLabel(
                                    "Resulting Mark or Grade",
                                    "Use the slider to enter your exact score on the assignment. Currikula uses this score to help you figure out what other results you need to get your desired grade.")
                            }
                        >
                            {getFieldDecorator('slider')(
                                <Slider marks={{
                                    0: <small>Fail</small>, 40: <small>3rd</small>, 50: <small>2:2</small>, 60: <small>2:1</small>, 70: <small>1st</small>, 80: <small>*1st</small>,
                                }}
                                />
                            )}
                        </Form.Item>
                    </Col>
                </Row>
                <Form.Item
                    {...formItemLayout}
                    style={{ textAlign: 'center' }}
                    label={
                        renderLabel(
                            "Work Effort",
                            "How much effort do you think you put into this assignment? Did you work your hardest or were you getting by?")
                    }
                >
                    {getFieldDecorator('workEffort')(
                        <Radio.Group>
                            <Radio.Button value="low">Low</Radio.Button>
                            <Radio.Button value="lower">Lower</Radio.Button>
                            <Radio.Button value="medium">Medium</Radio.Button>
                            <Radio.Button value="high">High</Radio.Button>
                            <Radio.Button value="highest">Highest</Radio.Button>
                        </Radio.Group>
                    )}
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label={
                        renderLabel(
                            "Markers Name",
                            "If you know the name of the marker enter it here. This helps us create a baseline of marking tendencies and help improve our services by giving better feedback on your essay.")
                    }
                >
                    {getFieldDecorator('markersName', {
                        rules: [],
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item
                    {...formItemLayout}
                    label={
                        renderLabel(
                            "Marker's Comments",
                            "If you get receive feedback from the marker on your essay, you should enter it here. This will help Currikula help you by isolating potential issues before submitting them.")
                    }
                >
                    {getFieldDecorator('markersComments', {
                        rules: [],
                    })(
                        <TextArea rows={4} />
                    )}
                </Form.Item>
                <Form.Item
                    wrapperCol={{ span: 12, offset: 0 }}
                >
                    <Button type="primary" htmlType="submit">Save</Button>
                </Form.Item>
            </Form>
        );
    }
}

const WrappedResultInput = Form.create({ name: 'validate_other' })(ResultInput);

export default WrappedResultInput


function renderLabel(header, subText) {
    return <div
        style={{ whiteSpace: 'normal' }}
    >
        <h3 style={{ marginBottom: 5 }}>{header}</h3>
        <p style={{ textAlign: 'justify', fontSize: 'smaller' }}>{subText}</p>
    </div>
}