import React, { Component } from 'react';
import {Row, Button} from 'antd';
import { withRouter } from 'react-router-dom';


class HeaderTitle extends Component {

  goBack = () => {
    this.props.history.goBack();
  }

  render() {
    const HeadOne = {
      textAlign: 'center',
      letterSpacing: '0.3rem',
      marginTop: this.props.marginTop ? this.props.marginTop : '14px'
    };

    const backButton =  <Button onClick={this.goBack} style={{position: 'absolute', marginTop: 4}} type="primary" icon="left">Back</Button>;

    return (
      <Row>
        <div style={{margin: '0 auto', width: '100%', padding: '0px 20px'}}>
          { this.props.backButton ? backButton : '' }
          <h1 style={HeadOne}>{this.props.title}</h1>
        </div>
      </Row>
    );
  };
};

export default withRouter(HeaderTitle);
