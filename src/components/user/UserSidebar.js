import React, { Component } from 'react';
import { Menu, Button } from 'antd';
import { Link, withRouter } from 'react-router-dom';

class UserSidebar extends Component {
  constructor() {
    super()
    this.state = {
      key: ["2"]
    }
  }
  navigation = (selection) => {
    let key = this.state.key;
    key.push(selection.key)
    this.setState({
      key: key
    })
    this.props.history.push(selection.item.props.route);
  }

  render() {
    return (
      <Menu
        theme="light"
        mode="vertical"
        defaultSelectedKeys={[this.props.location.pathname.match(/(\/[a-z-]+)/g)[1]]}
        style={{
          height: '100%',
          display: 'flex',
          position: 'fixed',
          width: 200,
          flexDirection: 'column',
          justifyContent: 'flex-end',
        }}
      >
        <Menu
          style={{ marginBottom: 'auto' }} >
          <Menu.Item style={{ textAlign: 'center' }} key="user">
            <h2 style={{ letterSpacing: '0.3rem' }}>USER</h2>
          </Menu.Item>
          <Menu.Item key="/profile">
            <Link to="/user/profile">PROFILE</Link>
          </Menu.Item>
          <Menu.Item key="/university-stats">
            <Link to="/user/university-stats">UNIVERSITY STATS</Link>
          </Menu.Item>
          <Menu.Item key="/grade-calculator">
            <Link to="/user/grade-calculator">GRADE CALCULATOR</Link>
          </Menu.Item>
          <Menu.Item key="/referrals">
            <Link to="/user/referrals">REFERRALS</Link>
          </Menu.Item>
          <Menu.Item key="/notifications">
            <Link to="/user/notifications">NOTIFICATIONS</Link>
          </Menu.Item>
        </Menu>
        <Menu>
          <Menu.Item style={{ textAlign: 'center', margin: '20px 0' }}>
            <Button block type="danger"><Link to="/">LOGOUT</Link></Button>
          </Menu.Item>
        </Menu>
      </Menu>
    );
  }
}

export default withRouter(UserSidebar);


