import React, { Component } from "react";
import { Card, Input, Row, Col, Collapse } from "antd";
import { getYearString } from "../../../functions/helpers/UserHelpers";

export default class YearPane extends Component {
  renderHelpMessage(currentYear, yearOfStudy) {
    if (currentYear && yearOfStudy === 1) {
      return <p> Enter your results below in order to calculate your grade.</p>;
    } else if (currentYear && yearOfStudy > 1) {
      return (
        <p>
          {" "}
          Enter your results below in order to calculate your grade and add
          results to previous years along with the weight of those years to gain
          a better picture of your grades overall.{" "}
        </p>
      );
    } else {
      return (
        <p>
          You can enter your overall results to the right, or go through and add
          your modules and assignments to let us calculate it for you.
        </p>
      );
    }
  }

  renderSettings(years) {
    if (years.length === 0) {
      return <Card style={{margin: 10}}>
        <h3 style={{textAlign: 'center'}}> You need to complete your profile before this section becomes available.</h3>
        </Card>
    }
    return years.map((year, index) => {
      return (
        <Row gutter={10} key={index} style={{ margin: "20px 0" }}>
          <Col span={12}>
            <h3 style={{ marginTop: 5 }}>{getYearString(year.year)}</h3>
          </Col>
          <Col span={4} style={{ fontStyle: "bold" }}>
            <b>Total Credits</b>
            <Input placeholder="0" value={year.credits} />
          </Col>
          <Col span={4} style={{ fontStyle: "bold" }}>
            <b>Year Weight</b>
            <Input placeholder="0" value={year.weight} />
          </Col>
          <Col span={4}>
            <b>Year Result</b>
            <Input placeholder="0" value={year.result ? year.result : ""} />
          </Col>
        </Row>
      );
    });
  }
  render() {
    return (
      <Card bordered={false} bodyStyle={{padding: '5px 24px'}}>
        <Collapse bordered={false}>
          <Collapse.Panel
            header={<h3 style={{ marginTop: 3 }}>Information</h3>}
            key="1"
          >
            <Row>
              <h4> General Information </h4>
              <p>
                Your degree is made up of a number of academic years. If you are
                an Undergraduate this could be three or four years depending on
                your course and whether you take a year abroad, while Masters
                could be only one year and Phd much longer. Each year is likely
                also weighted differently, with many universities in the UK not
                counting your first year (exceptions like Oxford and Cambridge
                exist), and then 1/3rd in year two and 2/3rd's in your final
                year (final year is most important almost always). Because of
                this it is important to take into account your year weightings -
                how much of your year 'mark' counts towards your overall degree
                grade and, thus, classification in addition to module and
                evaluation weightings. For example, if you got a 70% in year
                one, 65% in year two, and 60% in year three your overall grade
                would be 61%, not 65%.
              </p>
              <h4>The Credit System</h4>
              <p>
                Your academic year consists of several modules, depending on
                what course you take. Some courses have eight modules each year,
                some six, and some even less. Each module has a 'credit value'
                attached to it - the more contacts hours it is the more credits
                it will likely be worth. If you add up all your module credits
                that year you have your Total Credit Value for the year (or
                TCV). By dividing and individual module credit value by the TCV
                you get the module 'weight' in percentage terms. For example, if
                one module was worth 15 credits and your Total Credit Value was
                120, that module would count for 12.5% of your grade that year
                (15 / 120 x 100 = 12.5%).
              </p>
            </Row>
            <Row>
              <Col span={4}>
                <h4>Total Credits</h4>
              </Col>
              <Col span={20}>
                <p>
                  Your total credits are to determine the weight of each module
                  for each year. You can calculate total credits by adding all
                  your module credit values for the year together.{" "}
                </p>
              </Col>
            </Row>
            <Row>
              <Col span={4}>
                <h4>Year Weight</h4>
              </Col>
              <Col span={20}>
                <p>
                  You should know at the beginning of your course how much each
                  academic year is weighted towards your overall degree. This
                  may change slightly if you decide to do a year abroad in the
                  middle of your course, but for now we recommend inputting
                  exactly what your degree looks like in your university system.{" "}
                </p>
              </Col>
            </Row>
            <Row>
              <Col span={4}>
                <h4>Year Result</h4>
              </Col>
              <Col span={20}>
                <p>
                  Your year result is made up of each module grade multiplied by
                  the weighting of the module. weighting of the module is
                  calculated by dividing the credit value of the module by total
                  credits which you calculated earlier.{" "}
                </p>
              </Col>
            </Row>
          </Collapse.Panel>
        </Collapse>
        {this.renderSettings(this.props.years)}
      </Card>
    );
  }
}
