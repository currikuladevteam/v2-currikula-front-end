import React, { Component } from "react";
import { Card, Row, Col } from "antd";

const cardStyle = {
  minHeight: "20px",
  marginBottom: 30
};

const colHeaderContainerStyle = {
  height: 100, 
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center'
}

const headerStyle = {
  wordBreak: "break-word",
  textAlign: "center",
  margin: "auto"
};

const gradeCircleStyle = {
  borderRadius: "100px",
  height: "100px",
  width: "100px",
  border: "2px solid green",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  backgroundColor: "green",
  margin: "0 auto"
};

const gradeCircleResultStyle = {
  fontSize: "50px",
  fontWeight: "bold",
  color: "white"
};

const adviceStyle = { 
    marginTop: "20px", 
    textAlign: "center" 
}

const instructionStyle = {
  marginTop: "20px",
  marginBottom: "0px",
  textAlign: "center",
  width: "100%",
  fontSize: 11
};


class GradeCalculatorResult extends Component {
  render() {
    return (
      <Card style={cardStyle}>
        <Row style={{ margin: 10 }}>
          <Col style={colHeaderContainerStyle}  span={12}>
            <h1 style={headerStyle}>YOU'RE ON TRACK FOR A...</h1>
          </Col>
          <Col span={12}>
            <div style={gradeCircleStyle}>
              <div style={gradeCircleResultStyle}>{this.props.grade}</div>
            </div>
          </Col>
        </Row>
        <Row>
          <p style={adviceStyle}>
            {this.props.advice}
          </p>
          <p style={instructionStyle}>
            Update your modules and results below, then hit calculate to see
            what you are on track for.
          </p>
        </Row>
      </Card>
    );
  }
}

export default GradeCalculatorResult;
