import React, { Component } from 'react';
import { Row, Col, Layout } from 'antd';
import HeaderTitle from '../../HeaderTitle';
import GradeCalculatorResult from './GradeCalculatorResult';
// import GradeCalculatorForm from './GradeCalculatorForm';
import GradeCalculatorTabs from './GradeCalculatorTabs';
// import { UserConsumer } from '../../providers/UserProvider';



class gradeCalculator extends Component {
  render() {
    return (
      <Layout style={{ minHeight: '100vh' }}>
        <HeaderTitle title="GRADE CALCULATOR" marginTop="30px" />
        <Row
          type="flex"
          gutter={8}
          style={{ margin: '10px', padding: '20px 25px 20px 25px', height: `calc(100vh - 70px)`, justifyContent: 'center', flexDirectioon: 'column' }}
        >
          <Col span={20} >
            <GradeCalculatorResult
              grade="2:1"
              advice="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sed interdum diam."
            />
            <GradeCalculatorTabs
            />
          </Col>
        </Row>
      </Layout >
    );
  }

}

export default gradeCalculator;
