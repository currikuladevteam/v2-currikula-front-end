import React, { Component } from 'react';
import { Tabs, Card, Icon } from 'antd';
import ModuleCollapses from './ModuleCollapses';
import YearSettings from './YearSettings';
import { getYearString } from '../../../functions/helpers/UserHelpers';
import { UserConsumer } from '../../providers/UserProvider';
const TabPane = Tabs.TabPane;

// const module = { data: 'test' }

class GradeCalculatorTabs extends Component {
  constructor(props) {
    super(props);
    this.newTabIndex = 0;
    this.state = {

    }
  }


  onChange = (activeKey) => {
    this.setState({ activeKey });
  }

  onEdit = (targetKey, action) => {
    this[action](targetKey);
  }

  add = () => {
    const panes = this.state.panes;
    const activeKey = `newTab${this.newTabIndex++}`;
    panes.push({ title: 'New Tab', content: 'Content of new Tab', key: activeKey });
    this.setState({ panes, activeKey });
  }

  remove = (targetKey) => {
    let activeKey = this.state.activeKey;
    let lastIndex;
    this.state.panes.forEach((pane, i) => {
      if (pane.key === targetKey) {
        lastIndex = i - 1;
      }
    });
    const panes = this.state.panes.filter(pane => pane.key !== targetKey);
    if (lastIndex >= 0 && activeKey === targetKey) {
      activeKey = panes[lastIndex].key;
    }
    this.setState({ panes, activeKey });
  }


  renderResultsPanes(user) {
    if (user.grades.results.length === 0 ) {
      return <TabPane
      key="example"
      disabled
      tab="Example Year"
      >
    </TabPane>
    }

    return user.grades.results.map(tab => (
      <TabPane
        tab={getYearString(tab.year)}
        key={tab.year}
        closable={false}
        disabled={tab.status === "not_started" ? true : false}
      >
        <h3 style={{ textAlign: "center" }}>Modules</h3>
        <ModuleCollapses modules={tab.modules} />
        <Card
          className="addModule"
          size="small"
          style={{ marginTop: 10, textAlign: "center" }}
          bodyStyle={{ padding: 12 }}
          hoverable={true}
        >
          Add New Module
        </Card>
      </TabPane>
    ))
  }


  render() {
    return (
      <UserConsumer>
        {(({ user }) => {
          if (!user) {
              return <Card loading={true}
            />
          }
          return (
            <Card style={{ marginBottom: 50, paddingBottom: 20 }}>
              <Tabs
                onChange={this.onChange}
                activeKey={this.state.activeKey ? this.state.activeKey : user.grades.required.current ? (user.grades.required.current).toString(): "settings"}
                onEdit={this.onEdit}
              >
                <TabPane
                  key="settings"
                  tab={
                    <div style={{ width: 90 }}>
                      <Icon type="setting"/> Settings
                    </div>
                  }>
                  <YearSettings
                    years={user.grades.results}
                  />
                </TabPane>
                {this.renderResultsPanes(user)}
              </Tabs>
            </Card>)
        })}
      </UserConsumer>
    )
  }
}

export default GradeCalculatorTabs;
