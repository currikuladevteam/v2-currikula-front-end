import React, { Component } from 'react';
import { Row, Col, Input, Icon } from 'antd';




class GradeCalculatorForm extends Component {

    handleDelete() {
        this.props.delete(this.props.id);
    }
    render() {
        return (
            <Row type="flex" gutter={16}>
                <Col span={16}>
                    <Input placeholder="Enter your Module Name or select a project from the drop down" value={this.props.module} />
                </Col>
                <Col span={3} >
                    <Input placeholder="0" value={this.props.weight}/>
                </Col>
                <Col span={3}>
                    <Input placeholder="0" value={this.props.result}/>
                </Col>
                <Col span={2} style={{textAlign: 'center'}}>
                    <Icon  onClick={() => this.handleDelete()} type="delete" theme="twoTone"/>
                </Col>
            </Row>
        );
    }
}

export default GradeCalculatorForm;
