import React, { Component } from "react";
import { Icon, Row, Col } from "antd";

export default class YearPane extends Component {

    render() {
        const { assignment } = this.props;
        return (
            <div
                style={{ padding: 10, paddingLeft: 15 }}
            >
                <Row gutter={10} style={{}}>
                    <Col span={15} style={{ marginTop: 5 }}>
                        <h4>{assignment.title}</h4>
                    </Col>
                    <Col span={4} >
                        {/* <Input addonBefore={<Icon type="pie-chart" />} /> */}
                        <Row>
                            <h4>
                                <Icon type="pie-chart" style={{ marginRight: 5 }} />
                                {assignment.weight ? assignment.weight : 'N/A'}
                            </h4>
                        </Row>

                    </Col>
                    <Col span={4} >
                        {/* <Input addonBefore={<Icon type="file-done" />} /> */}
                        <h4>
                            <Icon type="file-done" style={{ marginRight: 5 }} />
                            {assignment.grade ? assignment.grade : 'N/A'}
                        </h4>
                    </Col>
                    <Col span={1}>
                        <Icon style={{ marginLeft: 10 }} type="edit" />
                    </Col>

                </Row>
            </div>
        );
    }
}
