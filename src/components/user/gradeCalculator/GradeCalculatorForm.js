import React, { Component } from 'react';
import { Card, Row, Col, Form, Button, Divider } from 'antd';
import GradeCalculatorRow from './GradeCalculatorRow';

const CardStyle = {
    minHeight: '500px'
}


class GradeCalculatorForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            results: [
                {
                    module: 'The Battle of the Somme',
                    result: 56,
                    weight: 15,
                    id: 1
                },
                {
                    module: 'Hitler\'s Rise to Power',
                    result: 70,
                    weight: 30,
                    id: 2
                },
            ]
        };
    }

    handleAddRowClick = () => {
        let results = [...this.state.results];
        const result = {
            module: '',
            result: '',
            weight: '',
            // Incrementer to prevent duplicate deletion
            id: results.length === 0 ? 1 : results[results.length-1].id + 1
        }
        results.push(result);
        this.setState({
            results,
        });
    }
    handleRemoveRowClick = (id) => {
        let results = [...this.state.results];
        results = results.filter((result) => result.id !== id)
        this.setState({
            results
        })
    }

    render() {
        return (
            <Card style={{ CardStyle }} >
                <Form.Item>
                    <Row type="flex" gutter={16}>
                        <Col span={16}>
                            <h4>MODULE NAME</h4>
                        </Col>
                        <Col span={3} >
                            <h4 style={{ textAlign: 'center' }}>WEIGHT</h4>
                        </Col>
                        <Col span={3}>
                            <h4 style={{ textAlign: 'center' }}>RESULT</h4>
                        </Col>
                        <Col span={2}>
                            <h4 style={{ textAlign: 'center' }}>DEL</h4>
                        </Col>
                    </Row>
                    <Row>
                        {this.state.results.map((result, index) =>
                            <GradeCalculatorRow
                                key={index}
                                id={index + 1}
                                module={result.module}
                                result={result.result}
                                weight={result.weight}
                                delete={this.handleRemoveRowClick}
                            />
                        )}
                    </Row>
                </Form.Item >
                <Divider />
                <Button style={{ float: 'left' }} onClick={this.handleAddRowClick}>ADD ROW</Button>
                <Button style={{ float: 'right' }}>CALCULATE</Button>
            </Card >
        );
    }
}

export default GradeCalculatorForm;
