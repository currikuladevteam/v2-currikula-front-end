import React, { Component } from "react";
import { Collapse, Icon, Row, Col } from "antd";
import AssignmentPanels from './AssignmentPanels';
const Panel = Collapse.Panel;

export default class YearPane extends Component {

    renderModules(modules) {
        return modules.map((module, index) => {
            return (
                <Panel header={
                    <h3 >
                        <Row gutter={10}>
                            <Col span={14} style={{ marginTop: 5 }}>
                                {module.title}
                            </Col>
                            <Col span={4} >
                                {/* <Input addonBefore={<Icon type="pie-chart" />} /> */}
                                <h4>
                                    <Icon type="copyright" style={{ marginRight: 10, }} />
                                    {module.credits}
                                </h4>

                            </Col>
                            <Col span={4}>
                                {/* <Input addonBefore={<Icon type="file-done" />} /> */}
                                <h4>
                                    <Icon type="file-done" style={{ marginRight: 10, }} />
                                    {module.result ? module.result : "N/A"}
                                </h4>
                            </Col>
                            <Col span={1}>
                                <Icon style={{ marginLeft: 10, }} type="edit" />
                            </Col>
                        </Row>
                    </h3>
                }
                    key={index}
                >
                    {module.assignments.map((assignment, index) => {
                        return <AssignmentPanels assignment={assignment} key={index} />
                    })}



                </Panel>
            )
        })
    }


    render() {
        return (
            <Collapse
                // defaultActiveKey={["0"]}
                bordered={false}
            >
                {this.renderModules(this.props.modules)}

            </Collapse>
        );
    }
}
