import React, { Component } from 'react';
import { Card, Avatar, Button, Row, Col, Icon } from 'antd';
import { getYearString } from '../../../functions/helpers/UserHelpers';
import { UserConsumer } from '../../providers/UserProvider';
import EditProfileCardModal from '../../modals/EditProfileCardModal';

const { Meta } = Card;

class UserProfile extends Component {
	constructor(props) {
		super(props);
		this.state = {
			user: null,
			EditProfileCardModalVisible: false
		};
	}

	openModal = modal => {
        console.log(modal)
		if (modal === 'profileCardEdit') {
			this.setState({
				EditProfileCardModalVisible: true
			});
		}
	};
	closeModal = modal => {
		if (modal === 'profileCardEdit') {
			this.setState({
				EditProfileCardModalVisible: false
			});
		}
	};

	render() {
		return (
			<UserConsumer>
				{/* How to implement the Consumer */}
				{({ user }) => {
					if (!user) return <Card loading={true} />;
					const {
						name: { first, last },
						course,
						// photo,
						year
					} = user.profile;
					const { university } = user;
					return (
						<Card loading={!user} hoverable>
						<EditProfileCardModal
								profileData={user}
								openModal={() => this.openModal('profileCardEdit')}
								closeModal={() => this.closeModal('profileCardEdit')}
								visible={this.state.EditProfileCardModalVisible}
							/>
							<Row>
								<Col span={6}>
									<Meta
										avatar={
											<Avatar
												// src={photo ? photo : null} 
												// icon={!photo ? "user" : null}
												style={{
													width: 100,
													height: 100,
													display: 'flex',
													flexDirection: 'column',
													justifyContent: 'center',
													backgroundColor: 'rgba(0,0,0,0)',
													border: this.props.goldProfile
														? '3px solid gold '
														: '3px solid green'
												}}
											><Icon type="user" style={{fontSize: '3rem', color: 'green'}} /></Avatar>
										}
									/>
								</Col>
								<Col span={11}>
									<h1
										style={{ marginBottom: 0 }}
									>
									{`${first} ${last ? last : ''}`}
									</h1>
									<div style={{ paddingLeft: 5 }}>

										<b>{course ? course : 'Courseless'}</b>
										<p>
											{university.name ? university.name : "Universityless" } { year ? `(${getYearString(year)})` : "Yearless" }
										</p>
									</div>
								</Col>
								<Col span={6}>
									{/*We didn't write a new component but used this method of -50px to resolve sizing issues. */}
									{university.shield ? <img
										style={{
											width: '80%',
											marginTop: '-50px'
										}}
										src={university.shield}
										alt="!Big shield!!!"
									/> : ""}
									
								</Col>

								<Col span={1}>
									<Button
										onClick={() => this.openModal('profileCardEdit')}
										type="circle"
										icon="edit"
									/>
								</Col>
							</Row>
						</Card>
					);
				}}
			</UserConsumer>
		);
	}
}
export default UserProfile;
