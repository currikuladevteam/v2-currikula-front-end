import React, { Component } from 'react';
import { Card, Row, Col, Spin } from 'antd';
import UserStatCircle from './UserStatCircle';
import StatToolTip from '../../../tooltips/StatToolTips.json'
import { UserConsumer } from '../../providers/UserProvider';



class UserStatsCard extends Component {

    returnStatObject(user) {
        const userStats = user.stats;
        return StatToolTip.tool_tips.map((stat, index) =>
            <Col span={8} key={index} style={{ textAlign: 'center', padding: 10 }}>
                <UserStatCircle stat={userStats[stat.dataId]} type={stat.type} tooltip={stat.tooltip} />
                <h4 style={{ marginTop: 5 }}>{stat.name}</h4>
            </Col>)
    }

    render() {

        return (
            <UserConsumer>
                {({ user }) => {
                    return (
                        <Card
                            hoverable
                            style={{ cursor: 'default' }}
                        >
                            <h2
                                style={{ textAlign: 'center', marginBottom: 20 }}>
                                YOUR LIFE TIME STATS
                            </h2>
                            <div style={{ minHeight: 256 }}>
                                <Row type="flex" justify="space-around">
                                    {!user ?
                                        <Spin style={{ marginTop: 50 }} tip="Loading..." />
                                        : this.returnStatObject(user)}
                                </Row>
                            </div>
                        </Card>
                    );
                }}
            </UserConsumer>
        );
    }

}

export default UserStatsCard;
