import React, { Component } from 'react';
import { Row, Col, Layout } from 'antd';
import ProfileCard from './ProfileCard';
import UserStatsCard from './UserStatsCard';


class UserProfile extends Component {
  state = {
    user: null
  }

  render() {
    return (
      <Layout style={{ height: '100vh', justifyContent: 'center' }} >
        <div>
          <Row type="flex" justify="center" >
            <Col span={16} style={{ marginBottom: '50px' }}>
              {/* Using the consumer to load data in lower */}
              <ProfileCard />
            </Col>
          </Row>
          <Row type="flex" justify="center">
            <Col span={16} style={{ display: 'inline-block' }}>
              <UserStatsCard />
            </Col>
          </Row>
        </div>
      </Layout>
    );
  }
}



export default UserProfile;
