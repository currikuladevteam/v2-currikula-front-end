import React, { Component } from "react";
import { Tooltip } from 'antd';
import { convertMsToHours } from '../../../functions/helpers/Time';

const circleStyle = {
  display: "flex",
  justifyContents: "center",
  height: 75,
  width: 75,
  border: 'green 2px solid',
  borderRadius: 100,
  fontSize: 18,
  margin: "0 auto",
  alignItems: "center",
  color: 'green'
};


export default class UserStatCircle extends Component {



  typeChange(type, toChange) {
    switch (type) {
      case 'h':
        return convertMsToHours(toChange)
      default:
        return toChange
    }
  }

  render() {
    return (
      <div>
        <Tooltip placement="top" title={<h4 style={{ textAlign: 'center', color: 'white' }}>{this.props.tooltip}</h4>} overlayStyle={{ textAlign: 'center !important' }}>
          <div style={circleStyle} className='statCircleHoverClass'>
            <div style={{ margin: "0 auto" }}>
              <b style={{ marginLeft: '-2px' }}>
                {this.props.stat ? this.typeChange(this.props.type, this.props.stat) : 0}
                {this.props.type ? this.props.type : ""}
              </b>
            </div>
          </div>
        </Tooltip>
      </div>
    );
  }
}
