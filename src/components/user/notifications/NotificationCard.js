import React, { Component } from 'react'
import { Card, Icon } from 'antd';


export default class NotificationCard extends Component {
    // constructor(props){
    //     super(props);
    //     this.state = {
    //         loading: this.props.loading
    //     }
    // }
  
    render() {
        const iconStyle = {
            float: 'right'
        }
        const iconWarn = {
            color: 'red'
        }
        const iconOkay = {
            color: 'green'
        }

        const { importance, notificationText, title, other, actionData, loading } = this.props;
        return (
            <Card
                hoverable
                title={(loading === true) ? 'Loading...' : ''}
                style={{marginBottom: 20, borderRadius: 4, cursor: 'default'}}
                bodyStyle={{padding: 15}}
                loading={loading}
            >
            <h3> {title} 
                <Icon
                    type={importance === 1 ?  "exclamation-circle" : "info-circle"}
                    style={importance ===  1 ?
                        {...iconStyle, ...iconWarn } :
                        {...iconStyle, ...iconOkay }}    
                />
            </h3>
                {notificationText} {other ? <b>{actionData.title}</b> : '' }

            </Card>
        )
    }
}
