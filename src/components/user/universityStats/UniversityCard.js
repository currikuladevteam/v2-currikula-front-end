import React, { Component } from 'react';
import { Card } from 'antd';
import UniversityCardRow from './UniversityCardRow';

class UniversityCard extends Component {

    getCorrectKey(key){
        switch (key) {
            case "studiousSub":
                return 'total_study_hours';
            case "prolificSub":
                return 'total_words_written';
            case "studiousUni":
                return 'total_study_hours';
            case "prolificUni":
                return 'total_words_written';
            default: 
                return "";
            
        }
    }

    render() {
        return (
            <Card style={{ width: 400 }}>
                <h3>{this.props.data.title}</h3>
                {this.props.data.top5.map((uni, index) => 
                    <UniversityCardRow
                    key={index}
                    index={index}
                    type={this.props.data.type}
                    name={uni.name}
                    amount={uni[this.getCorrectKey(this.props.type)]} 
                    />
                    )}
            </Card>
        );
    }
}

export default UniversityCard;
