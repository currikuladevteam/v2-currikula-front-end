import React, { Component } from 'react';
import { Row, Col, Layout, Button } from 'antd';
import HeaderTitle from '../../HeaderTitle';
import UniversityCard from './UniversityCard';

import {
	getAllUniversities,
	getAllSubjects
} from '../../../demo-data/fake-apis/universities.api';

// const studiosUniversities = [
// 	{
// 		name: 'Exeter',
// 		amount: 300056
// 	},
// 	{
// 		name: 'Hull',
// 		amount: 298445
// 	},
// 	{
// 		name: 'Leciester',
// 		amount: 109887
// 	},
// 	{
// 		name: 'Cambridge',
// 		amount: 17445
// 	},
// 	{
// 		name: 'Ambleside',
// 		amount: 9145
// 	}
// ];
// const prolificUniversities = [
// 	{
// 		name: 'Exeter',
// 		amount: 8239491
// 	},
// 	{
// 		name: 'Leciester',
// 		amount: 6542912
// 	},
// 	{
// 		name: 'Cambridge',
// 		amount: 1345768
// 	},
// 	{
// 		name: 'Hull',
// 		amount: 786482
// 	},
// 	{
// 		name: 'Kings College London',
// 		amount: 128475
// 	}
// ];
// const prolificSubjects = [
// 	{
// 		name: 'English',
// 		amount: 1450002
// 	},
// 	{
// 		name: 'Philosophy',
// 		amount: 1184562
// 	},
// 	{
// 		name: 'History',
// 		amount: 920341
// 	},
// 	{
// 		name: 'Social Science',
// 		amount: 576782
// 	},
// 	{
// 		name: 'Pyschology',
// 		amount: 431224
// 	}
// ];
// const studiousSubjects = [
// 	{
// 		name: 'Economics',
// 		amount: 54299
// 	},
// 	{
// 		name: 'History',
// 		amount: 52998
// 	},
// 	{
// 		name: 'Biology',
// 		amount: 34214
// 	},
// 	{
// 		name: 'Philosophy',
// 		amount: 23495
// 	},
// 	{
// 		name: 'Business',
// 		amount: 8237
// 	}
// ];

class UniversityStats extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loading: true,
			stats: {
				prolificUni: {
					title: 'Most Prolific Universities',
					type: 'words',
					top5: ''
				},
				studiousUni: {
					title: 'Most Studious Universities',
					type: 'hours',
					top5: ''
				},
				prolificSub: {
					title: 'Most Prolific Subjects',
					type: 'words',
					top5: ''
				},
				studiousSub: {
					title: 'Most Studious Subjects',
					type: 'hours',
					top5: ''
				}
			}
		};
	}

	componentDidMount() {
		getAllUniversities().then(unis => {
			let sortedUniversitiesWritten = JSON.parse(JSON.stringify(unis.universities));
			let sortedUniversitiesHours = JSON.parse(JSON.stringify(unis.universities));
			sortedUniversitiesWritten = sortedUniversitiesWritten.sort((a, b) =>
				a.total_words_written > b.total_words_written ? -1 : 1
			);
			sortedUniversitiesHours = sortedUniversitiesHours.sort((a, b) =>
				a.total_study_hours > b.total_study_hours ? -1 : 1
			);
			getAllSubjects().then(subs => {
				let sortedSubjectsWritten = JSON.parse(JSON.stringify(subs.subjects));
				let sortedSubjectsHours = JSON.parse(JSON.stringify(subs.subjects));
				sortedSubjectsWritten = sortedSubjectsWritten.sort((a, b) =>
					a.total_words_written > b.total_words_written ? -1 : 1
				);
				sortedSubjectsHours = sortedSubjectsHours.sort((a, b) =>
					a.total_study_hours > b.total_study_hours ? -1 : 1
        );
				this.setState({
          stats: {
            studiousSub: {
              ...this.state.stats.studiousSub,
              top5: sortedSubjectsHours.splice(0,5)
            },
            prolificSub: {
              ...this.state.stats.prolificSub,
              top5: sortedSubjectsWritten.splice(0,5)
            },
            studiousUni: {
              ...this.state.stats.studiousUni,
              top5: sortedUniversitiesHours.splice(0,5)
            },
            prolificUni: {
              ...this.state.stats.prolificUni,
              top5: sortedUniversitiesWritten.splice(0,5)
            }
          },
          loading: false
					
        });
        console.log('state', this.state)

			});
		});

	}
	render() {
		const uniKeys = Object.keys(this.state.stats);

		return (
			<div style={{ height: '100vh' }}>
				<HeaderTitle title="UNIVERSITY STATS" marginTop="30px" />
				<Layout
					style={{
						height: `calc(100vh - 86px)`,
						minHeight: 400,
						justifyContent: 'center'
					}}
				>
					<Row type="flex" justify="space-around">
						{!this.state.loading ? (
							uniKeys.map((key, index) => (
								<Col key={index} style={{ margin: 10 }}>   
									<UniversityCard data={this.state.stats[key]} type={key} />
									<h4 style={{ textAlign: 'center', margin: 10 }}>
										<Button type="primary" ghost>
											Share This
										</Button>
									</h4>
								</Col>

							))
						) : (
							<div>Loading...</div>
						)}
					</Row>
				</Layout>
			</div>
		);
	}
}

export default UniversityStats;


// next steps: 
// data is now loading through the components
// 
