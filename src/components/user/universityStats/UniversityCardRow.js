import React, { Component } from 'react';
import { Row, Col } from 'antd';

class UniversityCard extends Component {
    render() {
        return (
            <Row>
                <Col style={{ float: 'left' }}>{this.props.index + 1}. {this.props.name}</Col>
                <Col style={{ float: 'right' }}>{this.props.amount} {this.props.type}</Col>
            </Row>
        );
    }
}

export default UniversityCard;
