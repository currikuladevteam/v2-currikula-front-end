import React, { Component } from 'react';
import { Card, message } from 'antd';
import CopyToClipBoard from '../../../functions/helpers/CopyToClipBoard';
import { UserConsumer } from '../../providers/UserProvider';

export default class ReferralCode extends Component {

    handleClick(code) {
        CopyToClipBoard(code)
        function returnHTML() {
            return document.getElementById('referralLayout');
        }
        // console.log(document.body.childNodes[3].childNodes[0].childNodes[0])
        message.success('Copied to Clipboard Successfully', message.config({
            getContainer: returnHTML,
            maxCount: 4,
            duration: 2
        }))
    }

    render() {
        return (
            <UserConsumer>
                {(({ user }) => {
                    console.log(user)
                    return (
                        <div style={{ textAlign: 'center' }}>
                            <p>Your unique referral code is below. Use this to claim earn points towards rewards.</p>
                            <p>
                                <b>Click to copy the link to send to friends.</b>
                            </p>
                            <Card
                                onClick={() => this.handleClick(this.props.referralCode)}
                                hoverable
                                style={{ background: 'green', color: 'white', width: 200, margin: '0 auto', borderRadius: 5 }}>
                                <h2
                                    className="referralCode" style={{ margin: 0, color: 'white', minHeight: 30 }}>
                                    {user ? user.referrals.code : "Not found"}
                                </h2>
                            </Card>
                        </div>
                    )
                })}
            </UserConsumer>
        )
    }
}
