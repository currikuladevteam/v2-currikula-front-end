import React, { Component } from 'react';
import { Row, Col } from 'antd';

class ReferralInformationCard extends Component {
    render() {
        return (
            <div style={{ textAlign: 'center' }}>
                <h2 >CURRIKULA'S REFERRAL PROGRAM</h2>
                <p>Currikula's referral program is here to reward those that want to help others. If you believe Currikula is helping you (or you just want some free stuff) then use your referral code to stack up points.</p>
                <p>Points are rewarded for the following actions:</p>
                <Row type="flex" justify="center">
                    <Col span={14} style={{ display: 'inline-block', textAlign: 'left' }}>
                        <ul style={{
                            width: '271px',
                            margin: '0px auto 10px auto'
                        }}>
                            <li>
                                A person signing up with your code
                            </li>
                            <li>
                                Them starting their first project
                            </li>
                            <li>
                                Them finishing their first project
                            </li>
                            <li>
                                Them finishing 3 projects
                            </li>
                        </ul>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default ReferralInformationCard;
