import React, { Component } from 'react';

export default class RewardsCarouselPane extends Component {
    render() {
        const { component, reward, level, description, background } = this.props;
        const paneStyle = {
            height: '400px',
            background: background,
            textAlign: 'center',
            padding: 40,
            color: 'white',
            borderRadius: 4
        }
        return (
            <div style={paneStyle}>
                <div style={{ height: 180, padding: 20 }}>
                    {component}
                </div>
                <h1 style={{ color: 'white' }}>{level}</h1>
                <h4 style={{ color: 'white' }}>{reward}</h4>
                <p>{description}</p>
            </div>
        )
    }
}
