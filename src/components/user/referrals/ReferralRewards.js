import React, { Component } from 'react';
import { Carousel, Avatar, Icon } from 'antd';
import RewardsCarouselPane from './RewardsCarouselPane';
import { UserConsumer } from '../../providers/UserProvider';

import MoneyBags from '../../../images/rewards/money-bag.svg'
import TShirt from '../../../images/rewards/t-shirt.svg'
import Notebook from '../../../images/rewards/custom-notebook.svg'
import BeerMug from '../../../images/rewards/beer-mug.svg'



export default class ReferralRewards extends Component {
    renderPanes(user) {
        const photo = user ? user.profile.photo : null;

        const referralLevels = [{
            title: 'Level 1 (50 Points)',
            reward: 'GOLD PROFILE RING',
            background: '#3ea000ad',
            component: photo ? <Avatar
                src={photo}
                style={{
                    width: 150, height: 150, border: '5px solid gold',
                }}
            /> : <Avatar
                style={{
                    width: 150, height: 150,
                    border: '5px solid gold',
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    backgroundColor: 'rgba(0,0,0,0)',
                    margin: '0 auto'
                }}
            > <Icon type="user" style={{ fontSize: '3rem', color: 'gold' }} /></Avatar>,
            description: 'Get 50 points and unlock a golden profile. Your profile will never have looked so magnificent. Demonstrate your high levels of success with this simple but elegant golden band.'
        }, {
            title: 'Level 2 (100 Points)',
            reward: '1 FREE YEAR OF CURRIKULA',
            background: '#004fa0ad',
            component:
                <Avatar
                    src={MoneyBags}
                    style={{ width: 150, height: 150 }}
                />,
            description: "After 100 points we're now talking business. This is where there is monetary reward. We'll credit your account with a whole year of Currikula for free."
        }, {
            title: 'Level 3 (200 Points)',
            reward: 'FREE CURRIKULA T-SHIRT',
            background: '#8f00a0ad',
            component:
                <Avatar
                    src={TShirt}
                    style={{ width: 150, height: 150 }}
                />,
            description: "At 200 points we'll get in touch and send you a physical item through the snail mail. That's right you've earned yourself a Currikula t-shirt."

        }, {
            title: 'Level 4 (300 Points)',
            reward: 'PERSONALISED NOTEBOOK',
            background: '#03d071ad',
            component:
                <Avatar
                    src={Notebook}
                    style={{ width: 150, height: 150, padding: 15 }}
                />,
            description: "A t-shirt is great, but what happens when you don't have your computer? How can you work away from your desk or without a laptop - well if you hit 300 points you'll receive a customised notebook to track your offline school work."

        }, {
            title: 'Level 5 (500 Points)',
            reward: 'A TERM OF FREE DRINKS',
            background: '#d0ad03ad',
            component:
                <Avatar
                    src={BeerMug}
                    style={{ width: 150, height: 150, padding: 5 }}
                />,
            description: "The grand prize, achieve 500 points and we'll buy you a drink a day for a whole term! That's right we'll support your student life."
        }];
        return referralLevels.map(pane => {
            return <div
                key={pane.title}>
                <RewardsCarouselPane
                    level={pane.title}
                    reward={pane.reward}
                    background={pane.background}
                    component={pane.component}
                    description={pane.description}
                />
            </div>
        })
    }

    render() {
        return (
            <UserConsumer >
                {(({ user }) => {
                    console.log(user ? user : 'Not Loaded Yet')
                    return (
                        <Carousel autoplay afterChange={this.onChange} >
                            {this.renderPanes(user)}
                        </Carousel>
                    )
                }
                )}
            </UserConsumer>
        )
    }
}

// userPhoto={this.props.userPhoto}