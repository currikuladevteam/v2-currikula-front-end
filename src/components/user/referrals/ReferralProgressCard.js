import React, { Component } from 'react';
import { Steps, Card, Button } from 'antd';
import { UserConsumer } from '../../providers/UserProvider';

const { Step } = Steps;

export default class ReferralSteps extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return (
            <UserConsumer >
                {(({ user }) => {
                    const refLevel = getReferralLevel(user ? user.referrals.points : 1);
                    const allLevels = getReferralLevel(user ? user.referrals.points : 1, true);
                    const currentLevel = getCurrentLevel(user ? user.referrals.points : 0 );
                    console.log('currentLevel', currentLevel)
                    return (
                        <div style={{ textAlign: 'center' }}>
                            <h2 style={{ margin: 30 }}>
                                Your current progress: <b>{user ? user.referrals.points : "0"}</b> points
                            </h2>
                            <Steps current={currentLevel} style={{ marginBottom: 30 }}>
                                {allLevels.map(item => 
                                    <Step key={item.title} title={item.title} />) 
                                }
                            </Steps>
                            <Card
                                title={
                                    <h3 style={{ textAlign: 'center' }}>
                                        {currentLevel > -1 ?
                                            `Congratulations you are eligible to claim your ${refLevel.title} reward!` 
                                            : `Keep referring friends to Currikula to earn more points.`}
                                    </h3>
                                }
                            >
                                <h2>{refLevel.reward}</h2>
                                <Button type="primary" disabled={!(currentLevel > -1)}> CLAIM REWARD NOW</Button>
                            </Card>

                        </div>
                    )
                })}
            </UserConsumer>
        )
    }
}

const  getCurrentLevel = (referrals) => {
    if (referrals < 50) return -1;
    if (referrals > 49 && referrals < 100) return 0;
    if (referrals > 99 && referrals < 200) return 1;
    if (referrals > 199 && referrals < 300) return 2;
    if (referrals > 299 && referrals < 500) return 3;
    if (referrals > 499) return 4;
}

const getReferralLevel = (referralsPoints, all) => {
    const levels = [{
        title: 'Level 1',
        content: referralsPoints > 49 && referralsPoints < 100 ? 'Completed' : referralsPoints,
        reward: 'GOLD PROFILE RING'
    }, {
        title: 'Level 2',
        content: referralsPoints > 99 && referralsPoints < 200 ? 'Completed' : referralsPoints,
        reward: '1 FREE YEAR OF CURRIKULA'
    }, {
        title: 'Level 3',
        content: referralsPoints > 199 && referralsPoints < 300 ? 'Completed' : referralsPoints,
        reward: 'FREE CURRIKULA T-SHIRT'
    }, {
        title: 'Level 4',
        content: referralsPoints > 299 && referralsPoints < 300 ? 'Completed' : referralsPoints,
        reward: 'PERSONALISED NOTEBOOK'
    }, {
        title: 'Level 5',
        content: referralsPoints > 499 ? 'Completed' : referralsPoints,
        reward: 'A TERM OF FREE DRINKS'
    }]
    if (all) return levels
    if (!all) return levels[getCurrentLevel(referralsPoints)] || {}
}