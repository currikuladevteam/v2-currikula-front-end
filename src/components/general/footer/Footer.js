import React, { Component } from 'react';
import { Row, Col } from 'antd'

class Footer extends Component {
	render() {
		const actualCenter = {
			// 'textAlign': 'center'
			// border:  '1px solid red'
			// verticalAlign: 'middle'
		}

		const headerColor = {
			'color': 'white'
		}

		const footerDiv = {
			padding: '40px 0',
			height: '200px',
			backgroundColor: '#2c3a47',
			color: 'white'

		}


		return (
			<div style={footerDiv}>
				<Row type="flex" justify="center">
					<Col span={4} offset={3} style={actualCenter}>
						<Row ><h3 style={headerColor}>Company</h3></Row>
						<Row>About</Row>
						<Row>Blog</Row>
						<Row>Contact Us</Row>
					</Col>
					<Col span={4} style={actualCenter}>
						<Row><h3 style={headerColor}>Resources</h3></Row>
						<Row>Help Center</Row>
						<Row>The Ultimate Essay Guide</Row>
					</Col>
					<Col span={4} style={actualCenter}>
						<Row><h3 style={headerColor}>Where are we?</h3></Row>
						<Row>Facebook</Row>
						<Row>Instagram</Row>
						<Row>Twitter</Row>
					</Col>
					<Col span={4} style={actualCenter}>
						<Row><h3 style={headerColor}>Business</h3></Row>
						<Row>Term and Conditions</Row>
						<Row>Privacy Policy</Row>
						<Row>Cookie Policy</Row>
						<Row>Work With Us</Row>
					</Col>
				</Row>
			</div>
		)
	}
}

export default Footer;
