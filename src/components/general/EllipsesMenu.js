import React, { Component } from 'react';
import { Dropdown } from 'antd';
export default class EllipsesMenu extends Component {


 
  render() {
    const { menu } = this.props;
    const  menuStyle = {
      minWidth: '400px'
    }
    return (
      <Dropdown style={menuStyle} overlay={menu} trigger={['click']}>
          {this.props.children}
      </Dropdown>
    )
  }
}
