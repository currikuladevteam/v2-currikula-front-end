import React, { Component } from 'react';

class DateConverter extends Component {

  handleDate = (dateString) => {
    const Months = dateString.getMonth();
    const Day = dateString.getDate();
    const Hour = dateString.getHours();
    const Minutes = dateString.getMinutes();
    const Year = dateString.getYear().toString().split("");
    return {
      time: `${Hour}:${Minutes}`,
      date: `${Day}/${Months + 1}/${Year.splice(1, Year.length).join("")}`
    }
  }

  returnDate = (dateProp) => {
    if (typeof(dateProp) === 'string') {
        const dateString = new Date(parseInt(dateProp));
        return this.handleDate(dateString);
    } else if (typeof(dateProp) === 'number') {
        const dateString = new Date(dateProp);
        return this.handleDate(dateString);
    }
  }


  render() {
  const dateObject = this.returnDate(this.props.date)
  const { time, date } = dateObject;
    return (
          <span>
            <b>{time}</b> - {date}
          </span>
    );
  }
}

export default DateConverter;
