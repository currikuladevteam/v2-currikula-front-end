import React from 'react';
import { Route } from 'react-router-dom';

// Used to  pass props through routes from parent components

export default function RouteWrapper(props) {
    const { component: Component, ...opts } = props;
    return (
        <Route {...opts} render={props => (
            <Component {...props} {...opts} />
        )} />
    )
}