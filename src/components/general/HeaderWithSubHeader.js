import React from 'react'

export default function HeaderWithSubHeader(props) {
    const { large, small, subHeader } = props;
    const headerStyle = {
        letterSpacing: '1.1px',
        textTransform: 'uppercase'
    } 
  return (
    <div>
        {large ? <h1 style={headerStyle}>{props.header}</h1> : ''}
        {!large && !small ? <h2 style={headerStyle}>{props.header}</h2> : ''}
        {small ? <h3 style={headerStyle}>{props.header}</h3> : ''}      
        {subHeader ? <p>{props.subHeader}</p> : ''}
    </div>
  )
}
