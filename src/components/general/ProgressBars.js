import React, { Component } from 'react';
import { Progress, Col, Row, Tooltip } from 'antd';
import PercentageColourPicker from '../../functions/helpers/PercentageColourPicker';
class ProgressBars extends Component {

  timeTill(dueDate) {
    const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    dueDate = new Date(dueDate);
    const currentTime = new Date().getTime();
    // const diffDaysFromToday = Math.round(Math.abs((currentTime - dueDate.getTime()) / (oneDay)));
    const diffDaysFromToday = Math.round((dueDate.getTime()- currentTime) / (oneDay));
    if (diffDaysFromToday < 0) {
      return <span style={{fontSize: 8, fontWeight: 'bolder' }}>HANDED IN</span>
    }
    return diffDaysFromToday + " DAYS"
  }

  // Function to find out the percentage of days that have gone since they started the project, and till it is due.
  calculateDaysLeft(dueDate, startDate) {
    // Value of one day in seconds
    const oneDay = 24 * 60 * 60 * 1000;

    // The timestamp of the due date
    dueDate = new Date(dueDate);

    // What is the current time
    const currentTime = new Date().getTime();

    // What is the start date, this will be provided by the .createAT attribue
    startDate = new Date('January 01, 2019 03:24:00');

    // What is the difference in days from the startdate and the due date, i.e. total number of days the student has to complete the project
    const diffDays = Math.round(Math.abs((startDate.getTime() - dueDate.getTime()) / (oneDay)));

    // What is the time left (in days) from now till the project is due
    const diffDaysFromToday = Math.round(Math.abs((currentTime - dueDate.getTime()) / (oneDay)));
    const nonAbsDaysFromToday = Math.round((dueDate.getTime()- currentTime) / (oneDay));

    if (nonAbsDaysFromToday < 0) {
      return 100
    }
    // What the percentage of the days that are left out of the total is
    const percentOfDaysUsed = parseInt(diffDaysFromToday / diffDays * 100)

    // Inversed the percentage so that it shows what has gone by already rather than remaining
    return 100 - percentOfDaysUsed;
  }

  getFutureDate(timeStamp) {
    timeStamp = new Date(timeStamp)
    const month = timeStamp.toDateString().match(/ [a-zA-Z]*/)[0].replace(" ", "");
    const day = timeStamp.getDate();
    return day + ' ' + month;
  }

  render() {
    const underHeaderStyle = {
      marginTop: '10px',
      marginBottom: 0
    }
    const progressStyles = {
    };

    const { words, improve, project, due } = this.props.progress;
    return (
      <Row style={{ textAlign: 'center' }}>
        <Col span={6}>
          <Tooltip
            title={<h4 style={{color: 'white', textAlign: 'center', marginBottom: 3}}>How far along you are in your project overall</h4>}
            mouseEnterDelay={1}
          >
            <Progress
              strokeColor={'#' + PercentageColourPicker(project)}
              format={percent => <span style={{ fontSize: 12, fontWeight: 'bold' }}>{percent}%</span>}
              type="circle"
              percent={project}
              width={50}
              style={progressStyles} />
            <h5 style={underHeaderStyle}>PROGRESS</h5>
          </Tooltip>

        </Col>
        <Col span={6}>
        <Tooltip
            title={<h4 style={{color: 'white', textAlign: 'center', marginBottom: 3}}>Your project due date and how long till you need to hand it in</h4>}
            mouseEnterDelay={1}
          >
          <Progress
            strokeColor={'#' + PercentageColourPicker(this.calculateDaysLeft(due))}
            format={() => <span style={{ fontSize: 11, fontWeight: 'bold' }}>{this.timeTill(due)}</span>}
            type="circle"
            percent={this.calculateDaysLeft(due)}
            width={50}
            style={progressStyles} />
          <h5 style={underHeaderStyle}>{this.getFutureDate(due)}</h5>
          {/* {console.log(this.calculateDaysLeft(due))} */}
          </Tooltip>
        </Col>
        <Col span={6}>
        <Tooltip
            title={<h4 style={{color: 'white', textAlign: 'center', marginBottom: 3}}>How many of the improvement suggestions you have completed</h4>}
            mouseEnterDelay={1}
          >
          <Progress
            strokeColor={'#' + PercentageColourPicker(improve.completed / improve.total * 100)}
            format={percent => {
              if (percent === 0) {
                return <span style={{ fontSize: 7, fontWeight: 'bold', position: 'absolute', top: -7, left: 2 }}>NOT STARTED</span>
              } else if (percent > 0 && percent < 100) {
                return <div style={{ fontSize: 10, fontWeight: 'bold' }}>
                  {this.props.progress.improve.completed}
                  <br />
                  /{improve.total}
                </div>
              } else if (percent === 100) {
                return <span style={{ fontSize: 11, fontWeight: 'bold' }}>DONE</span>
              }
            }}
            type="circle"
            percent={improve.completed / improve.total * 100}
            width={50}
            style={progressStyles} />
          <h5 style={underHeaderStyle}>IMPROVE</h5>
          </Tooltip>
        </Col>
        <Col span={6}>
        <Tooltip
            title={<h4 style={{color: 'white', textAlign: 'center', marginBottom: 3}}>How many words you have written in your draft out of your word count goal</h4>}
            mouseEnterDelay={1}
          >
          <Progress
            format={percent => <span style={{ fontSize: 12, fontWeight: 'bold' }}>{percent}%</span>}
            strokeColor={'#' + PercentageColourPicker(parseInt((words.current / this.props.progress.words.target) * 100))}
            type="circle"
            percent={parseInt((words.current / words.target) * 100)}
            width={50}
            style={progressStyles} />
          <h5 style={underHeaderStyle}>{words.current}/{words.target}</h5>
          </Tooltip>
        </Col>
      </Row>
    );
  }
}

export default ProgressBars;
