import React, { Component } from 'react';
import { Row, Col } from 'antd';

export default class EllipsesMenuMetaData extends Component {

    truncate = (words, length) => {
        if (typeof length === 'string') {
            const numLength = parseInt(length);
            if (words.split("").length < numLength) {
                return words;
            } else {
                return words.substring(0, numLength) + "...";
            }
        }
    }

    timeSince(timeStamp) {
        timeStamp = new Date(timeStamp)
        var now = new Date(),
            secondsPast = (now.getTime() - timeStamp.getTime()) / 1000;
        if (secondsPast < 60) {
            return (<span><b>{parseInt(secondsPast)}</b>s</span>);
        }
        if (secondsPast < 3600) {
            return (<span><b>{parseInt(secondsPast / 60)}</b>m</span>);
        }
        if (secondsPast <= 86400) {
            return (<span><b>{parseInt(secondsPast / 3600)}</b>h</span>);
        }
        if (secondsPast > 86400) {
            const day = timeStamp.getDate();
            const month = timeStamp.toDateString().match(/ [a-zA-Z]*/)[0].replace(" ", "");
            const year = timeStamp.getFullYear() === now.getFullYear() ? "" : " " + timeStamp.getFullYear();
            return day + " " + month + year;
        }
    }

    getProjectTitle(projects, note_project_id) {
        const project = projects.filter(project => project.id === note_project_id)
        if (project.length === 1) {
            return project[0].title
        } else {
            return 'Unattached'
        }
    }

    render() {
        const { createdAt, editedAt, exportedAt, projects, projectId } = this.props;
        let projectTitle = null;
        if (projects && projectId) {
            projectTitle = this.getProjectTitle(projects, projectId)
        }
       
        
        return (
            <div style={{ padding: 10, fontSize: 'small', color: '#9c9c9c' }}>

                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <span>Last edited: </span>
                    {editedAt ? this.timeSince(editedAt) : 'Unknown'}
                </div>
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    <span>Created at: </span>
                    {createdAt ? this.timeSince(createdAt) : 'Unknown'}
                </div>
                <Row gutter={3}>
                    {/* If there is a project passed to the meta data menu, truncate and render, otherwise, render nothing */}
                    {projectTitle ? <div>
                        <Col span={6} style={{ margin: 0 }}>
                            Project:
                        </Col>
                        <Col span={18} style={{ fontWeight: 'bold', textAlign: 'right', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}> {projectTitle}
                        </Col>
                    </div> : ''}
                    {exportedAt ? <div>
                        <Col span={6} style={{ margin: 0 }}>
                            Exported:
                        </Col>
                        <Col span={18} style={{ fontWeight: 'bold', textAlign: 'right' }}>                 {this.timeSince(exportedAt)}
                        </Col>
                    </div> : ''}
                </Row>
            </div>
        )
    }
}


    //   getDate(timeStamp) {
    //     const timeString = new Date(timeStamp).toLocaleString();
    //     const timeArray = timeString.split(', ');
    //     const secondsRemoved = timeArray[1].replace(/(:\d{2}(?!:))/g, '');
    //     const yearsEdited = timeArray[0].replace(/(\d{2}(?=\d{2}))/g, '');
    //     const reversedTimeArray = [secondsRemoved, yearsEdited];
    //     const newDateString = reversedTimeArray.join(' - ');
    //     return newDateString;
    //   }