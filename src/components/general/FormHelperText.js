import React from 'react'

export default function FormHelperText(props) {
  return (
    <p style={{marginBottom: 10, marginTop: -10, color: 'lightgrey'}}>
      {props.text}
    </p>
  )
}
