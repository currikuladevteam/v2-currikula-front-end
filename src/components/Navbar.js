import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Menu, Icon } from 'antd';

class Navbar extends Component {
  state = {
    theme: 'light',
    top: 10,
    bottom: 10
  };

  render() {
    return (
      <Menu
        defaultSelectedKeys={[
          this.props.location.pathname.match(/\/[a-z]+/)[0]
        ]}
        theme="light"
        mode="vertical"
        style={{
          height: '100vh',
          display: 'flex',
          position: 'fixed',
          flexDirection: 'column',
          justifyContent: 'flex-end',
          left: 0,
          zIndex: 10
        }}
      >
        <Menu style={{ marginBottom: 'auto' }}>
          <Menu.Item key="/dashboard">
            <Link to="/dashboard">
              <Icon type="home" />
            </Link>
            <span>Home</span>
          </Menu.Item>
          <Menu.Item key="/projects">
            <Link to="/projects">
              <Icon type="folder-open" />
            </Link>

            <span>Projects</span>
          </Menu.Item>
          <Menu.Item key="/documents">
            <Link to="/documents">
              <Icon type="file" />
            </Link>
            <span>All Documents</span>
          </Menu.Item>
        </Menu>

        <Menu>
          <Menu.ItemGroup>
            <Menu.Item key="/user">
              <Link to="/user/profile">
                <Icon type="user" />


              </Link>

              <span> User </span>
              
            </Menu.Item>
            <Menu.Item key="/settings">
              <Link to="/settings">
                <Icon type="setting" />
              </Link>

              <span> Settings </span>
            </Menu.Item>
          </Menu.ItemGroup>
        </Menu>
      </Menu>
    );
  }
}



export default withRouter(Navbar);
