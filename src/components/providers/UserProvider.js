import React, { Component } from 'react'
import { getUser } from '../../demo-data/fake-apis/users.api';
import { getProjects } from '../../demo-data/fake-apis/projects.api';
import { getDrafts } from '../../demo-data/fake-apis/drafts.api';
import { getBibliographies } from '../../demo-data/fake-apis/bibliographies.api';
import { getNotes } from '../../demo-data/fake-apis/notes.api';


export const UserContext = React.createContext();

export const UserConsumer = UserContext.Consumer

export default class UserProvider extends Component {


    state = {
        loggedIn: false,
        loginUser: (user_id) => this.loginUser(user_id),
        registerUser: (user_id) => this.registerUser(user_id),
        defaultUser: this.fetchAllData(1),
        deleteNote: (note) => this.deleteNote(note),
        deleteBibliography: (bibliography) => this.deleteBibliography(bibliography),
        checkFileAccess: () => this.checkFileAccess(),
        changeSetting: (setting) => this.changeSetting(setting)
    }

    loginUser(user_id) {
        this.setState({
            loggedIn: true,
        },  this.fetchAllData(user_id))
    }
    registerUser(user_id) {
        this.setState({
            loggedIn: true,
        },  this.fetchAllData(user_id))
    }

    checkFileAccess(file_id, file_type) {
        // checks the file being requested against the ids of the files that the user has
        switch (file_type) {
            case "draft":
                return this.state.user.documents.drafts.indexOf(file_id);
            case "note":
                return this.state.user.documents.notes.indexOf(file_id);
            case "bibliography":
                return this.state.user.documents.notes.indexOf(file_id);
            case "project":
                return this.state.user.projects.indexOf(file_id);
            default: 
                throw new Error("File Not Found");
        }
    }

    deleteBibliography(deletedBibliography){
        const nonDeletedBibliographies = this.state.bibliographies.filter(bibliography => deletedBibliography.id !== bibliography.id)
        const bibliographies = [ 
            ...nonDeletedBibliographies,
            deletedBibliography
          ]
        this.setState({
            bibliographies
        })
    }

    changeSetting(newSettings){
        const user = this.state.user
        this.setState({
            user: {
                ...user,
                settings: newSettings
            }
        })
    }

    deleteNote(deletedNote){
        const nonDeletedNotes = this.state.notes.filter(note => deletedNote.id !== note.id)
        const notes = [ 
            ...nonDeletedNotes,
            deletedNote
          ]
        this.setState({
            notes
        })
    }


    fetchAllData(user_id) {
        getUser(1).then(user => {
            user = user[0];
            getNotes(user.documents.notes).then(notes => {
                getDrafts(user.documents.drafts).then(drafts => {
                    getProjects(user.projects).then(projects => {
                        getBibliographies(user.documents.bibliographies).then(bibliographies => {
                            this.setState({
                                bibliographies: bibliographies,
                                notes: notes,
                                drafts: drafts,
                                projects: projects,
                                user: user,
                                user_id
                            })
                        })
                    })
                })
            })
        })
    }


    // Function to gather all data for the sites main pages
    // * Dashboard 
    // * Projects
    // * Documents
    // * Settings / User
    // Gather this data so that the site doesn't have to reload every time a new page is loaded. 


    // When saving data or updating data can send a function that will deal with that through context

    render() {
        return (
            <UserContext.Provider value={this.state}>
                {this.props.children}
            </UserContext.Provider>
        )
    }
}
