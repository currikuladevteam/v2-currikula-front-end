import React from 'react';
import { Card } from 'antd';


export default function SettingContainer(props) {
    return (
        <div
            id={props.containerId}
            style={{ minHeight: '100vh', display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
            <Card 
                hoverable
                style={{ margin: '0 40px', cursor: 'default' }}
                >
                {props.children}
            </Card>

        </div>
    )
}
