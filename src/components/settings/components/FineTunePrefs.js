import React, { useContext } from 'react';

import { Checkbox, Row, notification } from 'antd';
import { unSnakeCase } from '../../../functions/helpers/TextHelpers';
import Log from '../../../functions/api/logs/Log';
import { UserContext } from '../../providers/UserProvider';

function onChange(change, context, changeSetting, user) {
	const value = change.target.value
	console.log('Value: ', value)
	const checked = change.target.checked

	console.log('Checked: ', checked)

	const marketing = context.marketing
	console.log('Marketing: ', marketing)

	let changed = {}
	changed[value] = checked;
	console.log('Changed: ', changed)

	const changeContext = {
		...context,
		marketing: {
			...marketing,
			choices: {
				...marketing.choices,
				...changed
			}
		}
	}

	// Remove the focus on the input after deselection
	change.nativeEvent.srcElement.blur()

	// Changes the context state
	// Will need to also send the change to the back end
	changeSetting(changeContext)
	const event = {
		type: 'setting',
		message: <p>You changed your marketing preference {unSnakeCase(value)} to: <b>{checked ? "receiving" : "not receiving"}</b></p>,
		document: {
			type: unSnakeCase(value)
		},
		oldInformation: !checked,
		newInformation: checked
	}
	const log = new Log({ event, type: "metadata", user }).log();
	// Fire off the notification (passing in the notification object)
	// This is required to give a location to the notification
	log.notify(notification)();
}


export default function FineTunePrefs(props) {
	const { user, changeSetting } = useContext(UserContext)
	const settings = user ? user.settings : null
	return (
		<div style={props.style}>
			{renderCheckboxes(props, settings, changeSetting, user)}
		</div>
	);
}

const settingsArray = [
	{
		title: "Currikula news and information",
		value: "news"
	},
	{
		title: "Blogs",
		value: "blogs"
	},
	{
		title: "Currikula updates",
		value: "currikula_updates"
	},
	{
		title: "Rewards and free stuff",
		value: "rewards"
	},
	{
		title: "Educational news",
		value: "education_news"
	},
]

function renderCheckboxes(props, settings, changeSetting, user) {
	return settingsArray.map(setting => 
	<Row
		key={setting.title}>
		<Checkbox 
			checked={props.options[setting.value]} 
			onChange={(change) => onChange(change, settings, changeSetting, user)}
			value={setting.value}>
			{setting.title}
		</Checkbox>
	</Row> )
}