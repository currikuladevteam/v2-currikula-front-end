import React, { Component } from 'react';

import { Form, Input, Button, notification } from 'antd';
import Log from '../../../functions/api/logs/Log';

class ChangeEmail extends Component {
	state = {
		confirmDirty: false,
	};

	handleSubmit = e => {
		e.preventDefault();
		this.props.form.validateFieldsAndScroll((err, values) => {
			if (err) {
				return ''
			} 

			const event = {
				type: 'metadata',
				message: <p>You changed your email to - <b>{values.email}</b></p>,
				document: {
					type: 'email'
				},
				oldInformation: this.props.user.email,
				newInformation: values.email
			}

			const log = new Log({ event, type: "metadata", user: this.props.user}).log();
			// Fire off the notification (passing in the notification object)
			// This is required to give a location to the notification
			log.notify(notification)();
		});
	};

	handleConfirmBlur = e => {
		const value = e.target.value;
		this.setState({ confirmDirty: this.state.confirmDirty || !!value });
	};

	compareToFirstDetails = (rule, value, callback) => {
		const form = this.props.form;
		if (value && value !== form.getFieldValue('email')) {
			callback('This does not match your first email entry!');
		} else {
			callback();
		}
	};

	validateToNextPassword = (rule, value, callback) => {
		const form = this.props.form;
		if (value && this.state.confirmDirty) {
			form.validateFields(['confirm'], { force: true });
		}
		callback();
	};

	// componentDidUpdate() {
	// 	if (this.props.details && !this.state.email) {
	// 		this.setState({
	// 			email: this.props.details.email
	// 		})
	// 		this.props.form.setFieldsValue({
	// 			email: this.props.details.email,
	// 		});
	// 	}
	// }

	render() {
		// const { details } = this.props;

		const { getFieldDecorator } = this.props.form;

		const formItemLayout = {
			labelCol: {
				xs: { span: 24 },
				sm: { span: 20 }
			},
			wrapperCol: {
				xs: { span: 24 },
				sm: { span: 20 }
			}
		};

		return (
			<Form onSubmit={this.handleSubmit}>
				{/* <====================== initialization ==========================> */}

				<Form.Item {...formItemLayout}>
					{getFieldDecorator('email', {
						rules: [
							{
								type: 'email',
								message: 'The input is not valid e-mail address'
							},
							{
								required: true,
								message: 'Please input your e-mail!'
							}
						]
					})(<Input placeholder="Please enter your new email here"  type="email"/>)}
				</Form.Item>

				{/* <============================ confirmation ====================================> */}
				<Form.Item {...formItemLayout}>
					{getFieldDecorator('confirm', {
						rules: [
							{
								required: true,
								message: 'Please confirm your new email'
							},
							{
								validator: this.compareToFirstDetails
							}
						]
					})(<Input placeholder="Please confirm your new email" type="email" onBlur={this.handleConfirmBlur} />)}
				</Form.Item>

				<Form.Item wrapperCol={{ span: 6 }}>
					<Button type="primary" htmlType="submit">
						Save Email
					</Button>
				</Form.Item>
			</Form>
		);
	}
}

// <=================== allows for the creation the form =======================>

const WrappedChangeEmail = Form.create({ name: 'change_email' })(
	ChangeEmail
);

export default WrappedChangeEmail;