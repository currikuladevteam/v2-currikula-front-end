import React from 'react';
import { Card, Icon, Row, Col, Button } from 'antd';

const cardCircle = {
	borderRadius: '70px',
	backgroundColor: 'lightgrey',
	width: '70px',
	height: '70px',
	display: 'flex'
};

export default function BillingCard(props) {
	return (
		<div style={props.style}>
			<Card style={{ width: 350, borderRadius: 8 }} hoverable>
				<Row>
					<Col span={10} style={{ display: 'flex', justifyContent: 'center' }}>
						<div style={cardCircle}>
							<Icon
								type="credit-card"
								style={{ fontSize: '16px', margin: 'auto' }}
							/>
						</div>
					</Col>
					<Col span={14}>
						<div>XXXX XXXX XXXX 5968</div>
						<div>John Smith</div>
						<div>09/20</div>
					</Col>
				</Row>
				<Row style={{ marginTop: 20 }}>
					<Button type="primary" block>Change</Button>
				</Row>
			</Card>
		</div>
	);
}

