import React from 'react'
import Log from '../../../functions/api/logs/Log';
import { notification, Modal } from 'antd';



const confirm = Modal.confirm

export function showConfirm(user) {

    // Logging Event 
    const event = {
      type: 'subscription',
      message: <p>You cancelled your subscription</p>,
      document: {
        type: 'user_deletion',
        project_id: null
      }
    }
    confirm({
      title: `Do you want to cancel your subscription?`,
      content: (
        <span>
          Cancelling your subscription will be a permanent action.
        </span>
      ),

      onOk() {
        return new Promise((resolve, reject) => {
          console.log('subscription deleted', user)

          setTimeout(() => {
            resolve();
          }, 1000)
        })
          .then((error) => {
            if (error) console.log(error) 
            // Log the events and the type of events
            const log = new Log({event, type: "delete", user }).log(); 
        // Send the log to the Time Line Create event function to add to project if there is a project
        //     TimeLine.createEvent(log)
            console.log(log)
            // Fire off the notification (passing in the notification object)
           // This is required to give a location to the notification
            log.notify(notification)();
      })

    

          .catch(error => {
            
            // If there is an error log the event and attempt
            const log = new Log({error, event}, "error").log(); // log the events and the type of events
            log.notify(notification)();
            return false
          })
        
      },
      onCancel() {
       }
    });
  };