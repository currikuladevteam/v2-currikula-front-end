import React, { useContext } from 'react';
import { Checkbox, Row, notification } from 'antd';
import { UserContext } from '../../providers/UserProvider';
import Log from '../../../functions/api/logs/Log';
import { unSnakeCase } from '../../../functions/helpers/TextHelpers';



function onChange(change, context, changeSetting, user) {
	const value = change.target.value
	const checked = change.target.checked
	const notifications = context.notifications
	let changed = {}
	changed[value] = checked;
	const changeContext = {
		...context, 
		notifications: {
			...notifications,
			choices: {
				...notifications.choices,
				...changed
			}
		}
	}
	
	// Remove the focus on the input after deselection
	change.nativeEvent.srcElement.blur()
	// Changes the context state
	// Will need to also send the change to the back end
	changeSetting(changeContext)

    const event = {
      type: 'setting',
      message: <p>You changed your notification setting default {unSnakeCase(value)} to: <b>{checked ? "receiving" : "not receiving"}</b></p>,
      document: {
        type: unSnakeCase(value)
      },
      oldInformation: !checked,
      newInformation: checked
    }

    const log = new Log({ event, type: "metadata", user}).log();
    // Fire off the notification (passing in the notification object)
    // This is required to give a location to the notification
	log.notify(notification)();
	
}

export default function WhatNotifications(props) {
	const context = useContext(UserContext)
	const { changeSetting, user }  = context;
	const settings = user ? user.settings : null;
	return (
		<React.Fragment>
			{renderCheckboxes(props, settings, changeSetting, user)}
		</React.Fragment>
	);
}

const settingsArray = [
	{
		title: "Project Deadlines (Recommended)",
		value: "project_deadlines"
	},
	{
		title: "Project Updates (Recommended)",
		value: "project_updates"
	},
	{
		title: "Edits",
		value: "edits"
	},
	{
		title: "Weekly summary of work and effort",
		value: "weekly_summary"
	},
]

function renderCheckboxes(props, settings, changeSetting, user) {
	return settingsArray.map(setting => 
	<Row
		key={setting.title}>
		<Checkbox 
			checked={props.options[setting.value]} 
			onChange={(change) => onChange(change, settings, changeSetting, user)}
			value={setting.value}>
			{setting.title}
		</Checkbox>
	</Row> )
}

