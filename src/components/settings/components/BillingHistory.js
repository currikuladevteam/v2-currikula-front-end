import React from 'react';

import { List, Row, Icon, Button } from 'antd';

const data = [
	{
		sub: 'Monthly Currikula Subscription',
		date: '12/01/2001',
		amount: '2.99',
		card: '5678'
	},
	{
		sub: 'Monthly Currikula Subscription',
		date: '12/02/2001',
		amount: '2.99',
		card: '5678'
	},
	{
		sub: 'Monthly Currikula Subscription',
		date: '12/03/2001',
		amount: '2.99',
		card: '5678'
	}
];

export default function BillingHistory(props) {
	return (
		<div style={props.style}>
			<List
				itemLayout="horizontal"
				dataSource={data}
				renderItem={item => (
					<List.Item
						actions={[
							<div style={{ cursor: 'default', color: '#000000a6' }}>
								<div style={{ fontWeight: 'bold' }}>£{item.amount}</div>
								<Row >
									{item.card} <Icon type="credit-card" />
								</Row>
							</div>
						]}
					>
						<Row>
							{item.sub}
							<Row>{item.date}</Row>
						</Row>
					</List.Item>
				)}
			/>
			<div style={{ textAlign: 'center' }} >
				<Button type="primary">Load More</Button>
			</div>
		</div>
	);
}

