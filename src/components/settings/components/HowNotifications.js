import React, { useContext, Fragment } from 'react';
import { Checkbox, Row, notification } from 'antd';
import { UserContext } from '../../providers/UserProvider';

import Log from '../../../functions/api/logs/Log';
// import { unSnakeCase } from '../../../functions/helpers/TextHelpers';

export function upperCase(str) {
	return str.toUpperCase();
  }
  
  export function unSnakeCase(snake) {
	return snake.replace(/_/g, ' ').replace(/\W[a-z]/g, upperCase).replace(/^[a-z]/g, upperCase)
  }

function onChange(change, context, changeSetting, user) {
	const value = change.target.value;
	const checked = change.target.checked;
	const notifications = context.notifications;
	let changed = {};
	changed[value] = checked;
	const changeContext = {
		...context,
		notifications: {
			...notifications,
			receive_via: {
				...notifications.receive_via,
				...changed
			}
		}
	};

	// Remove the focus on the input after deselection
	change.nativeEvent.srcElement.blur();
	// Changes the context state
	// Will need to also send the change to the back end
	changeSetting(changeContext);

	const unSnakeVal = unSnakeCase(value);


const event = {
	type: 'setting',
	message: <p>You changed your notification setting default {unSnakeVal} to: <b>{checked ? "via " + unSnakeVal : "not via " + unSnakeVal}</b></p>,
	document: {
	  type: unSnakeVal
	},
	oldInformation: !checked,
	newInformation: checked
  }

  const log = new Log({ event, type: "metadata", user}).log();
  // Fire off the notification (passing in the notification object)
  // This is required to give a location to the notification
  log.notify(notification)();
  


}

export default function HowNotifications(props) {
	const context = useContext(UserContext);
	const { changeSetting, user } = context;
	const settings = user ? user.settings : null;

	return (
		<Fragment>
			{renderCheckboxes(props, settings, changeSetting, user)}
		</Fragment>
	);
}

const settingsArray = [
	{
		title: 'Email',
		value: 'email'
	},
	{
		title: 'Currikula',
		value: 'currikula'
	}
];

function renderCheckboxes(props, settings, changeSetting, user) {
	return settingsArray.map(setting => (
		<Row key={setting.title}>
			<Checkbox
				checked={props.options[setting.value]}
				onChange={change => onChange(change, settings, changeSetting, user)}
				value={setting.value}
			>
				{setting.title}
			</Checkbox>
		</Row>
	));
}
















