import React, { Component } from 'react';

import { Form, Input, Button } from 'antd';

class ChangePassword extends Component {
	state = {
		confirmDirty: false
	};

	handleSubmit = e => {
		e.preventDefault();
		this.props.form.validateFieldsAndScroll((err, values) => {
			if (!err) {
				console.log('Received values of form: ', values);
			}
		});
	};

	handleConfirmBlur = e => {
		const value = e.target.value;
		this.setState({ confirmDirty: this.state.confirmDirty || !!value });
	};

	compareToFirstDetails = (rule, value, callback) => {
		const form = this.props.form;
		if (value && value !== form.getFieldValue('password')) {
			callback('This does not match your first password entry!');
		} else {
			callback();
		}
	};

	validateToNextPassword = (rule, value, callback) => {
		const form = this.props.form;
		if (value && this.state.confirmDirty) {
			form.validateFields(['confirmPassword'], { force: true });
		}
		callback();
	};

	// componentDidUpdate() {
	// 	// console.log('5details: ', this.props);
	// 	if (this.props.details && !this.state.password) {
	// 		this.setState({
	// 			password: this.props.details.password
	// 		});
	// 		// console.log('details: ', this.props.details);
	// 		this.props.form.setFieldsValue({
	// 			password: this.props.details.password
	// 		});
	// 	}
	// }

	render() {
		// const { details } = this.props;

		const { getFieldDecorator } = this.props.form;

		const formItemLayout = {
			labelCol: {
				xs: { span: 24 },
				sm: { span: 20 }
			},
			wrapperCol: {
				xs: { span: 24 },
				sm: { span: 20 }
			}
		};

		return (
			<Form onSubmit={this.handleSubmit}>
				{/* <====================== initialization ==========================> */}

				<Form.Item {...formItemLayout}>
					{getFieldDecorator('password', {
						rules: [
							{
								required: true,
								message: 'Please input your password'
							},
							{
								validator: this.validateToNextPassword
							}
						]
					})(<Input 
						type="password" 
						placeholder="New password" />)}
				</Form.Item>

				{/* <============================ confirmation ====================================> */}
				
				<Form.Item {...formItemLayout}>
					{getFieldDecorator('confirmPassword', {
						rules: [
							{
								required: true,
								message: 'Please confirm your password!'
							},
							{
								validator: this.compareToFirstDetails
							}
						]
					})(<Input 
						placeholder="Confirm new password" 
						type="password" 
						onBlur={this.handleConfirmBlur} 
						/>)}
				</Form.Item>

				<Form.Item wrapperCol={{ span: 6 }}>
					<Button type="primary" htmlType="submit">
					Save Password
					</Button>
				</Form.Item>
			</Form>
		);
	}
}

// <=================== allows for the creation the form =======================>

const WrappedChangePassword = Form.create({ name: 'Change_details' })(
	ChangePassword
);

export default WrappedChangePassword;