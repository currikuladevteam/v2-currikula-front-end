import React, { Component } from 'react'

import { Select, notification } from 'antd';
import Log from '../../../functions/api/logs/Log';
import { UserContext } from '../../providers/UserProvider';
import { unSnakeCase, upperCase } from '../../../functions/helpers/TextHelpers';

const Option = Select.Option;

// function handleChange(value) {
//   console.log(`selected ${value}`);

// }

// function handleBlur() {
//   console.log('blur');
// }

// function handleFocus() {
//   console.log('focus');
// }
class SelectDefaultReferences extends Component {
  constructor(props) {
    super(props)
    this.state = {
      options: this.props.options,
      value: this.props.value
    }
  }

  componentDidMount(){

    if (this.state.value !== this.props.value) {
      this.setState({
        value: this.props.value 
      })
    }
  }

  updateSetting(value){

    const { title } = this.props
    // Send to API with new setting value 
    // Change the local cache
    // Update the context API



    const event = {
      type: 'setting',
      message: <p>You changed your default {title} to: <b>{value.length === 3 ? upperCase(unSnakeCase(value)): unSnakeCase(value)}</b></p>,
      document: {
        type: title
      },
      oldInformation: this.state.value,
      newInformation: value
    }

    const log = new Log({ event, type: "metadata", user: this.context.user}).log();
    // Fire off the notification (passing in the notification object)
    // This is required to give a location to the notification
    log.notify(notification)();

  }

  handleChange = (value) => {
    this.setState({
      value: value
    })
    this.updateSetting(value)
  }
  render() {
    const { placeholder } = this.props;
      return (
        <Select
          // showSearch
          style={{ width: 300 }}
          // placeholder={placeholder}
          value={this.state.value || placeholder}
        // optionFilterProp="children"
          onChange={(value) => this.handleChange(value)}
        // onFocus={handleFocus}
        // onBlur={handleBlur}
        // filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
        >
          {this.state.options.map(item =>
            <Option key={item} value={item.toLowerCase()}>{unSnakeCase(item)}</Option>
          )}
        </Select>
      )
  }
}


SelectDefaultReferences.contextType = UserContext;

export default SelectDefaultReferences;