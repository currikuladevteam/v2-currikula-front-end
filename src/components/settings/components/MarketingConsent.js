import React, { useContext } from 'react';
import { Checkbox, notification } from 'antd';
import { unSnakeCase } from '../../../functions/helpers/TextHelpers';
import Log from '../../../functions/api/logs/Log';
import { UserContext } from '../../providers/UserProvider';


function onChange(change, context, changeSetting, user) {
	const value = change.target.value
	console.log('Value: ', value)
	const checked = change.target.checked

	console.log('Checked: ', checked)

	const choices = context.marketing.choices
	console.log('Marketing: ', choices)

	let changed = {}
	changed[value] = checked;
	console.log('Changed: ', changed)

	const changeContext = {
		...context,
		marketing: {
			choices,
			...changed
		}
	}

	// Remove the focus on the input after deselection
	change.nativeEvent.srcElement.blur()

	// Changes the context state
	// Will need to also send the change to the back end
	changeSetting(changeContext)
	const event = {
		type: 'setting',
		message: <p>You changed your marketing consent {unSnakeCase(value)} to: <b>{checked ? "consenting" : "not consenting"}</b></p>,
		document: {
			type: unSnakeCase(value)
		},
		oldInformation: !checked,
		newInformation: checked
	}
	const log = new Log({ event, type: "metadata", user }).log();
	// Fire off the notification (passing in the notification object)
	// This is required to give a location to the notification
	log.notify(notification)();
}

export default function MarketingConsent(props) {
	const { user, changeSetting } = useContext(UserContext)
	const settings = user ? user.settings : null
	console.log(user)
	return (
		<React.Fragment>
			<Checkbox
				value={'consent'}
				checked={props['consent']}
				onChange={(change) => onChange(change, settings, changeSetting, user)}
			>
				I consent to receive the occasional marketing email from Currikula
			</Checkbox>
		</React.Fragment>
	);
}

