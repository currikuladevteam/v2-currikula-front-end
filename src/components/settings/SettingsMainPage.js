import React, { Component } from 'react';
import { BackTop, Row, Col, Button, Icon } from 'antd';
import WrappedChangeEmail from './components/ChangeEmail';
import WrappedChangePassword from './components/ChangePassword';
import SelectSettings from './components/SelectSettings';
import HowNotifications from './components/HowNotifications';
import MarketingConsent from './components/MarketingConsent';
import WhatNotifications from './components/WhatNotifications';
import FineTunePrefs from './components/FineTunePrefs';
import BillingCard from './components/BillingCard';
import BillingHistory from './components/BillingHistory';
import { UserContext } from '../providers/UserProvider';
import SettingContainer from './components/SettingContainer';
import HeaderWithSubHeader from '../general/HeaderWithSubHeader';
import { showConfirm } from './components/ConfirmPopUp'

const selectTitle = {
	margin: '10px 0'
};
class SettingsMainPage extends Component {
	constructor() {
		super();
		this.state = {
			email: ''
		};
	}

	render() {
		const { user } = this.context;


		return (
		<div id="SettingsMainPage">
			<BackTop target={() => document.querySelector('#SettingsMainPage')} />
			
			{/* <============================== Accounts =====================================> */}

			<SettingContainer containerId="Account">
				<HeaderWithSubHeader 
					header="Account"
					subHeader="Here you can change the details associated with your account."
				/>
				<Row>
					<Col span={12}>
						<h4>Change Email
							<span style={{float: 'right', marginRight: 10, minWidth: 200}}>
								<Icon  type="mail" style={{marginRight: 5}}/>
								{ this.context.user ? this.context.user.email : 'Fetching...' }
							</span></h4>
						<WrappedChangeEmail
							user={this.context ? this.context.user : null}
						/>
					</Col>
					<Col span={12}>
						<h4>Change Password</h4>
						<WrappedChangePassword
							details={this.context ? this.context : null}
						/>
					</Col>
				</Row>
			</SettingContainer>

			{/* <============================ DEFAULTS =========================================> */}

			<SettingContainer containerId="Defaults">
				<HeaderWithSubHeader 
					header="Defaults"
					subHeader="Set the default of your account, like templates and referencing styles."
				/>
					<Row>
						<Col span={12} >
							<h4 style={selectTitle}>Referencing Style</h4>
							<SelectSettings
								title="Referencing Style"
								value={user ? user.settings.defaults.referencing_style : null}
								options={['Harvard', 'MLA', 'APA']}
								placeholder="Select a default referencing style"
							/>
							<h4 style={selectTitle}>Note Taking Style</h4>
							<SelectSettings
								title="Note Taking Style"
								value={user ? user.settings.defaults.note_style : null}
								options={['sentence_method', 'outline_method', 'cornell_method', 'freeform']}
								placeholder="Select a default new note style"
							/>
						</Col>

						<Col span={12}>
							<h4 style={selectTitle}>Font Style</h4>
							<SelectSettings
								title="Font Style"
								value={user ? user.settings.defaults.font_style : null}
								options={['times_new_roman', 'arial', 'courier', 'open_sans']}
								placeholder="Select a default font style"
							/>
							<h4 style={selectTitle}>Line Spacing</h4>
							<SelectSettings
								title="Line Spacing"
								value={user ? user.settings.defaults.line_style : null}
								options={['1', '1.5', '2', '2.5', '3']}
								placeholder="Select a default line spacing height"
							/>
							<h4 style={selectTitle}>Text Size</h4>
							<SelectSettings
								title="Text Size"
								value={user ? user.settings.defaults.text_style : null}
								options={['tiny', 'small', 'normal', 'large', 'extra_large']}
								placeholder="Select a default text size"
							/>
						</Col>
					</Row>

			</SettingContainer>

			{/* <=================================== NOTIFICATIONS ==============================> */}

			<SettingContainer containerId="Notifications">
				<HeaderWithSubHeader 
					header="Notifications"
					subHeader="Change how you receive notifications and how often you are reminded about project deadlines and progress."
				/>
					<Row> 
						<Col span={12}>
							<h4 style={selectTitle}>How to receive notifications</h4>
							<HowNotifications options={user ? user.settings.notifications.receive_via : {}}/>
						</Col>
						<Col span={12}>
							<h4 style={selectTitle}>What to receive from notifications</h4>
							<WhatNotifications options={user ? user.settings.notifications.choices : {}} />
						</Col>
					</Row>
			</SettingContainer>

			{/* <=================================== MARKETING ==================================> */}

			<SettingContainer containerId="Marketing">
				<HeaderWithSubHeader 
					header="Marketing"
					subHeader="Change your marketing preferences and control when you receive emails from us about new features, deals and exciting things going on in the education world."
				/>
					<Row >
						<MarketingConsent consent={user ? user.settings.marketing.consent : false}/>
						<h4 style={selectTitle}>Fine tune your preferences</h4>
						<FineTunePrefs options={user ? user.settings.marketing.choices : {}} style={{marginLeft: 10}}/>
					</Row>
			</SettingContainer>

			{/* <=================================== BILLING ===================================> */}

			<SettingContainer containerId="Billing">
				<HeaderWithSubHeader 
					header="Billing"
					subHeader="Change your billing information and update your subscription
					settings."
				/>
				<Row >
					<Col span={12}>
						<h4>Current Billing Information</h4>
						<BillingCard style={{marginTop: 20}} />
					</Col>
					<Col span={12}>
						<h4>Billing History</h4>
						<BillingHistory style={{maxWidth: 400, margin: '0 auto'}} />
					</Col>
				</Row>
				
			</SettingContainer>

			{/* <=================================== Subscription  =====================================> */}

			<SettingContainer containerId="Subscription">
				<HeaderWithSubHeader 
					header="Subscription"
					subHeader="Here you can cancel your subscription."
				/>
					<h3>Cancel Subscription</h3>
						<div style={{ marginLeft: 10 }}>
							<p>
								As Currikula is a paid service, cancelling your subscription will result in the suspension of you account. You will still be able to download your projects, notes and bibliographies, but you will not be able to edit them or create new ones.
							</p>
							<Button type="danger" onClick={() => showConfirm(user)} >Cancel Subscription</Button>

							{console.log(user)}
						</div>
			</SettingContainer>

			{/* <=================================== DANGER ZONE ===================================> */}

			<SettingContainer containerId="Danger_Zone">
				<HeaderWithSubHeader 
					header="Danger Zone"
					subHeader="This is where you can delete your account. Deleting your account will cancel your subscription and begin the process of wiping your data from our servers. This process is not  immediate and can take up to 1 month to accomplish. However, once you have deleted your account you will not be able to recover it. If you wanted to continue to use Currikula’s services you would have to create a new account."
				/>
				<div style={{ padding: '0 20px' }} >
					<Button type="danger">Delete Account</Button>
				</div>
			</SettingContainer>
		</div>
		);
	}
}

SettingsMainPage.contextType = UserContext;

export default SettingsMainPage;
