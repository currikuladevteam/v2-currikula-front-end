import React, { Component } from 'react';
import { Anchor } from 'antd';
import { withRouter } from 'react-router-dom';

const { Link } = Anchor;



class SettingsSidebar extends Component {
  render() {
    return (
      <div id="SettingsSideBarJS">
        <Anchor
          getContainer={() => document.querySelector('#SettingsMainPage')} 
          showInkInFixed={false}
          affix={false}
          style={{
            height: '100vh',
            display: 'flex',
            flexDirection: 'column',
            marginBottom: 'auto',
            borderRight: '2px solid lightgrey',
            borderLeft: 'none',
            fontSize: '30px',
            lineHeight: '2rem'
          }}>

          <div style={{ textAlign: 'center', marginTop: '10px' }}>
            <h2 style={{ letterSpacing: '0.3rem'  }}>SETTINGS</h2>
          </div>
          <Link href="#Account" title="ACCOUNT" />
          <Link href="#Defaults" title="DEFAULTS" />
          <Link href="#Notifications" title="NOTIFICATIONS" />
          <Link href="#Marketing" title="MARKETING" />
          <Link href="#Billing" title="BILLING" />
          <Link href="#Subscription" title="SUBSCRIPTION" />
          <Link href="#Danger_Zone" title="DANGER ZONE" />
        </Anchor>
      </div>
    );
  }

}

export default withRouter(SettingsSidebar);
