import React, { Component } from 'react';
import { Row, Col, Button } from 'antd';
import { Link } from 'react-router-dom';

class PreviewHeader extends Component {
  close = () => {
    this.props.closeDrawer();
  }

  open = () => {
    this.props.openDocument();
  }

  render() {
    return (
      <Row>
        <Col span={4} >
          <Link to={`/writer/${this.props.docId}/${this.props.type}`}> <Button style={{marginRight: 15}}> Open </Button></Link>
        </Col>
        <Col span={18} style={{textAlign: 'center', paddingRight: 40}} >
          <h2 style={{marginBottom: 0, display: 'inline'}}>{this.props.title}</h2>
        </Col>
        <Col style={{textAlign: 'right'}} span={2}>
          <Button onClick={this.close} icon="cross" />
        </Col>
      </Row>
    );
  }
}


export default PreviewHeader;