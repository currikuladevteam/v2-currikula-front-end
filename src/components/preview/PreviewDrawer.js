import React, { Component } from 'react';
import { Drawer, Divider } from 'antd';
import PreviewHeader from '../preview/PreviewHeader';
import QuickPreviewEditor from '../writer/editor/QuickPreviewEditor';
import { Value } from 'slate';

class PreviewDraw extends Component {
  constructor(props) {
    super(props)
    this.state = {
      visible: false,
      document: {}
    };
  }

  showDrawer = (documentToPreview) => {
    console.log(documentToPreview)
    this.setState({
      visible: true,
      value: this.getReadableValue(documentToPreview),
      document: documentToPreview.document,
      id: documentToPreview.id
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
      document: {},
      value: {},
      id: ''
    });
  };

  getReadableValue(file) {
    console.log(file)
    if (file.document) {
      return Value.fromJSON(file.document.value)
    } else {
      return null
    }
  }

  render() {

    const { type } = this.state.document;
    return (
      <div>
        <Drawer
          width={640}
          placement="right"
          closable={false}
          onClose={this.onClose}
          visible={this.state.visible}
          destroyOnClose={true}
        >
          <PreviewHeader 
            title={type ? type.toUpperCase() : ''} 
            closeDrawer={this.onClose} 
            docId={this.state.id}
            type={type}
            />
          <Divider style={{margin: '15px 0 0 0'}} />
          <div
          style={{height: `calc(100vh - 100px)`,  overflow: 'scroll'}}
          >
            <QuickPreviewEditor editorValue={this.state.value} />

          </div>
        </Drawer>
      </div>
    );
  }
}


export default PreviewDraw;