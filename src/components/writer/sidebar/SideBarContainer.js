import React, { Component } from 'react'
import { Tabs, Layout, Icon, Collapse } from 'antd';
import ResourcesPane from './ResourcesPane';
import ImprovePane from './ImprovePane';
import EditPane from './EditPane';
import PlagiarismPane from './PlagiarismPane';
import ExportPane from './ExportPane';

const { Sider } = Layout;
const TabPane = Tabs.TabPane;
const { Panel } = Collapse;

class SideBarContainer extends Component {
    constructor() {
        super()
        this.state = {
            collapsed: false,
            sidebarCss: {
                right: 0,
            },
            exportExpanded: false,
            containerHeight: 200
        };
    }

    componentDidMount() {
        this.setState({
            containerHeight: 400
        })
    }
    toggleOpen = () => {
        if (this.state.exportExpanded) {
            this.setState({
                exportExpanded: false,
                displace: '180'
            });
        } else {
            this.setState({
                exportExpanded: true,
                displace: '380'
            });
        }
    };


    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    }

    adjustCss = (broken) => {
        const state = this.state;
        if (broken) {
            this.setState({
                ...state,
                sidebarCss: {
                    position: 'absolute',
                    right: 0,
                }
            })
        } else {
            this.setState({
                ...state,
                sidebarCss: {
                    position: 'relative',
                    right: 0,
                }
            })
        }
    }
    render() {
        const collapserIconStyle = {
            fontSize: 30,
            position: 'absolute',
            color: '#2C3A47',
            top: 'calc(50vh - 20px)',
            left: this.state.collapsed ? '-40px' : '-37px',
            padding: this.state.collapsed ? '12px 10px 12px 0px' : '12px',
            zIndex: 1,
            backgroundColor: '#ffff00',
            borderRadius: this.state.collapsed ? '0px 40px 40px 0px' : '40px',
            transform: this.state.collapsed ? 'rotate(180deg)' : 'rotate(0deg)',
            transition: '0.3s',
            overflow: 'hidden'
        }
        return (
            <Sider
                theme="light"
                style={this.state.sidebarCss}
                width={400}
                trigger={null}
                collapsible
                collapsed={this.state.collapsed}
                collapsedWidth={0}
                // onBreakpoint={(broken) => { this.adjustCss(broken) }}
                breakpoint={'xl'}
            >
                <div style={{ height: "100%", display: 'flex', flexDirection: 'column', alignItems: 'bottom' }}>
                    <Icon
                        style={collapserIconStyle}
                        type={'right'}
                        onClick={this.toggle}
                    />
                    <Tabs tabBarGutter={3} tabBarStyle={{ width: '400px', marginBottom: 8 }} defaultActiveKey="4">
                        <TabPane tab="EDIT" key="1">
                            <EditPane {...this.props} />
                        </TabPane>
                        <TabPane tab="IMPROVE" key="2" style={{ display: 'flex', flexDirection: 'column' }}>
                            <ImprovePane essay={this.props.essay} />
                        </TabPane>
                        <TabPane tab="PLAGIARISM" key="3">
                            <PlagiarismPane />
                        </TabPane>
                        <TabPane tab="RESOURCES" key="4">
                            <ResourcesPane
                                project={this.props.project}
                                essay={this.props.essay}
                            />
                        </TabPane>
                    </Tabs>
                    <Collapse
                        onChange={this.toggleOpen}
                        defaultActiveKey={[]}
                        style={{
                            border: 0,
                            bottom: 0,
                            position: !this.state.collapsed ? 'absolute' : '',
                            width: '100%',
                            color: 'white',
                            zIndex: 2, borderRadius: 0
                        }} >
                        <Panel
                            id="exportPanel"
                            style={{ background: 'green', border: 0, borderRadius: 0, color: 'white' }}
                            header={
                                <h4 style={{ textAlign: 'center', marginBottom: 0, paddingRight: 35, color: 'white' }}> EXPORT </h4>
                            }
                            key="1"
                        >
                            <ExportPane height={this.state.containerHeight} />
                        </Panel>
                    </Collapse>
                </div>
            </Sider >
        )
    }
}

export default SideBarContainer
