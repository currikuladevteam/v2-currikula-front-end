import React, { Component } from "react";
import { Button } from "antd";
import PreviewEditor from '../editor/PreviewEditor';

class ResourcePreviewPane extends Component {

  constructor(props) {
    super(props)
    this.state = {
      document: ''
    }
  }
  render() {
    return (
      <div style={{ padding: "10px 10px 10px 20px", display: this.props.style.display, overflow: 'scroll' }}>
        <div>
          <Button icon="left" size="small" onClick={() => {this.props.back()}}> BACK </Button>
          <h2
            style={{ textAlign: "center", display: "inline", marginLeft: 20 }}
          >
            Document Preview
          </h2>
        
        </div>
        <div style={{ overflow: 'scroll', paddingLeft: 8 }}>
        {this.props.document ? <PreviewEditor
          editorValue={this.props.document} 
        /> : <div>Loading...</div>}
        
        </div>

      </div>
    );
  }
}

export default ResourcePreviewPane;
