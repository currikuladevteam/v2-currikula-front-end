import React, { Component } from 'react';
import StatCircle from '../subComponents/StatCircle';
import { Collapse, Popover, Progress } from 'antd';

const { Panel } = Collapse;



// Css Vars
const customPanelStyle = {
  borderRadius: 4,
  border: 0,
  overflow: 'hidden',

};

const Tooltips = {
  progress: (
    <div style={{ width: 200 }}>
      <p>
        This is your overall progress in your project. It includes factors such as a word count, improvement checks, plagiarism checks, referencing finalisation,  bibliography completion and exportation of the project.
      </p>
    </div>
  ),
  due: (
    <div style={{ width: 200 }}>
      <p>
        This is how far out your are from the due date of your assignment.
      </p>
    </div>
  ),
  wordCount: (
    <div style={{ width: 200 }}>
      <p>
        This is your current word count versus your target word count for the piece. Typically universities allow you a 10% margin on your work with regards to this limit. Make sure to check you have such a margin!
      </p>
    </div>
  ),
  references: (
    <div style={{ width: 200 }}>
      <p>
        This is the amount of references that we have found in your work, it does not include the references you have in your bibliography.
      </p>
    </div>
  ),
  quotes: (
    <div style={{ width: 200 }}>
      <p>
        This is the amount of quotes we have identified in your draft so far.
      </p>
    </div>
  ),
  paragraphs: (
    <div style={{ width: 200 }}>
      <p>
        This is the total number of paragraphs that make up your draft.
      </p>
    </div>
  )
};




class QuickAddInlineRefCollapse extends Component {
  render() {
    // console.log(this.props)
    const { project } = this.props;
    let wordCountTarget = 0;
    let currentWordCount = 0;
    if (project ) {
      wordCountTarget = project.progress.words.target
      currentWordCount = this.props.wordCount
    }
    return (
      <Collapse
      onChange={this.props.toggleOpen}
      defaultActiveKey={[]}
      style={{ border: 0 }}
    >
      <Panel
        style={customPanelStyle}
        header={
          <h4
            style={{ textAlign: 'center', marginBottom: 0, paddingRight: 40 }}
          >
            STATISTICS
          </h4>
        }
        key="1"
      >
        <div style={{ display: 'flex', justifyContent: 'space-around' }}>
          <Popover
            placement="topLeft"
            content={Tooltips.progress}
            title="Progress"
          >
            <div style={{ textAlign: 'center', width: 100 }}>
              <Progress
                type="circle" percent={project ? project.progress.project : 0} width={52} style={{ marginBottom: 6 }}
              />
              <h4>PROGRESS</h4>
            </div>
          </Popover>
          <Popover
            content={Tooltips.due}
            title="Due Date">
            <div style={{ textAlign: 'center', width: 100 }}>
              <Progress
                type="circle" percent={80} width={52} style={{ marginBottom: 6 }}
              />
              <h4>1ST SEP</h4>
            </div>
          </Popover>
          <Popover
            placement="topRight"
            content={Tooltips.wordCount}
            title="Word Count"
          >
            <div style={{ textAlign: 'center', width: 100 }}>
              <Progress
                type="circle" percent={Math.ceil((currentWordCount/wordCountTarget)*100)} width={52} style={{ marginBottom: 6 }}
              />
              <h4>{currentWordCount}/{wordCountTarget}</h4>
            </div>
          </Popover>
        </div>
        <div style={{ display: 'flex', justifyContent: 'space-around' }}>
          <Popover
            placement="topLeft"
            content={Tooltips.references}
            title="References"
          >
            <div>
              <StatCircle
                stat={14}
                color=""
                border="#2C3A47"
                header="REFERENCES"
              />
            </div>
          </Popover>

          <Popover content={Tooltips.quotes} title="Quotes">
            <div>
              <StatCircle
                stat={14}
                color=""
                border="#2C3A47"
                header="QUOTES"
              />
            </div>
          </Popover>

          <Popover
            placement="topRight"
            content={Tooltips.paragraphs}
            title="Paragraphs"
          >
            <div>
              <StatCircle
                stat={14}
                color=""
                border="#2C3A47"
                header="PARAGRAPHS"
              />
            </div>
          </Popover>
        </div>
      </Panel>
    </Collapse>
    );
  }
}

export default QuickAddInlineRefCollapse;
