import React, { Component } from 'react';
import StatCircle from '../subComponents/StatCircle';
import { Collapse, Popover } from 'antd';

const { Panel } = Collapse;



// Css Vars
const customPanelStyle = {
  borderRadius: 4,
  border: 0,
  overflow: 'hidden',

};


// Tool Tips for the Expanding Panel (Plagiarism Overview)
const tooltips = {
  originality: (
    <div style={{ width: 200 }}>
      <p>
        Your originality score is an approximation of the amount of original work contained in your essay. Our algorithm uses propriety techniques to determine to the best of its ability the sources and types of content contained in your work.
      </p>
      <p>
        However, it is not perfect and should be used as a guideline. Currikula helps you minimise likelihood accidental plagiarism by providing you the tools to reference quickly, accurately and effectively.
      </p>
    </div>
  ),
  authorship: (
    <div style={{ width: 200 }}>
      <p>
        Aut horship analysis looks at your writing for inconsistencies. Paragraphs or chunks of text that seem to stick out, have profile that might not be your own.
      </p>
      <p>
        It is an experimental technology and should be used as rough guidance only.
        </p>
    </div>
  ),
  web: (
    <div style={{ width: 200 }}>
      <p>
        Currikula searches billions of webpages to look for matches across your essay to determine likelihood of plagiarism.
      </p>
      <p>
        Check the sources we have found to determine whether you have accidentally forgotten to cite something or you didn't realise you haven't cited something.
      </p>
    </div>
  ),
  academic: (
    <div style={{ width: 200 }}>
      <p>
        Similar to the Web check our Academic sources scans academic paper databases to determine matches.
      </p>
      <p>
        We are always trying to increase the scope of our academic database scanning and therefore the accuracy and reach of this feature will increase over time.
        </p>
    </div>
  )
};


class PlagiarismCollapse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displace: '185',
      collapseOpen: false,
      active: 'all',
      activeIcon: true
    };
  }

  render() {
    return (
        <Collapse
          onChange={this.props.toggleOpen}
          defaultActiveKey={[]}
          style={{ border: 0 }}
        >
          <Panel
            style={customPanelStyle}
            header={
              <h4 style={{ textAlign: 'center', marginBottom: 0, paddingRight: 30 }}>
                PLAGIARISM OVERVIEW
                </h4>}
            key="1">
            <div style={{ display: 'flex', justifyContent: 'space-around', padding: '10px 10px 0 15px' }}>
              <div>
                <div style={{ fontSize: 14 }}>Originality Score</div>
                <p style={{ fontSize: 10, textAlign: 'justify' }}>Your Plagiarism score is calculated by multiple different factors. However, it does not guarantee that you are protected from accidental plagiarism.</p>
              </div>
              <Popover
                placement="topRight"
                content={tooltips.originality}
                title="Originality Score"
              >
                <div style={{ marginTop: 4 }}>
                  <StatCircle
                    stat={78}
                    color="#2C3A47"
                    border="lightgreen"
                  />
                </div>
              </Popover>
            </div>
            <div style={{ display: 'flex', justifyContent: 'space-around' }}>
              <Popover
                placement="topLeft"
                content={tooltips.authorship}
                title="Authorship Checks"
              >
                <div>
                  <StatCircle
                    stat={14}
                    color="#2C3A47"
                    border="#FFF8AB"
                    header="AUTHORSHIP"
                  />
                </div>
              </Popover>

              <Popover
                content={tooltips.web}
                title="Web Sources">
                <div>
                  <StatCircle
                    stat={14}
                    color="#2C3A47"
                    border="#FFF8AB"
                    header="WEB"
                  />
                </div>
              </Popover>

              <Popover
                placement="topRight"
                content={tooltips.academic}
                title="Academic Sources"
              >
                <div>
                  <StatCircle
                    stat={14}
                    color="#2C3A47"
                    border="#FFF8AB"
                    header="ACADEMIC"
                  />
                </div>
              </Popover>
            </div>
          </Panel>
        </Collapse>
    );
  }
}

export default PlagiarismCollapse;
