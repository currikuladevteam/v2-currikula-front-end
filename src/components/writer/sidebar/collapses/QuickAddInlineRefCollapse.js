import React, { Component } from 'react';
import { Collapse, List, Skeleton, Card, Spin } from 'antd';
import { getReferences } from '../../../../demo-data/fake-apis/references.api'

const { Panel } = Collapse;



// Css Vars
const customPanelStyle = {
  borderRadius: 4,
  border: 0,
  overflow: 'hidden',

};




class QuickAddInlineRefCollapse extends Component {
  constructor(props) {
    super(props) 
    this.state= {
      references: null
    }
  }

componentWillReceiveProps() {
    if (this.props.bibliography) {
      getReferences(this.props.bibliography.document.reference_ids).then(references => {
        const stateReferences = JSON.stringify(this.state.references);
        const apiReferences = JSON.stringify(references);
        if (stateReferences !== apiReferences) {
          this.setState({
            references: references
          })
        }
      })
    }
}
  // componentDidMount(){
  //   console.log('Mounting', this.props)
  //   if (this.props.bibliography) {
  //     getReferences(this.props.bibliography.document.reference_ids).then(references => {
  //       const stateReferences = JSON.stringify(this.state.references);
  //       const apiReferences = JSON.stringify(references);
  //       if (stateReferences !== apiReferences) {
  //         this.setState({
  //           references: references
  //         })
  //       }
  //     })
  //   }
  // }

  createFullTextRefDemo(ref) {
    return `${checkNull(ref.author[0].initials)} ${checkNull(ref.author[0].last)} (${checkNull(ref.publication_date.year)}) ${checkNull(ref.title)}`
  }

  render() {
    return (
      <Collapse
      onChange={this.props.toggleOpen}
      defaultActiveKey={[]}
      style={{ border: 0, zIndex: 10, backgroundColor: '#f5f5f5'}}
    >
      <Panel
      className=""
      id="child-container-padding-remove"

        style={customPanelStyle}
        header={
          <h4
            style={{ textAlign: 'center', marginBottom: 0, paddingRight: 30}} >
            QUICK ADD INLINE REFERENCE
        </h4>
        }
        key="1"
      >
        {this.state.references ?
        <List
        style={{height: 160, overflow: 'scroll'}}
        loading={this.state.references === []}
        itemLayout="horizontal"
        dataSource={this.state.references}
        renderItem={ref => (
        <List.Item
          style={{ margin: '10px 15px', padding: '0px 5px', borderRadius: 5 }}
        >
          <Skeleton
            title={false} 
            loading={!!this.props.project} 
            active>
            <Card hoverable bodyStyle={{padding: 10}}> {this.createFullTextRefDemo(ref)}</Card>
          </Skeleton>
        </List.Item>
      )}
    /> : this.props.project ? <Spin tip="Loading"/> : <div style={{height: 160, textAlign: "center", padding: 10}}>No Project Attached</div> }
        
      </Panel>
    </Collapse>
    );
  }
}

export default QuickAddInlineRefCollapse;


function checkNull(input) {
  return input ? input : ''
}