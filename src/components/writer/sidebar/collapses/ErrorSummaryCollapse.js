import React, { Component } from 'react';
import StatCircle from '../subComponents/StatCircle';
import { Collapse, Popover } from 'antd';

const { Panel } = Collapse;



// Css Vars
const customPanelStyle = {
  borderRadius: 4,
  border: 0,
  overflow: 'hidden',

};


// Tool Tips for the Expanding Panel (Plagiarism Overview)
const Tooltips = {
  grammar: (
    <div style={{ width: 200 }}>
      <p>
        Grammar mistakes are bad and can cause significant damage to your
        overall grade.{' '}
      </p>
      <p>
        We highly recommend looking at these closely to determine to the best of
        your ability if what you have written is correct.
      </p>
    </div>
  ),
  spelling: (
    <div style={{ width: 200 }}>
      <p>
        Spelling mistakes are look careless, and whoever is reading your essay
        will think you have put less effort and time into your work.
      </p>
      <p>We highly recommend you make your best effort to resolve these.</p>
    </div>
  ),
  referencing: (
    <div style={{ width: 200 }}>
      <p>
        Referencing errors can be lead you to being accused of plagiarism as
        your work is incorrectly cited.
      </p>
      <p>
        It is best practice to double check all references before submitting
        your work.
      </p>
    </div>
  ),
  content: (
    <div style={{ width: 200 }}>
      <p>
        Content errors are to do with your word choice, the use of terms and
        other word based advice.
      </p>
      <p>We suggest using these to help you write better essays.</p>
    </div>
  ),
  structure: (
    <div style={{ width: 200 }}>
      <p>
        Structure errors are things like overly long paragraphs, sentences,
        incorrect formatting and general layout.
      </p>
      <p>We suggest using these to help you write better essays.</p>
    </div>
  ),
  argument: (
    <div style={{ width: 200 }}>
      <p>
        Argument errors are where we highlight possible areas that your argument
        might be weak or not follow.
      </p>
      <p>
        We suggest taking a look at these to help identify areas to double check
        for errors.
      </p>
    </div>
  )
};



class PlagiarismCollapse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displace: '185',
      collapseOpen: false,
      active: 'all',
      activeIcon: true
    };
  }

  render() {
    return (
      <Collapse
          onChange={this.props.toggleOpen}
          defaultActiveKey={[]}
          style={{ border: 0 }}
        >
          <Panel
            style={customPanelStyle}
            header={
              <h4
                style={{ textAlign: 'center', marginBottom: 0, paddingRight: 30}}
              >
                ERROR SUMMARY
              </h4>
            }
            key="1"
          >
            <div style={{ display: 'flex', justifyContent: 'space-around' }}>
              <Popover
                placement="topLeft"
                content={Tooltips.grammar}
                title="Grammar Errors"
              >
                <div>
                  <StatCircle
                    stat={14}
                    color="#2C3A47"
                    border="#FFBEC6"
                    header="GRAMMAR"
                  />
                </div>
              </Popover>
              <Popover content={Tooltips.spelling} title="Spelling Errors">
                <div>
                  <StatCircle
                    stat={14}
                    color="#2C3A47"
                    border="#FFBEC6"
                    header="SPELLING"
                  />
                </div>
              </Popover>
              <Popover
                placement="topRight"
                content={Tooltips.referencing}
                title="Referencing Errors"
              >
                <div>
                  <StatCircle
                    stat={14}
                    color="#2C3A47"
                    border="#FFBEC6"
                    header="REFERENCING"
                  />
                </div>
              </Popover>
            </div>
            <div style={{ display: 'flex', justifyContent: 'space-around' }}>
              <Popover
                placement="topLeft"
                content={Tooltips.content}
                title="Content Errors"
              >
                <div>
                  <StatCircle
                    stat={14}
                    color="#2C3A47"
                    border="#FFF8AB"
                    header="CONTENT"
                  />
                </div>
              </Popover>

              <Popover content={Tooltips.structure} title="Structure Errors">
                <div>
                  <StatCircle
                    stat={14}
                    color="#2C3A47"
                    border="#FFF8AB"
                    header="STRUCTURE"
                  />
                </div>
              </Popover>

              <Popover
                placement="topRight"
                content={Tooltips.argument}
                title="Argument Errors"
              >
                <div>
                  <StatCircle
                    stat={14}
                    color="#2C3A47"
                    border="#FFF8AB"
                    header="ARGUMENT"
                  />
                </div>
              </Popover>
            </div>
          </Panel>
        </Collapse>
    );
  }
}

export default PlagiarismCollapse;
