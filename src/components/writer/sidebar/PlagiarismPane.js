import React, { Component } from 'react';
import { List } from 'antd';
import PlagiarismCollapse from './collapses/PlagiarismCollapse';
import PaneResultMenu from './subComponents/PaneResultMenu';
import PlagiarismCard from '../../cards/PlagiarismCard';
// Example Feedback Object
const plagiarismFeedback = [
  {
    type: 'academic',
    flaggedText:
      'This is where the flagged text would go and it would make up the string',
    sourceText:
      'This is where the source text would go, with the appropiate parts highlighted',
    compiledReference:
      'Smith, Jones. (2012) The Oxford Examination Dictionary. Cambridge University Press. pp.1', // in the final version this will be an object
    flaggedLocation: [], // for making it work
    ignored: false,
    timeAccepted: new Date(),
    documentVersion: 335
  },
  {
    type: 'web',
    flaggedText:
      'This is where the flagged text would go and it would make up the string',
    sourceText:
      'This is where the source text would go, with the appropriate parts highlighted',
    compiledReference:
      'Irons, Jemma. (2011) www.wikipedia.com/article/that/is/relevant. Date accessed: 12th June 2019', // in the final version this will be an object
    flaggedLocation: [], // for making it work
    ignored: false,
    timeAccepted: new Date(),
    documentVersion: 335
  },
  {
    type: 'authorship',
    flaggedText:
      'Aute dolor nulla nisi irure veniam reprehenderit cupidatat magna ullamco nulla nulla incididunt deserunt. Culpa magna deserunt occaecat do exercitation non eiusmod adipisicing proident. Mollit tempor ut Lorem laboris ea consequat. Lorem ut deserunt non labore adipisicing exercitation velit do et quis quis. Ad tempor proident fugiat sint consectetur aliquip. Voluptate id cupidatat nostrud aliqua mollit dolor ad consectetur quis esse nulla id ad.',
    compiledReference: null,
    adviceText:
      "This doesn't look like your style of writing please check you haven't followed source material too closely. We would recommend re-writing this section in more of your own words if this is the case.",
    flaggedLocation: [],
    ignored: false,
    timeAccepted: new Date(),
    documentVersion: 335
  },
  {
    type: 'web',
    flaggedText:
      'This is where the flagged text would go and it would make up the string',
    sourceText:
      'This is where the source text would go, with the appropriate parts highlighted',
    compiledReference:
      'Irons, Jemma. (2011) www.wikipedia.com/article/that/is/relevant. Date accessed: 12th June 2019', // in the final version this will be an object
    flaggedLocation: [], // for making it work
    ignored: false,
    timeAccepted: new Date(),
    documentVersion: 335
  },
  {
    type: 'authorship',
    flaggedText:
      'Aute dolor nulla nisi irure veniam reprehenderit cupidatat magna ullamco nulla nulla incididunt deserunt. Culpa magna deserunt occaecat do exercitation non eiusmod adipisicing proident. Mollit tempor ut Lorem laboris ea consequat. Lorem ut deserunt non labore adipisicing exercitation velit do et quis quis. Ad tempor proident fugiat sint consectetur aliquip. Voluptate id cupidatat nostrud aliqua mollit dolor ad consectetur quis esse nulla id ad.',
    compiledReference: null,
    adviceText:
      "This doesn't look like your style of writing please check you haven't followed source material too closely. We would recommend re-writing this section in more of your own words if this is the case.",
    flaggedLocation: [],
    ignored: false,
    timeAccepted: new Date(),
    documentVersion: 335
  },
  {
    type: 'academic',
    flaggedText:
      'This is where the flagged text would go and it would make up the string',
    sourceText:
      'This is where the source text would go, with the appropiate parts highlighted',
    compiledReference:
      'Smith, Jones. (2012) The Oxford Examination Dictionary. Cambridge University Press. pp.1', // in the final version this will be an object
    flaggedLocation: [], // for making it work
    ignored: false,
    timeAccepted: new Date(),
    documentVersion: 335
  }, {
    type: 'academic',
    flaggedText:
      'This is where the flagged text would go and it would make up the string',
    sourceText:
      'This is where the source text would go, with the appropiate parts highlighted',
    compiledReference:
      'Smith, Jones. (2012) The Oxford Examination Dictionary. Cambridge University Press. pp.1', // in the final version this will be an object
    flaggedLocation: [], // for making it work
    ignored: false,
    timeAccepted: new Date(),
    documentVersion: 335
  },
];

class PlagiarismContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displace: '185',
      collapseOpen: false,
      unfiltered: plagiarismFeedback,
      filtered: plagiarismFeedback,
      active: 'all',
      activeIcon: true
    };
  }

  toggleOpen = () => {
    if (this.state.collapseOpen) {
      this.setState({
        collapseOpen: false,
        displace: '185'
      });
    } else {
      this.setState({
        collapseOpen: true,
        displace: '380'
      });
    }
  };
  filterResults = value => {
    this.setState({ active: value });

    const filt = this.state.unfiltered.filter(item => item.type === value);

    const newState = { ...this.state };
    newState.filtered = filt;
    this.setState({
      filtered: filt
    });
    if (value === 'all') {
      this.setState({
        filtered: this.state.unfiltered
      });
    }
  };

  render() {
    const menuHeaders = ['ALL', 'AUTHORSHIP', 'WEB', 'ACADEMIC']
    return (
      <div id="plagiarismPanel">
        <PaneResultMenu 
        active={this.state.active} 
        menuHeaders={menuHeaders} 
        filterResults={this.filterResults}
        />
        <List
          style={{
            height: `calc(100vh - ${this.state.displace + 'px'})`,
            overflow: 'scroll',
            transition: '0.3s',
            marginLeft: 15
          }}
          className="demo-loadmore-list"
          loading={false}
          itemLayout="horizontal"
          loadMore={false}
          dataSource={this.state.filtered}
          renderItem={plagiarismData => (
            <PlagiarismCard plagiarismData={plagiarismData}/>
          )}
        />
        <PlagiarismCollapse toggleOpen={this.toggleOpen}/>
      </div>
    );
  }
}

export default PlagiarismContainer;
