import React, { Component } from 'react';
import { List, Skeleton, Icon, Tabs, Spin, Avatar, Tooltip } from 'antd';
import { Value } from 'slate';
import OverviewMainDocuments from '../../project-overview/OverviewMainDocuments';
import ResourcePreviewPane from './ResourcePreviewPane';
import QuickAddInlineRefCollapse from './collapses/QuickAddInlineRefCollapse';
import { Link } from 'react-router-dom';

// Context
import { UserContext } from '../../providers/UserProvider'

// Fake APIs
import { getNotes } from '../../../demo-data/fake-apis/notes.api'
import { getDraftsWithTitle } from '../../../demo-data/fake-apis/drafts.api'
import { getBibliographiesWithTitle, getBibliography } from '../../../demo-data/fake-apis/bibliographies.api'

const TabPane = Tabs.TabPane;

class ResourcesContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displace: '378',
      collapseOpen: false,
      previewOpen: false,
      previewDocument: false,
      noDocuments: true,
      activeKey: "2"
    };
  }

  componentDidUpdate() {
    if (this.props.project && !this.state.notes && this.context.user) {
      const { user } = this.context;
      getNotes(this.props.project.notes).then(notes => {
        const stateNotes = JSON.stringify(this.state.notes);
        const apiNotes = JSON.stringify(notes);
        getBibliography(this.props.project.bibliography.id).then(bib => {
          if (stateNotes !== apiNotes && this.state.bibliography !== bib) {
            this.setState({
              notes: notes.filter(note => user.documents.notes.indexOf(note.id) !== -1),
              bibliography: bib[0],
              activeKey: "1"
            })
          }
        })
      })
    } else if (!this.props.project && this.state.activeKey !== "2") {
      this.setState({
        activeKey: "2"
      })
    }
    if (!this.state.allDocuments && this.context.user) {
      const { user } = this.context;
      // Stops the requests from happening again whilst they are being made
      this.setState({ allDocuments: [] });
      getNotes(user.documents.notes).then(notes => {
        getBibliographiesWithTitle(user.documents.bibliographies).then(bibliographies => {
          getDraftsWithTitle(user.documents.drafts).then(drafts => {
            // Perhaps shouldn't make the same data fetch? 
            //Some way to compare the data returned from the request for project data?
            const allDocuments = [...drafts, ...notes, ...bibliographies];
            const jsonAllDocuments = JSON.stringify(allDocuments);
            const stateDocuments = JSON.stringify(this.state.allDocuments);
            if (jsonAllDocuments !== stateDocuments) {
              this.setState({
                allDocuments: allDocuments,
                noDocuments: allDocuments.length > 0 ? false : true
              })
            }
          })
        })
      })
    }
  }

  toggleClosePreview = () => {
    this.setState({
      previewOpen: false,
      previewDocument: false,
    })
  }

  // Toggles the opening of the preview window
  toggleOpenPreview = (document) => {
    this.setState({
      previewOpen: true,
      previewDocument: Value.fromJSON(document)
    })
  }

  getDocument = () => {
    return this.state.previewDocument
  }

  toggleOpen = () => {
    if (this.state.collapseOpen) {
      this.setState({
        collapseOpen: false,
        displace: '378'
      });
    } else {
      this.setState({
        collapseOpen: true,
        displace: '540'
      });
    }
  };

  toggleActiveTab() {
    this.setState({
      activeKey: this.state.activeKey === "1" ? "2" : "1"
    })
  }

  renderDocuments(documents, projectStatus) {
    if (projectStatus) {
      return <div style={{ textAlign: 'center' }}>
        <Spin tip="Loading all documents..." />
      </div>
    }
    if (documents && documents.length > 0) {
      return <List
        itemLayout="horizontal"
        dataSource={documents ? documents : ["No Documents found"]}
        style={{ paddingRight: 15 }}
        renderItem={doc => {
          // console.log(doc)
          if (doc.deleted) return <div></div>
          return (
            <List.Item
              style={doc.project_completed ? { opacity: 0.7 } : {}}
              actions={[
                <Tooltip
                  title={
                  <h4 style={{ color: 'white', textAlign: 'center', marginBottom: 3 }}> 
                  Project Completed 
                  </h4>} >
                  {doc.project_completed ?
                    <Icon style={{ marginLeft: 10, cursor: 'default' }} type="check" /> :
                    ""}
                </Tooltip>,
                <Link to={`/writer/${doc.id}/${doc.document.type}`} ><Icon type="edit" /> </Link>
                ,
                <Icon onClick={() => this.toggleOpenPreview(doc.document.value)} type="eye" />
              ]}
            >
              <Skeleton
                title={false}
                loading={!documents}
              >
                <List.Item.Meta
                  avatar={
                    <Avatar
                      size="small"
                      style={{ backgroundColor: 'rgba(0,0,0,0)', color: 'black' }}
                      icon={
                        doc.document.type === "note" ? 'profile' :
                          doc.document.type === "draft" ? "file-text" :
                            "read"}
                    />
                  }
                  title={doc.title}
                />
              </Skeleton>
            </List.Item>
          )
        }
        }
      />
    } else if ((documents && documents.length < 0)) {
      return <p style={{ textAlign: 'center', padding: 10 }}>No documents found. Try creating other notes or projects first</p>
    }
  }

  render() {
    return (
      <div>
        <div style={{ display: this.state.previewOpen ? 'none' : 'block' }}>
          <div style={{ position: 'relative', overflow: 'scroll', padding: '10px 20px 0px 20px', width: '100%' }}>
            <OverviewMainDocuments
              style={{ minHeight: '180px !important' }}
              project={this.props.project}
              openResourcePreview={this.toggleOpenPreview}
            />
            <Tabs
              defaultActiveKey="1"
              activeKey={this.state.activeKey}
              tabBarGutter={40}
              style={{ height: '100%', paddingLeft: 5 }}
              onTabClick={() => this.toggleActiveTab()}
            >
              <TabPane
                tab="PROJECT NOTES"
                key="1"
                disabled={!this.props.project}
                style={{ height: `calc(100vh - ${this.state.displace + 'px'})`, overflowY: 'scroll', transition: '0.3s' }}
              >
                {this.renderDocuments(this.state.notes, !this.props.project)}
              </TabPane>
              <TabPane
                tab="ALL DOCUMENTS"
                key="2"
                style={{ height: `calc(100vh - ${this.state.displace + 'px'})`, overflowY: 'scroll', transition: '0.3s' }}
              >
                {this.renderDocuments(this.state.allDocuments, this.state.noDocuments)}
              </TabPane>
            </Tabs>
          </div>
          <QuickAddInlineRefCollapse
            bibliography={this.state.bibliography ? this.state.bibliography : null}
            toggleOpen={this.toggleOpen} />
        </div>
        <ResourcePreviewPane
          document={this.state.previewDocument}
          style={{ display: this.state.previewOpen ? 'block' : 'none' }}
          back={this.toggleClosePreview}
        />
      </div>
    );
  }
}

ResourcesContainer.contextType = UserContext;

export default ResourcesContainer;
