import React, { Component } from 'react';
import { Row, Col } from 'antd';

class PaneResultMenu extends Component {

   renderMenuItem = (menuItems) => {
      return menuItems.map(item => 
      <Col
        key={item}
        className={this.props.active === item.toLowerCase() ? 'boldSpan' : 'notBoldSpan'}
        // style={{ paddingLeft: 20 }}
        span={6}
        onClick={e => {
          this.props.filterResults(item.toLowerCase());
        }}
      >
        {item.toUpperCase()}
      </Col>)
   }   
   render() {
    return (
        <Row style={{ margin: '5px 30px 15px 0px ', textAlign: 'center', justifyContent: 'space-around' }}>
          {this.renderMenuItem(this.props.menuHeaders)}
        </Row>
    );
  }
}

export default PaneResultMenu;
