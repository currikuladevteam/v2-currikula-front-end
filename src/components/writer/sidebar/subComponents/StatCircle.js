import React, { Component } from "react";


export default class StatCircle extends Component {
  render() {
    const circleStyle = {
        display: "flex",
        justifyContents: "center",
        height: 52,
        width: 52,
        backgroundColor: this.props.color,
        borderRadius: 100,
        border: `4px ${this.props.border} solid`,
        margin: "0 auto",
        alignItems: "center"
      };
    return (
      <div style={{minWidth: 100, marginBottom: 5}}>
        {this.props.header ? <h4 style={{textAlign: 'center'}}>{this.props.header}</h4> : ''}
        <div style={circleStyle}>
          <div style={{ color: this.props.color ? "white" : this.props.backgroundColor, margin: "0 auto" }}>
            <b>
              {this.props.stat}
              {this.props.type ? this.props.type : ""}
            </b>
          </div>
        </div>
      </div>
    );
  }
}
