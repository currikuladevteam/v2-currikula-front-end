import React, { Component } from 'react';
import { Button, Icon, Row, Col, Select } from 'antd';
import StatisticsCollapse from './collapses/StatisticsCollapse';


const Option = Select.Option;

const DEFAULT_NODE = 'paragraph';



class EditContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      nodeKey: [],
      displace: '144',
      editor: this.props.editor
    }
    this.editor = this.props.editor;
  }

  toggleOpen = () => {
    if (this.state.collapseOpen) {
      this.setState({
        collapseOpen: false,
        displace: '144'
      });
    } else {
      this.setState({
        collapseOpen: true,
        displace: '340'
      });
    }
  };


  // Some Logic for highligting the buttons when they are selected 
  // hasMark = type => {
  //   const { value } = this.state
  //   return value.activeMarks.some(mark => mark.type == type)
  // }

  // Same for blocks
  // hasBlock = type => {
  //   const { value } = this.state
  //   return value.blocks.some(node => node.type == type)
  // }

  onMarkClick = (event, type) => {
    event.preventDefault();
    this.props.editor.focus();
    return this.props.editor.toggleMark(type);
  }

  onH1Click = () => {
    const editor = this.props.editor;
    const selection = { ...editor.value.selection }
    selection.isFocused = true;
    const isH1 = editor.value.blocks.some(block => block.type === 'h1');
    editor.select(selection);
    return editor.setBlocks(isH1 ? 'paragraph' : 'h1');
  }

  onH2Click = () => {
    const editor = this.props.editor;
    const selection = { ...editor.value.selection }
    selection.isFocused = true;
    const isH2 = editor.value.blocks.some(block => block.type === 'h2');
    editor.select(selection);
    return editor.setBlocks(isH2 ? 'paragraph' : 'h2');
  }


  // Alignment click handlers
  onRightAlignClick = () => {
    return this.selectParagraph().toggleAlignNode('rightAlign');
  }
  onLeftAlignClick = () => {
    return this.selectParagraph().toggleAlignNode('leftAlign');
  }

  onCenterAlignClick = () => {
    return this.selectParagraph().toggleAlignNode('centerAlign');
  }
  onJustifyAlignClick = () => {
    return this.selectParagraph().toggleAlignNode('justifyAlign');
  }


  // Selects the paragraph of the selection that was initially made
  selectParagraph = () => {
    const editor = this.props.editor;
    const selectionNode = editor.value.blocks.first();
    editor.moveToRangeOfNode(selectionNode)
    return this;
  }


  // Toggling the align Node of paragraphs
  // Best to refactor and make into a package later
  toggleAlignNode = (markToAddAfterToggle) => {
    const { editor, value: { document } } = this.props.editor;
    const alignmentMarks = ['rightAlign', 'centerAlign', 'leftAlign', 'justifyAlign'];
    const marks = editor.value.blocks.first()
    const parent = document.getParent(marks.key)

    // If there is a parent 
    if (alignmentMarks.indexOf(parent.type) !== -1) {
      editor.unwrapBlock(parent.type);
    }

    // Stopping double wraps with left align when it is the default
    if (markToAddAfterToggle !== 'leftAlign') {
      editor.wrapBlock(markToAddAfterToggle);
    }

    // Return the focus to show selection
    return editor.focus();
  }

  // Affect Global Document Styling
  handleFontFamilyChange = (value) => {
    this.props.changeFontFamily(value)
  }
  handleFontSizeChange = (value) => {
    this.props.changeFontSize(value)
  }
  handleLineSpacingChange = (value) => {
    this.props.changeLineHeight(value)
  }
  handleReferencingStyleChange = (value) => {
    this.props.changeReferencingStyle(value)
  }

  printValue() {
    console.log(this.props.editor.value)
    const value = this.props.editor.value
    console.log(JSON.stringify(value.toJSON()))
  }

  renderMarkButton(type, icon) {

    if (!this.props.editor) return;
    const value = this.props.editor.value;
    const isActive = value ? value.activeMarks.some(mark => mark.type === type) : false;
    return (
      <Button
        shape="circle"
        size="large"
        style={buttonStyle}
        onMouseDown={event => this.onMarkClick(event, type)}
      >
        <Icon
          style={{ color: isActive ? 'blue' : '' }}
          type={icon} />
      </Button>
    )
  }

  hasBlock = type => {
    if (!this.props.editor) return;
    // Need to get the value from the editor
    const { value } = this.props.editor ? this.props.editor : this.state.editor;
    return value.blocks.some(node => node.type === type)
  }

  renderBlockButton = (type, icon) => {
    if (!this.props.editor) return;

    let isActive = this.hasBlock(type);
    if (['numbered-list', 'bulleted-list'].includes(type)) {
      // This needs to change to passed Editor value. 
      const { value: { document, blocks } } = this.props.editor;
      if (blocks.size > 0) {
        const parent = document.getParent(blocks.first().key)
        isActive = this.hasBlock('list-item') && parent && parent.type === type
      }
    }
    return (
      <Button
        style={buttonStyle}
        shape="circle"
        onMouseDown={event => this.onClickBlock(event, type)}
      >
        <Icon
          style={{ color: isActive ? '#40a9ff' : '' }}
          type={icon} />
      </Button>
    )
  }

  onClickBlock = (event, type) => {
    event.preventDefault();

    const { editor } = this.props;
    const { value } = editor;
    const { document } = value;

    // Handle everything but list buttons.
    if (type !== 'bulleted-list' && type !== 'numbered-list') {
      const isActive = this.hasBlock(type);
      const isList = this.hasBlock('list-item');

      if (isList) {
        editor
          .setBlocks(isActive ? DEFAULT_NODE : type)
          .unwrapBlock('bulleted-list')
          .unwrapBlock('numbered-list')
      } else {
        editor.setBlocks(isActive ? DEFAULT_NODE : type)
      }
    } else {
      // Handle the extra wrapping required for list buttons.
      const isList = this.hasBlock('list-item');
      const isType = value.blocks.some(block => {
        return !!document.getClosest(block.key, parent => parent.type === type)
      });


      if (isList && isType) {
        editor
          .setBlocks(DEFAULT_NODE)
          .unwrapBlock('bulleted-list')
          .unwrapBlock('numbered-list')
      } else if (isList) {
        editor
          .unwrapBlock(
            type === 'bulleted-list' ? 'numbered-list' : 'bulleted-list'
          )
          .wrapBlock(type)
      } else {
        editor.setBlocks('list-item').wrapBlock(type)
      }
    }
  }

  getWordCount() {
    const { editor } = this.props;
    if (editor) {
      return this.props.editor.value.document
      .getBlocks()
      .reduce((memo, b) => memo + b.text.trim().split(/\s+/).length, 0)
    } else return 0
  }

  render() {
    return (
      <div>
        <div style={{ height: `calc(100vh - ${this.state.displace + 'px'})`, overflow: 'scroll', transition: '0.3s', padding: '10px 20px', letterSpacing: '0.05rem' }}>
          <h3 style={{ marginBottom: 20, textAlign: 'center' }}>DOCUMENT STYLE</h3>
          <Row style={{ marginBottom: 10 }}>
            <Col span={4}>
              <h4>FONT</h4>
            </Col>
            <Col span={20} style={{ textAlign: 'right' }}>
              <Select defaultValue={this.props.font}
                style={{ textAlign: 'right', width: 180 }}
                onChange={this.handleFontFamilyChange}>
                <Option value="times-new-roman">Times New Roman</Option>
                <Option value="arial">Arial</Option>
                <Option value="Courier">Courier</Option>
                <Option value="Open Sans">Open Sans</Option>
              </Select>
            </Col>
          </Row>
          <Row style={{ marginBottom: 10 }}>
            <Col span={8}>
              <h4>TEXT SIZE</h4>
            </Col>
            <Col span={16} style={{ textAlign: 'right' }}>
              <Select defaultValue={this.props.fontSize}
                style={{ textAlign: 'right', width: 180 }}
                onChange={this.handleFontSizeChange}>
                <Option value="0.7rem">Tiny</Option>
                <Option value="0.8rem">Small</Option>
                <Option value="0.9rem">Normal</Option>
                <Option value="1rem">Large</Option>
                <Option value="1.1rem">Extra Large</Option>
              </Select>
            </Col>
          </Row>
          <Row style={{ marginBottom: 20 }}>
            <Col span={8}>
              <h4>LINE SPACING</h4>
            </Col>
            <Col span={16} style={{ textAlign: 'right' }}>
              <Select defaultValue={this.props.lineHeight}
                style={{ textAlign: 'right', width: 70 }}
                onChange={this.handleLineSpacingChange}>
                <Option value="1">1</Option>
                <Option value="1.5">1.5</Option>
                <Option value="2">2</Option>
                <Option value="2.5">2.5</Option>
                <Option value="3">3</Option>
              </Select>
            </Col>
          </Row>
          <h3 onClick={() => this.printValue()} style={{ margin: 10, textAlign: 'center' }}>STYLE</h3>
          <Row style={{ marinBottom: 10 }}>
            <Col span={12}>
              <Row style={{}}>
                {this.renderMarkButton('bold', 'bold')}
                {this.renderMarkButton('italic', 'italic')}
                {this.renderMarkButton('underline', 'underline')}
                {this.renderMarkButton('strikethrough', 'strikethrough')}
              </Row>
            </Col>
            <Col span={12}>
              <Button shape="circle" style={buttonStyle} onClick={this.onH1Click}>H1</Button>
              <Button shape="circle" style={buttonStyle} onClick={this.onH2Click}>H<span style={{ fontSize: 10 }}>2</span></Button>
              {this.renderBlockButton('bulleted-list', 'bars')}
              {this.renderBlockButton('numbered-list', 'ordered-list')}
            </Col>
          </Row>
          <Row style={{ marginBottom: 40, textAlign: 'left' }}>
            <Button shape="circle" size="large" style={buttonStyle} onClick={this.onLeftAlignClick}><Icon type="align-left" /></Button>
            <Button shape="circle" size="large" style={buttonStyle} onClick={this.onCenterAlignClick}><Icon type="align-center" /></Button>
            <Button shape="circle" size="large" style={buttonStyle} onClick={this.onRightAlignClick}><Icon type="align-right" /></Button>
            <Button shape="circle" size="large" style={buttonStyle} onClick={this.onJustifyAlignClick}><Icon type="align-right" /></Button>
          </Row>
          {this.props.referencingStyle ? <div>
            <h3 style={{ textAlign: 'center' }}>REFERENCING</h3>
            <Row style={{ marginBottom: 10, marginTop: 20 }}>
              <Col span={4}>
                <h3>STYLE</h3>
              </Col>
              <Col span={20} style={{ textAlign: 'right' }}>
                <Select defaultValue={this.props.referencingStyle} style={{ textAlign: 'right', width: 180 }} onChange={this.handleReferencingStyleChange}>
                  <Option value="harvard">Harvard</Option>
                  <Option value="mla">MLA</Option>
                  <Option value="apa">APA</Option>
                  <Option disabled value="chicago">Chicago (coming soon)</Option>
                </Select>
              </Col>
            </Row> </div> :
            ''}

        </div>
        <StatisticsCollapse 
          wordCount={this.getWordCount()}
          toggleOpen={this.toggleOpen}
          project={this.props.project}
          />
      </div>
    )
  }
}

const buttonStyle = {
  border: 'none'
}

export default EditContainer

// Function that I've tried using
// Sets the node value, but requires the key of the node, you can change a p to h2 for example
//editor.setNodeByKey(key, { type: type, data: {textAlign: 'left'} });


// Original Way to access the list, its immutable objects they are specific kinds of objects that require specific methods to interact with 
// editor.value.node.some(node => console.log(node));
// editor.value.nodes.get(index)
// editor.value.nodes.first()


// ** Complete Alignment of Document (or more than one paragraph)
  // Todo a complete document alignment you will need to do a determine how many nodes have been selected and then operate based on that.
  // This will also have to select the rest of half selected nodes