import React, { Component } from 'react';
import { Button, Input, Radio, Icon } from 'antd';

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;



class ExportPane extends Component {
    constructor(props) {
        super(props)
        this.state = {
            windowHeight: window.innerHeight
        }
    }
    updateDimensions = () => {
        this.setState({ windowHeight: window.innerHeight });
    }
    componentWillMount() {
        this.updateDimensions();
    }
    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }
    render() {
        const exportPaneStyle = {
            // height: window.innerHeight-47 > 650 ? window.innerHeight -47 : 650,
            height: window.innerHeight - 47,
            overflow: 'scroll',
            letterSpacing: '1px',
            textAlign: 'center',

            // minHeight: 650
        }
        const headerStyle = {
            textAlign: 'left',
            display: 'flex',
            justifyContent: 'space-between',
            marginBottom: 20
        }
        const spanStyle = {
            fontWeight: '300',
        }
        const subTextStyle = {
            fontSize: 10,
            marginTop: 4
        }
        const flexContainerStyle = { 
            height: this.state.windowHeight > 600 ? this.state.windowHeight - 50 : 600,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between'
        }
        return (
            <div style={exportPaneStyle}>

                <div style={flexContainerStyle}>
                    <div style={{ width: '100%', height: 120, marginTop: 20 }}>
                        <h3 style={headerStyle}>Step 1: <span style={spanStyle}>Choose Export Type</span></h3>
                        <RadioGroup defaultValue="a" size="large" >
                            <RadioButton value="a" style={{ fontWeight: 'bold' }}>                    <Icon style={{ marginRight: 10 }} shape="circle" type="file-word" />
                                WORD
                    </RadioButton>
                            <RadioButton value="b" style={{ fontWeight: 'bold' }}>
                                <Icon style={{ marginRight: 10 }} shape="circle" type="file-pdf" />
                                PDF
                    </RadioButton>
                            <RadioButton value="c" style={{ fontWeight: 'bold' }}>
                                <Icon style={{ marginRight: 10 }} shape="circle" type="file-text" />
                                TEXT
                    </RadioButton>
                        </RadioGroup>
                    </div>
                    <div style={{ height: 120 }}>
                        <h3 style={headerStyle}>Step 2: <span style={spanStyle}>Attach Bibliography</span></h3>
                        <RadioGroup defaultValue="a" size="large" >
                            <RadioButton value="a" style={{ fontSize: 10, fontWeight: 'bold' }}>
                                ATTACH BIBLIOGRAPHY
                    </RadioButton>
                            <RadioButton value="b" style={{ fontSize: 10, fontWeight: 'bold' }}>
                                NO BIBLIOGRAPHY
                    </RadioButton>
                        </RadioGroup>
                        <p style={subTextStyle}>This is the one you created in the bibliography builder</p>
                    </div>
                    <div style={{ height: 120 }}>
                        <h3 style={headerStyle}>Step 3: <span style={spanStyle}>Add Title Page</span></h3>
                        <RadioGroup defaultValue="a" size="large" >
                            <RadioButton value="a" style={{ fontSize: 10, fontWeight: 'bold' }}>
                                ATTACH TITLE PAGE
                    </RadioButton>
                            <RadioButton value="b" style={{ fontSize: 10, fontWeight: 'bold' }}>
                                NO TITLE PAGE
                    </RadioButton>
                        </RadioGroup>
                        <p style={subTextStyle}>You can customise your title page in the settings</p>
                    </div>


                    <div style={{ height: 120 }}>
                        <h3 style={headerStyle}>Step 4: <span style={spanStyle}>Name Export</span></h3>
                        <Input size="large" defaultValue="This is an Example Essay Title" />
                    </div>
                    <div style={{ marginBottom: 40 }}>
                        <Button type="primary" block={true} style={{ marginBottom: 5 }} size="large"> EXPORT AND DOWNLOAD</Button>
                        <Button size="large" block={true} > PREVIEW EXPORT</Button>
                    </div>

                </div>
            </div>
        )
    }
}

export default ExportPane
