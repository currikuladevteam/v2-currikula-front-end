import React, { Component } from 'react';
import ErrorSummaryCollapse from './collapses/ErrorSummaryCollapse';
import PaneResultMenu from './subComponents/PaneResultMenu';
import { List } from 'antd';
import ImprovementCard from '../../cards/ImprovementCard';


const improveArray = [
  {
      evaluation_level: "sentence", 
      type: "content", 
      error: "and_checker",
			content_index: 0, 
			path : {}, 
			submitted_text: "full",
			resolved: false, 
			ignored: false, 
			feedback: {
				original_text: "...a home or invest in a business, <b>and</b> the markets are in place so that businesses can get investors to grow their business <b>and</b>  investors can earn...", 
				advice: "Using 'and' multiple times in a sentence is ill advised as this will make sentences drag on, we'd suggest splitting this sentence into two.", 
				priority: 3 
			}, 
			check_parameters: {  
					not_values: [
						{
							paths: {}, 
							problem: "" 
						}
					] 
			} 
  },
  {
    evaluation_level: "paragraph", 
    type: "content", 
    error: "high_sentence_complexity",
    content_index: 0, 
    path : {}, 
    submitted_text: "full",
    resolved: false, 
    ignored: false, 
    feedback: {
      original_text: "<b>Since a good financial system means a good economy it means that interest rates will be lower and with lower interest rates it means that borrowers will borrow more and invest more in business ventures which in turn creates more jobs.</b>", 
      advice: "Long and complex sentences make it difficult for your readers to understand exactly what you are trying to say. We recommend breaking down the ideas in this sentence and seeing if you can express them in a clearer and more concise fashion.", 
      priority: 3
    }, 
    check_parameters: {  
        not_values: [
          {
            paths: {}, 
            problem: "" 
          }
        ] 
    } 
  },
  {
    evaluation_level: "paragraph", 
    type: "argument", 
    error: "passive_voice",
    content_index: 0, 
    path : {}, 
    submitted_text: "full",
    resolved: false, 
    ignored: false, 
    feedback: {
      original_text: "<b>During both crisis the housing market was booming...</b>.", 
      advice: "The passive voice generally does not present a strong argument, you want to use the active voice. For more information on how to use active voice, view this link here.", 
      priority: 3
    }, 
    check_parameters: {  
        not_values: [
          {
            paths: {}, 
            problem: "" 
          }
        ] 
    } 
  },
  {
    evaluation_level: "paragraph", 
    type: "argument", 
    error: "evidence_evaluation",
    content_index: 0, 
    path : {}, 
    submitted_text: "full",
    resolved: false, 
    ignored: false, 
    feedback: {
      original_text: null, 
      advice: "There are low levels of evidence (references or quotes) in this essay, this might be problematic if you are trying to develop an argument. It is also symptomatic of having too much 'personal' touch on the subject. Rather than crafting a well supported argument, these essays are typically reminscent of \"I think this...\" or \"I think that...\" style essays which should not be encouraged as these are personal perspectives rather than informed.", 
      priority: 1
    }, 
    check_parameters: {  
        not_values: [
          {
            paths: {}, 
            problem: "" 
          }
        ] 
    } 
  },
  {
    evaluation_level: "paragraph", 
    type: "referencing", 
    error: "unreferenced_quote",
    content_index: 0, 
    path : {}, 
    submitted_text: "full",
    resolved: false, 
    ignored: false, 
    feedback: {
      original_text: "<b>\"was created to protect investors, maintain fair, orderly, and efficient markets, and facilitate capital formation\"</b>. The Federal Deposit Insurance...", 
      advice: "Unreferenced quotes are technically plagiarism and can be viewed by your academic institution as such. You need to reference this quote.", 
      priority: 1 
    }, 
    check_parameters: {  
        not_values: [
          {
            paths: {}, 
            problem: "" 
          }
        ] 
    } 
  },
  {
    evaluation_level: "paragraph", 
    type: "structure", 
    error: "overly_long_paragraph",
    content_index: 0, 
    path : {}, 
    submitted_text: "full",
    resolved: false, 
    ignored: false, 
    feedback: {
      original_text: "<b>The main cause of the great depression and the recession in 2008 was said to be the government. In the case of the great depression the Federal Reserve contributed to the crisis because the economy was booming when interest rates were low and people were applying for loans, the government decided to suddenly the raise of interest rates which caused a sudden halt in the boom. Whereas, with the recession in 2008 banks were allowing uncreditworthy individuals to apply for mortgages loans that they were unable to pay back. Similarly, both presidents at the time respond to the financial crisis the same way with a federal budget and instead of cutting spending both presidents went on to spend more money. After the increase in federal spending unemployment remained high during both crisis. During Roosevelt’s presidency unemployment was at twenty one percent and during Obama’s term unemployment was eight percent and increased over two percent over time. During the great depression and the recession both presidents were spending a lot of money and it did not help unemployment rates instead unemployment still remained high. When an economy is falling, the government turns to the wealthy for more money and taxes this is what happened during the great depression. This put many Americans in the fiftieth percentile tax bracket meaning the wealthy had to pay more than half of their income. President Obama is trying to tax differently, instead of taxing the wealthy President Obama is raising taxes on items like cigarettes, liquor, plane tickets, and soft drinks. The recession is said to be the next worse period since the great depression. During both crisis the housing market was booming with low interest rates and the banks were lending money to people who were unable to pay back loans, the unemployment rate was high, and taxes were being raised. It was almost as if the past was repeating itself, the 2008 recession was going in the direction of the great depression.</b>", 
      advice: "This paragraph is overly long compared to other paragraphs. It is not a good idea to have a long paragraph surrounded by lots of small paragraphs.", 
      priority: 2
    }, 
    check_parameters: {  
        not_values: [
          {
            paths: {}, 
            problem: "" 
          }
        ] 
    } 
  }
];

class ImproveContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displace: '185',
      collapseOpen: false,
      unfiltered: improveArray,
      filtered: improveArray,
      active: 'all',
      activeIcon: true,
    };
  }

  toggleOpen = () => {
    if (this.state.collapseOpen) {
      this.setState({
        collapseOpen: false,
        displace: '185'
      });
    } else {
      this.setState({
        collapseOpen: true,
        displace: '380'
      });
    }
  };
  filterResults = value => {
    this.setState({ active: value });

    const filt = this.state.unfiltered.filter(item => item.type === value);

    const newState = { ...this.state };
    newState.filtered = filt;
    this.setState({
      filtered: filt
    });
    if (value === 'all') {
      this.setState({
        filtered: this.state.unfiltered
      });
    }
  };

  render() {
    const menuHeaders = ['ALL', 'CONTENT', 'STRUCTURE', 'ARGUMENT']
    return (
      <div>
         <PaneResultMenu 
        active={this.state.active} 
        menuHeaders={menuHeaders} 
        filterResults={this.filterResults}/>


        <List
          style={{ 
            height: `calc(100vh - ${this.state.displace + 'px'})`,
            overflow: 'scroll',
            transition: '0.3s',
            marginLeft: 15
           }}
          loading={false}
          itemLayout="horizontal"
          loadMore={false}
          dataSource={this.state.filtered}
          renderItem={data => (
            // <List.Item
            //   hoverable="true"
            //   className="wider-list-item"
            //   style={{ backgroundColor: backgroundColors[imp.priority], margin: '10px 15px', padding: '10px 5px', borderRadius: 5, }}
            // >
            //   <Skeleton
            //     className="wider-list-item" title={false} loading={false} active >
            //     <Row style={{width: '100%'}}>
            //       <Col span={2} className="vert-center">
            //         <Avatar size="small" style={{ margin: '0 auto', backgroundColor: 'white', color: 'darkgrey' }}> 
            //           {imp.type.split("")[0].toUpperCase()}
            //         </Avatar>
            //       </Col>
            //       <Col span={18}>
            //         < List.Item.Meta description={imp.suggestion} style={{textAlign: 'justify', padding: '0 10px'}}/>
            //       </Col>
            //       <Col span={4} className="vert-center" style={{alignSelf: 'flex-end', textAlign: 'center'}}>
            //       <div>
            //       <Icon type="check-circle" theme="filled" className="whiteIcon hoverGreen" />
            //         <Divider type="vertical"></Divider>
            //         <Icon type="close-circle" theme="filled" className="whiteIcon hoverRed" />
            //       </div>
            //       </Col>
            //     </Row>
            //   </Skeleton>
            // </List.Item>
            <ImprovementCard data={data}/>
          )}
        />
       <ErrorSummaryCollapse toggleOpen={this.toggleOpen} />
      </div>
    );
  }
}

export default ImproveContainer;
