import React from 'react';

// The Nodes imported
import H1Node from '../nodes/H1Node';
import H2Node from '../nodes/H2Node';
import ParagraphNode from '../nodes/ParagraphNode';
import TitleNode from '../nodes/TitleNode';
import RightAlignNode from '../nodes/RightAlignNode';
import CenterAlignNode from '../nodes/CenterAlignNode';
import JustifyAlignNode from '../nodes/JustifyAlignNode';
import PageSpacerNode from '../nodes/PageSpacerNode';

export default function RenderNodes() {
    return (props, editor, next) => {
        const { attributes, children, node } = props;
        switch (node.type) {
            case 'h1':
                return <H1Node {...props} />
            case 'title':
                return <TitleNode {...props} />
            case 'h2':
                return <H2Node {...props} />
            case 'paragraph':
                return <ParagraphNode {...attributes}>{children}</ParagraphNode>
            case 'rightAlign':
                return <RightAlignNode {...props} />
            case 'centerAlign':
                return <CenterAlignNode {...props} />
            case 'justifyAlign':
                return <JustifyAlignNode {...props} />
            case 'bulleted-list':
                return <ul {...attributes}>{children}</ul>
            case 'numbered-list':
                return <ol {...attributes}>{children}</ol>
            case 'list-item':
                return <li {...attributes}>{children}</li>
            case 'spacer':
                return <PageSpacerNode {...attributes}>{children}</PageSpacerNode>
            default:
                return next()
        }
    }
}
