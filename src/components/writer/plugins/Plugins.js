import HotKeyMarks from './HotKeyMarks';
import SelectAll from './SelectAll';
import HotKeyNodes from './HotKeyNodes';
import InlineFeedbackMenu from './InlineFeedbackMenu';


function Plugins() {
    return [
        // Adds corresponding marks marks when using cmd/ctrl + **select key**
        HotKeyMarks({type: 'bold', key: 'b',}),
        HotKeyMarks({type: 'italic', key: 'i',}), 
        HotKeyMarks({type: 'underline', key: 'u',}), 
        HotKeyMarks({type: 'strikethrough', key: 's',}), 

        // Custom selection function selects all but the title
        SelectAll({key: 'a'}),

        // Adding HotKeyShortCuts
        HotKeyNodes({}),

        // Inline Menu
        InlineFeedbackMenu()
    ]
}

export default Plugins;