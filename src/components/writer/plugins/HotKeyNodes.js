export default function HotKeyNodes(options) {

    // Get the block type for a series of auto-markdown shortcut `chars`.
    const getType = chars => {
        switch (chars) {
            case '*':
            case '-':
            case '+':
                return 'list-item'
            case '1.': 
                return 'numbered-list'
            case '#':
                return 'h1'
            case '##':
                return 'h2'
            default:
                return null
        }
    }

    // On space, if it was after an auto-markdown shortcut, convert the current
    // node into the shortcut's corresponding type.
    const onSpace = (event, editor, next) => {
        const { value } = editor;
        const { selection } = value;
        if (selection.isExpanded) return next();

        const { startBlock } = value;
        const { start } = selection;
        const chars = startBlock.text.slice(0, start.offset).replace(/\s*/g, '');
        const type = getType(chars);
        if (!type) return next();
        if (type === 'list-item' && startBlock.type === 'list-item') return next();
        event.preventDefault(); 

        if (type === 'numbered-list') {
            editor.setBlocks('list-item');
        } else {
            editor.setBlocks(type);
        }
        
        if (type === 'list-item') {
            editor.wrapBlock('bulleted-list');
        }

        if (type === 'numbered-list') {
            editor.wrapBlock('numbered-list');
        }

        editor.moveFocusToStartOfNode(startBlock).delete();
    }

    //   On return, if at the end of a node type that should not be extended,
    //   create a new paragraph below it.
    const onEnter = (event, editor, next) => {
        const { value } = editor;
        const { selection } = value;
        const { start, end, isExpanded } = selection;
        if (isExpanded) return next();

        const { startBlock } = value;
        if (start.offset === 0 && startBlock.text.length === 0)
            return onBackspace(event, editor, next);
        if (end.offset !== startBlock.text.length) return next();

        if (
            startBlock.type !== 'h1' &&
            startBlock.type !== 'h2'
        ) {
            return next()
        }

        event.preventDefault()
        editor.splitBlock().setBlocks('paragraph');
    }

    //  On backspace, if at the start of a non-paragraph, convert it back into a
    // paragraph node.
    const onBackspace = (event, editor, next) => {
        const { value } = editor
        const { selection } = value
        if (selection.isExpanded) return next();
        if (selection.start.offset !== 0) return next();

        const { startBlock } = value
        if (startBlock.type === 'paragraph') return next();

        event.preventDefault();
        editor.setBlocks('paragraph');
        if (startBlock.type === 'list-item') {
            editor
            .unwrapBlock('bulleted-list')
            .unwrapBlock('numbered-list');
        }
    }
    return {
        //  On key down, check for our specific key shortcuts.
        onKeyDown: (event, editor, next) => {
            switch (event.key) {
                case ' ':
                    return onSpace(event, editor, next)
                case 'Backspace':
                    return onBackspace(event, editor, next)
                case 'Enter':
                    return onEnter(event, editor, next)
                default:
                    return next();
            }
        }
    }
}