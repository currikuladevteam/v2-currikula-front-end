import { Range } from 'slate';


export default function SelectAll(options) {
    return {
        onKeyDown(event, editor, next) {
            // If there is no CTRL or CMD (mac) key pressed, then return 
            // If the key that is pressed is not the one passed in return, or the key that is pressed is not a modified version (caps lock) then return early
            if ((!event.metaKey && !event.ctrlKey) || (event.key !== options.key && event.key !== options.key.toUpperCase())) {
                return next();
            }

            if (event.key === options.key || event.key === options.key.toUpperCase()) {
                console.log('Reached');
                // Prevent the auto selection of the whole document

                event.preventDefault();

                // AnchorKey -> the start of the selection
                let anchorKey = editor.value.document.nodes.first().key;


                if (editor.value.document.nodes.first().type === "title") {
                    anchorKey = (parseInt(editor.value.document.nodes.first().key) + 2) + '';
                }

                // Accessing the focus keys and offsets
                // The offset is needed because selecting by key only starts the selection at the start of the node rather than at the end
                const { key, offset } = editor.moveFocusToEndOfDocument().value.selection.focus;

                const range = Range.create({
                    anchor: {
                        key: anchorKey,
                    },
                    focus: {
                        key: key,
                        offset: offset
                    },
                })

                // Check to see that there is stuff to select
                // If checks to see that all the things required to make the selection exist
                // If not, returns next
                if (key && offset && anchorKey) {
                    return editor.select(range);
                } else {
                    return next();
                }
                // Selecting the range of the document, minus the title
            } else {
                return next()
            }
        },
    }
}