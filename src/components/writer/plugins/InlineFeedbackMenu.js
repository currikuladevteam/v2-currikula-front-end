import ReactDOM from 'react-dom';

export default function InlineFeedbackMenu(options) {
      // Setting the menu on the object for access
    const showMenu = (menu) => {
        const inlineMenu = menu;
        if (!inlineMenu) return;
    
        const native = window.getSelection();
        const range = native.getRangeAt(0);
    
        // If there is a selection, that is greater than '' then retrieve the dimensions and spacing
        const rect = range.getBoundingClientRect();
    
        const offsetHeight = ReactDOM.findDOMNode(inlineMenu).offsetHeight;
        const offsetWidth = ReactDOM.findDOMNode(inlineMenu).offsetWidth;
    
        // Pass these through into the CSS setter in the inlineMenu
        inlineMenu.toggleVisibility(true, {
            left: `${rect.left + window.pageXOffset - offsetWidth / 2 + rect.width / 2}px`,
            top: `${Math.floor(rect.top + window.pageYOffset + offsetHeight / 4)}px`
        })
    }

    const hideMenu = (menu) => {
        const inlineMenu = menu;
        inlineMenu.toggleVisibility(false, {});
    }
    const hasMark = (type, editor) => {
        const { value } = editor;
        return value.activeMarks.some(mark => mark.type === type)
      }
    return {
        //  On key down, check for our specific key shortcuts.
        onClick: (event, editor, next) => {
            const menu = editor.props.setInlineMenu;
            const isActive = hasMark('severeError', editor);
            if (isActive) {
                showMenu(menu);
            } else {
                hideMenu(menu);
            }
          }
    }
}