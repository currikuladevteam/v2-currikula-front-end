import { Icon, Button, Divider } from 'antd';
import React from 'react';
import ReactDOM from 'react-dom'
import HoverMenuContainer from './HoverMenuContainer';

const DEFAULT_NODE = 'paragraph';


class HoverMenuPreview extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      value: this.props.value,
      editor: this.props.editor
    }
  }


   renderMarkButton(type, icon) {
    const editor = this.props.editor;
    const isActive = editor ? editor.value.activeMarks.some(mark => mark.type === type) : false;
    return (
      <Button 
        style={{border: 'none', marginLeft: '4px', backgroundColor: '#f5f5f5'}}
        shape="circle"
        onMouseDown={event => this.onClickMark(event, type)}
      >
        <Icon 
          style={{color: isActive ? '#40a9ff' : ''}}
          type={icon}/>
      </Button>
    )
  }

  hasBlock = type => {
    // Need to get the value from the editor
    const { value } = this.props.editor ? this.props.editor : this.state;
    return value.blocks.some(node => node.type === type)
  }

  hasMark = type => {
    const { value } = this.state.editor
    return value.activeMarks.some(mark => mark.type === type)
  }

  renderBlockButton = (type, icon) => {
    let isActive = this.hasBlock(type);
    if (['numbered-list', 'bulleted-list'].includes(type)) {
      // This needs to change to passed Editor value. 
      const { value: { document, blocks } } = this.props.editor ? this.props.editor : this.state
      if (blocks.size > 0) {
        const parent = document.getParent(blocks.first().key)
        isActive = this.hasBlock('list-item') && parent && parent.type === type
      }
    }
    return (
      <Button
        style={{border: 'none', marginLeft: '4px', backgroundColor: '#f5f5f5'}}
        shape="circle"
        onMouseDown={event => this.onClickBlock(event, type)}
      >
        <Icon 
          style={{color: isActive ? '#40a9ff' : '' }}
          type={icon}/>
      </Button>
    )
  }

  onClickBlock = (event, type) => {
    event.preventDefault();

    const { editor } = this.props;
    const { value } = editor;
    const { document } = value;
    
    // Handle everything but list buttons.
    if (type !== 'bulleted-list' && type !== 'numbered-list') {
      const isActive = this.hasBlock(type);
      const isList = this.hasBlock('list-item');

      if (isList) {
        editor
          .setBlocks(isActive ? DEFAULT_NODE : type)
          .unwrapBlock('bulleted-list')
          .unwrapBlock('numbered-list')
      } else {
        editor.setBlocks(isActive ? DEFAULT_NODE : type)
      }
    } else {
      // Handle the extra wrapping required for list buttons.
        const isList = this.hasBlock('list-item');
        const isType = value.blocks.some(block => {
        return !!document.getClosest(block.key, parent => parent.type === type)
      });


      if (isList && isType) {
        editor
          .setBlocks(DEFAULT_NODE)
          .unwrapBlock('bulleted-list')
          .unwrapBlock('numbered-list')
      } else if (isList) {
        editor
          .unwrapBlock(
            type === 'bulleted-list' ? 'numbered-list' : 'bulleted-list'
          )
          .wrapBlock(type)
      } else {
        editor.setBlocks('list-item').wrapBlock(type)
      }
    }
  }


  onClickMark(event, type) {
    console.log(this.props);
    console.log(this);
    const { editor } = this.props;
    event.preventDefault();
    editor.toggleMark(type);
  }
  render() {
    const root = window.document.getElementById('root');
    return ReactDOM.createPortal(
      <HoverMenuContainer ref={this.props.innerRef}>
        {this.renderMarkButton('bold', 'bold')}
        {this.renderMarkButton('italic', 'italic')}
        {this.renderMarkButton('underline', 'underline')}
        {this.renderMarkButton('strikethrough', 'strikethrough')}
        <Divider type="vertical"/>
        {this.renderBlockButton('bulleted-list', 'bars')}
        {this.renderBlockButton('numbered-list', 'ordered-list')}
      </HoverMenuContainer>,
      root
    )

  }
}
export default HoverMenuPreview