import React from 'react';

const menuStyleInvisible = {
  padding: '8px 7px 6px',
  position: 'absolute',
  zIndex: '0',
  top: '-10000px',
  left: '-10000px',
  marginTop: '-8px',
  opacity: '0',
  transition: 'opacity 0.75s'
}

const menuStyleVisible = {
  textAlign: 'center',
  padding: '8px 7px 6px',
  position: 'absolute',
  zIndex: '100',
  marginTop:'-8px',
  borderRadius: '4px',
  transition: 'opacity 0.75s',
  backgroundColor: '#f5f5f5',
  boxShadow: 'rgba(0,0,0,0.5) 1px 2px 5px 0px'
}

class HoverMenuContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      style: menuStyleInvisible,
    }
  }

  toggleVisibility(visible, cssToMerge) {
    if (visible) {
      this.setState({
        style: {
          ...menuStyleVisible, ...cssToMerge
        }
      })
    } else {
      this.setState({
        style: menuStyleInvisible
      })
    }
  }

  render() {
    return (
      <div style={this.state.style} >
        {this.props.children}
      </div>
    )
  }

}
export default HoverMenuContainer