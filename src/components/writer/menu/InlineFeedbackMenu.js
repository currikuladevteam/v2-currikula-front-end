import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Card, List, Icon } from 'antd';

const menuStyleInvisible = {
  position: 'absolute',
  zIndex: '0',
  top: '-10000px',
  left: '-10000px',
  marginTop: '-8px',
  opacity: '0',
  transition: 'opacity 0.75s'
}

const menuStyleVisible = {
  textAlign: 'center',
  position: 'absolute',
  zIndex: '100',
  marginTop: '-8px',
  borderRadius: '10px',
  transition: 'opacity 0.75s',
  backgroundColor: '#f5f5f5',
  boxShadow: 'rgba(0,0,0,0.5) 1px 2px 5px 0px'
}

export class InlineFeedbackMenu extends Component {
  constructor(props) {
    super(props)
    this.state = {
      style: menuStyleInvisible
    }
  }

  handleClick() {

  }
  toggleVisibility(visible, cssToMerge) {
    if (visible) {
      this.setState({
        style: {
          ...menuStyleVisible, ...cssToMerge
        }
      })
    } else {
      this.setState({
        style: menuStyleInvisible
      })
    }
  }
  render() {
    const root = window.document.getElementById('root');
    const data = ['Test string', 'string of test', 'third option'];
    const cardStyle = {borderRadius: 6}
    return ReactDOM.createPortal(
      <div style={this.state.style}>
        <Card
          style={cardStyle}
          size="small"
          bodyStyle={{ padding: '0', minWidth: 250, minHeight: 100, borderTop: '4px red solid', borderRadius: 6 }}
        actions={[<Icon type="delete" />, <Icon type="compass" />]}
        >
          <div style={{padding: 4, fontWeight: 600, letterSpacing: '1.4px'}}>
            ISSUE TYPE
        </div>
          <List
            size="small"
            dataSource={data}
            renderItem={item => (
            <List.Item 
            style={{padding: '8px'}}
            className="feedbackSelection"
            onMouseDown={this.handleClick} >
            {item}
            </List.Item>
            )}
          />
        </Card>
      </div>,
      root
    )
  }
}

export default InlineFeedbackMenu
