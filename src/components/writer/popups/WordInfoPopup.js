import React, { Component } from 'react';
import { Modal, Button, Divider } from 'antd';
import {Range} from 'slate';


class WordInfoPopup extends Component {
  state = {
    visible: false,
    textSelection: 'No Word Selected',
    matches: '0',
    wordList: ['word', 'something', 'else'],
    editor: ''
  };

  showModal = (textSelection, sanitizedText, matches, editor) => {
    this.setState({
      visible: true,
      textSelection,
      matches,
      editor,
      sanitizedText
    });

  };

  // Inserts the selected word into the text at the correct range.
  insertSelectedWord = (selectedWord) => {
    const { editor, textSelection } = this.state;
    let selectedRange = editor.value.selection;
    
    // If the last letter of selection is not a letter, then change the selection to remove that from offset
    if (!textSelection.charAt(textSelection.length-1).match(/[a-z]/)) {
      // Create a new modified range to affect
      selectedRange = Range.create({
        anchor: selectedRange.anchor,
        focus: {
          key: selectedRange.focus.key,
          offset: selectedRange.focus.offset - 1,
          path: selectedRange.focus.path,
        },
        isFocused: false
      })
    }

    const matchedStateWord = this.matchState(this.state.textSelection[0], selectedWord);
    editor.insertTextAtRange(selectedRange, `${matchedStateWord}`);

    this.setState({
      visible: false
    });
    // Deselect the selection (selects the fullstop)
    editor.deselect();
  }

  matchState = (textSelection, selectedWord) => {
    const capital = textSelection.charAt(0);
    if (capital === capital.toUpperCase()) {
      return selectedWord.charAt(0).toUpperCase() + selectedWord.slice(1);
    } else {
      return selectedWord;
    }
  }
  handleOk = e => {
    this.setState({
      visible: false
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false
    });
  };

  render() {

    const words = this.state.wordList.map((word) => {
      return (
        <Button
          key={word}
          onMouseDown={() => this.insertSelectedWord(word)}
          style={{ backgroundColor: 'lightgrey' }}>{word}</Button>
      )
    })

    return (
      <Modal
        title={
          <h4 style={{ marginBottom: 0, textAlign: 'center' }}>
            WORD INFORMATION
          </h4>
        }
        visible={this.state.visible}
        onOk={this.handleOk}
        onCancel={this.handleCancel}
        footer={null}
      >
        <h4 style={{ textAlign: 'center' }}>{this.state.textSelection}</h4>
        <h3>Definition</h3>
        <p>
          Lorem ipsum dolor sit, amet consectetur adipisicing elit. Laudantium
          molestias reprehenderit, inventore ea expedita delectus accusantium
          obcaecati dolores! Non dolorem sapiente neque ducimus ipsum eius, ipsa
          sint quas cum commodi.
        </p>
        <h3 style={{ marginBottom: 20 }}>Thesaurus</h3>
        <div style={{
          display: 'flex',
          justifyContent: 'space-around'
        }}>
          {words}
        </div>
        <Divider />
        <div style={{ textAlign: 'center' }}>
          You have used this word
          <span style={{ fontWeight: 'bold' }}>{` ${this.state.matches} `}</span> times
      in this document
        </div>
      </Modal>
    );
  }
}

export default WordInfoPopup;