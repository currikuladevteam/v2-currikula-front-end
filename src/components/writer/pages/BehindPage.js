import React, { Component } from 'react';

export default class BehindPage extends Component {

  renderPage = (pagesToRender) => {
    const pagesToCreate = Array.from(Array(pagesToRender).keys())
    console.log(pagesToCreate)
    return pagesToCreate.map(page => {
      return (
        <div key={page} style={{
          height: 1200,
          backgroundColor: 'white',
          width: 800,
          margin: '0 auto',
          marginBottom: 50, 
        }}
        />
      )
    }

    )
  }


  render() {
    return (
      <div>
        {this.renderPage(this.props.pagesRequired)}
      </div>

    )
  }
}
