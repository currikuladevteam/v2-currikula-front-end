import React from 'react';


const TitleNode = (props) => {
   return<h1 style={{letterSpacing: '0.1rem', borderBottom: '2px black solid', marginBottom: 30}}>
        {props.children}
    </h1>
}

export default TitleNode;