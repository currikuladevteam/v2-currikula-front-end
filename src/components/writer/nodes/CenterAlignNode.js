import React from 'react';


const CenterAlignNode = (props) => {
    const { children  } = props;
    // const { children, node: { data } } = props;
    // const alignment = data.get('textAlign') || 'left';
    return <div style={{ textAlign: `center` }}>
        {children}
    </div>
}

export default CenterAlignNode;
