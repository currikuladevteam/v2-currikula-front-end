import React from 'react';

const H1Node = ({ children, node: { data } }) => {
    return <h2 style={{ textAlign: `${data.get('textAlign')}` }}>
        {children}
    </h2>
}

export default H1Node;