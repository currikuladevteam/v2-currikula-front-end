import React from 'react';


const RightAlignNode = (props) => {
    // const { children, node: { data } } = props;
    const { children  } = props;
    // const alignment = data.get('textAlign') || 'left';
    return <div style={{ textAlign: `right` }}>
        {children}
    </div>
}

export default RightAlignNode;
