import React from 'react';

const JustifyAlignNode = (props) => {
    // const { children, node: { data } } = props;
    const { children } = props;
    return <div style={{ textAlign: `justify` }}>
        {children}
    </div>
}

export default JustifyAlignNode;
