import React from 'react';
const ParagraphNode = (props) => {
    // const { children, node, node: { data } } = props;
    // const alignment = data.get('textAlign') || 'left';
    // style={{ textAlign: `${alignment}`
    return <p style={{marginBottom: '1rem'}}>
        {props.children}
    </p>
}

export default ParagraphNode;