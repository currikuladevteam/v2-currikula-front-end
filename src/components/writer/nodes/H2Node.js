import React from 'react';


const H2Node = (props) => {
    const { children, node: { data } } = props;
    const alignment = data.get('textAlign') || 'left';
    // console.log(props);
    return <h3 style={{ textAlign: `${alignment}` }}>
        {children}
    </h3>
}

export default H2Node;