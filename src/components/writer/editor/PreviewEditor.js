import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Editor } from 'slate-react';

// Hover Menu
import HoverMenuPreview from '../menu/HoverMenuPreview';


// Importing Marks
import BoldMark from '../marks/BoldMark';
import ItalicMark from '../marks/ItalicMark';
import UnderlineMark from '../marks/UnderlineMark';
import StrikethroughMark from '../marks/StrikethroughMark';


//Importing Nodes
import H1Node from '../nodes/H1Node';
import H2Node from '../nodes/H2Node';
import ParagraphNode from '../nodes/ParagraphNode';
import TitleNode from '../nodes/TitleNode';

class PreviewEditor extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: this.props.editorValue,
      editor: this.props.editor
    }
  }



  // So that when the reference is passed at the correct time.
  // componentDidMount() {
  //   this.props.getEditor(this.editor);
  // }

  // Writer Changes
  onChange = ({ value }) => {
    this.setState({ value })
  }


  // Editor Shortcuts
  onKeyDown = (event, editor, next) => {

    if (!event.ctrlKey && !event.metaKey) {
      return next();
    }
    event.preventDefault();

    switch (event.key) {
      case 'b':
        return editor.toggleMark('bold');
      case 'i':
        return editor.toggleMark('italic');
      case 'u':
        return editor.toggleMark('underline');
      default:
        return next();
    }

  }

  // Adding the 'marks' to the text
  // These are what give us italics and bold 
  renderMark = (props, editor, next) => {
    switch (props.mark.type) {
      case 'bold':
        return <BoldMark {...props} />
      case 'italic':
        return <ItalicMark {...props} />
      case 'underline':
        return <UnderlineMark {...props} />
      case 'strikethrough':
        return <StrikethroughMark {...props} />
      default:
        return next()
    }
  }

  // These are nodes to give the text different behaviour
  renderNode = (props, editor, next) => {
    const { attributes, children, node } = props;
    switch (node.type) {
      case 'h1':
        return <H1Node {...props} />
      case 'title':
        return <TitleNode {...props} />
      case 'h2':
        return <H2Node {...props} />
      case 'paragraph':
        return <ParagraphNode {...props} />
      case 'bulleted-list':
        return <ul {...attributes}>{children}</ul>
      case 'numbered-list':
        return <ol {...attributes}>{children}</ol>
      case 'list-item':
        return <li {...attributes}>{children}</li>
      default:
        return next()
    }
  }

  componentDidMount() {
    // this.props.getEditor(this.editor);
    this.updateMenu();
  }
  componentDidUpdate = () => {
    this.updateMenu()
  }

  // Setting the menu on the object for access
  setMenu = (menu) => {
    this.menu = menu;
  }


  updateMenu = () => {
    const menu = this.menu

    // If the menu hasn't rendered for some reason
    if (!menu) return

    // Assign the relevant values for determining a selection
    const { value } = this.state
    const { fragment, selection } = value

    // Is there a cursor selection live in the editor
    if (selection.isBlurred || selection.isCollapsed || fragment.text === '') {
      menu.toggleVisibility(false, {});
      return
    }

    // If there is a selection, that is greater than '' then retrieve the dimensions and spacing
    const native = window.getSelection();
    const range = native.getRangeAt(0);
    const rect = range.getBoundingClientRect();
    const offsetHeight = ReactDOM.findDOMNode(menu).offsetHeight;
    const offsetWidth = ReactDOM.findDOMNode(menu).offsetWidth;

    // Pass these through into the CSS setter in the menu
    menu.toggleVisibility(true, {
      left: `${rect.left + window.pageXOffset - offsetWidth / 2 + rect.width / 2}px`,
      top: `${Math.floor(rect.top + window.pageYOffset - offsetHeight)}px`
    })
  }
  // Setting the menu on the object for access
  setMenu = (menu) => {
    this.menu = menu;
  }


  updateMenu = () => {
    const menu = this.menu

    // If the menu hasn't rendered for some reason
    if (!menu) return

    // Assign the relevant values for determining a selection
    const { value } = this.state
    const { fragment, selection } = value

    // Is there a cursor selection live in the editor
    if (selection.isBlurred || selection.isCollapsed || fragment.text === '') {
      menu.toggleVisibility(false, {});
      return
    }

    // If there is a selection, that is greater than '' then retrieve the dimensions and spacing
    const native = window.getSelection();
    const range = native.getRangeAt(0);
    const rect = range.getBoundingClientRect();
    const offsetHeight = ReactDOM.findDOMNode(menu).offsetHeight;
    const offsetWidth = ReactDOM.findDOMNode(menu).offsetWidth;

    // Pass these through into the CSS setter in the menu
    menu.toggleVisibility(true, {
      left: `${rect.left + window.pageXOffset - offsetWidth / 2 + rect.width / 2}px`,
      top: `${Math.floor(rect.top + window.pageYOffset - offsetHeight)}px`
    })
  }



  render() {
    return (
      <React.Fragment>
        <HoverMenuPreview 
          innerRef={this.setMenu} 
          editor={this.editor} 
          value={this.state.value} />
        <Editor
          style={{ height: `calc(100vh - 136px)` }}
          // schema={schema}
          ref={editor => this.editor = editor}
          value={this.state.value}
          onChange={this.onChange}
          onKeyDown={this.onKeyDown}
          renderMark={this.renderMark}
          renderNode={this.renderNode}
          autoFocus={true}
      >
      </Editor>

      </React.Fragment>
      
    )
  }
}


// An example schema with enforced title 
// These will need to be created and store separately so that we can manage future iterations of layouts 
//    * Plans
//    * Drafts
//    * Title Pages
//    * Notes
//    * Bibliographies



export default PreviewEditor
