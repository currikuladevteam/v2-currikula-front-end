import React, { Component, Fragment } from 'react'
import { Editor } from 'slate-react'
import ReactDOM from 'react-dom';

//Master Plugin File
import Plugins from '../plugins/Plugins';
// import InlineFeedbackMenuPlugin from '../plugins/InlineFeedbackMenu';

// Popups
import WordInfoPopup from '../popups/WordInfoPopup';


// Render nodes function
import RenderNodes from '../plugins/RenderNodes';


// Importing HoverMenu
import HoverMenu from '../menu/HoverMenu';
import InlineFeedbackMenu from '../menu/InlineFeedbackMenu';

// Importing Marks
import BoldMark from '../marks/BoldMark';
import ItalicMark from '../marks/ItalicMark';
import UnderlineMark from '../marks/UnderlineMark';
import StrikethroughMark from '../marks/StrikethroughMark';
import SevereErrorMark from '../marks/SevereErrorMark';

class CurrikulaEditor extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: this.props.editorValue,
      font: 'Times New Roman',
      lineHeight: '1.5',
      menuVisible: false
    }
  }

  // So that when the reference is passed at the correct time.
  componentDidMount() {
    this.props.getEditor(this.editor);
    this.updateMenu();
  }
  componentDidUpdate = () => {
    this.updateMenu()
  }

  // Writer Changes
  onChange = ({ value }) => {
    this.setState({ value })
  }


  // Editor Shortcuts
  // To be removed after checking that it all works
  // Where replaced by plugins
  onKeyDown = (event, editor, next) => {

    if (!event.ctrlKey && !event.metaKey) {
      return next();
    }
    event.preventDefault();

    switch (event.key) {
      case 'b':
        return editor.toggleMark('bold');
      case 'i':
        return editor.toggleMark('italic');
      case 'u':
        return editor.toggleMark('underline');
      default:
        return next();
    }

  }

  // Adding the 'marks' to the text
  // These are what give us italics and bold 
  renderMark = (props, editor, next) => {
    switch (props.mark.type) {
      case 'bold':
        return <BoldMark {...props} />
      case 'italic':
        return <ItalicMark {...props} />
      case 'underline':
        return <UnderlineMark {...props} />
      case 'strikethrough':
        return <StrikethroughMark {...props} />
      case 'severeError':
        return <SevereErrorMark {...props} />
      default:
        return next()
    }
  }

  // Importing the render Node function
  // Original function just had the function here to begin with
  renderNodes = RenderNodes();

  // Importing the plugins and then assigning them here
  plugins = Plugins();

  // Setting the menu on the object for access
  setMenu = (menu) => {
    this.menu = menu;
  }
  // Setting the menu on the object for access
  setInlineMenu = (menu) => {
    this.inlineMenu = menu;
  }
  // Setting the popup on the object for access
  setPopup = (wordInfoPopUp) => {
    this.wordInfoPopUp = wordInfoPopUp;
  }

  updateMenu = () => {
    const menu = this.menu

    // If the menu hasn't rendered for some reason
    if (!menu) {
      return
    }

    // Assign the relevant values for determining a selection
    const { value } = this.state
    const { fragment, selection } = value

    // Is there a cursor selection live in the editor
    if (selection.isBlurred || selection.isCollapsed || fragment.text === '') {
      menu.toggleVisibility(false, {});
      return
    }

    // If there is a selection, that is greater than '' then retrieve the dimensions and spacing
    const native = window.getSelection();
    const range = native.getRangeAt(0);
    const rect = range.getBoundingClientRect();
    const offsetHeight = ReactDOM.findDOMNode(menu).offsetHeight;
    const offsetWidth = ReactDOM.findDOMNode(menu).offsetWidth;

    // Pass these through into the CSS setter in the menu
    menu.toggleVisibility(true, {
      left: `${rect.left + window.pageXOffset - offsetWidth / 2 + rect.width / 2}px`,
      top: `${Math.floor(rect.top + window.pageYOffset - offsetHeight)}px`
    })
  }

  hasMark = type => {
    const { value } = this.editor
    return value.activeMarks.some(mark => mark.type === type)
  }

  render() {
    return (
      <Fragment>
        <HoverMenu 
          innerRef={this.setMenu} 
          editor={this.editor} 
          value={this.state.value}
          wordInfoPopUp={this.wordInfoPopUp}
        />
        <InlineFeedbackMenu 
          ref={this.setInlineMenu} 
          editor={this.editor} 
          value={this.state.value} 
        />
        <WordInfoPopup 
          ref={this.setPopup}
        />
        <Editor
          style={{
            fontFamily: this.props.font,
            fontSize: this.props.fontSize,
            lineHeight: this.props.lineHeight,
            marginBottom: 100
          }}
          // schema={schema}
          plugins={this.plugins}
          value={this.state.value}
          onChange={this.onChange}
          // onKeyDown={this.onKeyDown} // Covered by plugins for now
          renderMark={this.renderMark}
          renderNode={this.renderNodes}
          autoFocus={true}
          ref={editor => this.editor = editor}
          setInlineMenu={this.inlineMenu}
        >
        </Editor>
      </Fragment>

    )
  }
}


// An example schema with enforced title 
// These will need to be created and store separately so that we can manage future iterations of layouts 
//    * Plans
//    * Drafts
//    * Title Pages
//    * Notes
//    * Bibliographies

// Currently breaking due to errors in the normalisation rules -> Will have to look at again in the future
// const schema = {
//   document: {
//     nodes: [
//       { match: { type: 'title' }, min: 1, max: 1 },
//       { match: { type: 'paragraph' }, min: 0 },
//       { match: { type: 'h1' }, min: 0},
//       { match: { type: 'h2' }, min: 0 },
//     ],
//     blocks: {
//       paragraph: {
//         nodes: [
//           {
//             match: { object: 'text' },
//           },
//         ],
//       },
//     },
//     normalize: (editor, { code, node, child, index }) => {
//       // console.log(code, node, child, index)
//       console.log(code)
//       console.log(node)
//       console.log(child.key)
//       console.log(child.type)
//       console.log(index)

//       // switch (code) {
//       //   case 'child_type_invalid': {
//       //     const type = index === 0 ? 'title' : 'paragraph'
//       //     return editor.setNodeByKey(child.key, type)
//       //   }
//       //   case 'child_min_invalid': {

//       //     if (index === 0) {
//       //       const block =  Block.create('title');
//       //       return editor.insertNodeByKey(node.key, index, block)
//       //     } else {
//       //       const block = Block.create('paragraph')
//       //       return editor.insertNodeByKey(node.key, index, block)
//       //     }
//       //     // const block = Block.create(index === 0 ? 'title' : 'paragraph')

//       //   }
//       //   default: 
//       //     console.log([code, node, child, index])
//       //     return editor.setNodeByKey(child.key, 'paragraph')
//       // }
//       switch (code) {
//         case 'child_object_invalid':
//           editor.wrapBlockByKey(child.key, 'paragraph')
//           return
//         case 'child_type_invalid':
//           editor.setNodeByKey(child.key, 'paragraph')
//           return
//       }
//     },
//   },
// }

// To Add Features
// * Select All 
// * If you select a style and it is not paragraph when you hit enter it resets back to paragraph


export default CurrikulaEditor


// Old code for finding page height, to allow insertion of other pages
// const lastNode = this.editor.value.document;
// const children = ReactDOM.findDOMNode(this.editor).children;
// const lastElement = children[children.length -1]
// const offsetTop = lastElement.offsetTop;
// const height = lastElement.offsetHeight;


// if (offsetTop + height > 1210) {
//   console.log('New Page Needed');
// }


