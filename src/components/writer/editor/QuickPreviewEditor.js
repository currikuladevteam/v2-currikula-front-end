import React, { Component } from "react";
import { Editor } from "slate-react";
import RenderNodes from "../plugins/RenderNodes";

// Hover Menu
// import HoverMenuPreview from '../menu/HoverMenuPreview';

// Importing Marks
import BoldMark from "../marks/BoldMark";
import ItalicMark from "../marks/ItalicMark";
import UnderlineMark from "../marks/UnderlineMark";
import StrikethroughMark from "../marks/StrikethroughMark";

class QuickPreviewEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.editorValue,
      editor: this.props.editor
    };
  }

  // Writer Changes
  onChange = ({ value }) => {
    this.setState({ value });
  };

  // Editor Shortcuts
  onKeyDown = (event, editor, next) => {
    if (!event.ctrlKey && !event.metaKey) {
      return next();
    }
    event.preventDefault();

    switch (event.key) {
      case "b":
        return editor.toggleMark("bold");
      case "i":
        return editor.toggleMark("italic");
      case "u":
        return editor.toggleMark("underline");
      default:
        return next();
    }
  };

  // Adding the 'marks' to the text
  // These are what give us italics and bold
  renderMark = (props, editor, next) => {
    switch (props.mark.type) {
      case "bold":
        return <BoldMark {...props} />;
      case "italic":
        return <ItalicMark {...props} />;
      case "underline":
        return <UnderlineMark {...props} />;
      case "strikethrough":
        return <StrikethroughMark {...props} />;
      default:
        return next();
    }
  };

  // These are nodes to give the text different behaviour
  renderNode = RenderNodes();

  render() {
    return (
      <Editor
          style={{ paddingTop: '20px' }}
        // schema={schema}
        ref={editor => (this.editor = editor)}
        value={this.state.value}
        onChange={this.onChange}
        onKeyDown={this.onKeyDown}
        renderMark={this.renderMark}
        renderNode={this.renderNode}
        autoFocus={true}
        readOnly={true}
      />
    );
  }
}
export default QuickPreviewEditor;
