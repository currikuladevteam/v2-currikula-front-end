import React, { Component } from 'react';

class BaseWriter extends Component {
  constructor(props) {
    super(props)
    this.state = {
      pagesRequired: 1
    }
  }

  setPages = () => {
    this.setState({
      pagesRequired: this.state.pagesRequired + 1,
    });
    console.log(this.state.pagesRequired);
  }

  render() {
    return (
      <div
        style={{ padding: '60px 20px' }}>
        <div style={{
          height: 1200,
          width: 800,
          margin: '0 auto',
          marginBottom: 50,
          padding: 50
        }}>
          {this.props.children}
        </div>
      </div>
    )
  }
}

export default BaseWriter
