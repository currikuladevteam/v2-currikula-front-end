export const MarkLimiter = (props) => {


    // New Fix 
    // If there is an active alignment run the corresponding function to toggle that alignment off first before running the alignment chosen
    // UI fix over Editor 
    const alignmentMarks = ['rightAlign', 'centerAlign', 'leftAlign', 'justifyAlign'];
    if (alignmentMarks.indexOf(props.mark.type) !== -1) {
        console.log('Inside', alignmentMarks.indexOf(props.mark.type))
        props.marks.some(mark => {
            if (mark.type !== props.mark.type && alignmentMarks.indexOf(mark.type) !== -1) {
                console.log('Toggling', mark.type)
                props.editor.toggleMark(mark.type);
            }
        })
    }
}