import React from 'react';
import { MarkLimiter } from './ultils/MarkLimiter';

const LeftAlign = (props) => {
    MarkLimiter(props);
    return <span style={{ display: 'block', textAlign: 'left' }}>
        {props.children}
    </span>
}

export default LeftAlign;