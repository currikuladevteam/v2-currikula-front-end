import React from 'react';
import { MarkLimiter } from '../ultils/MarkLimiter';


const RightAlign = (props) => {
    console.log('Right Align Mark called', props);
    MarkLimiter(props);
    return <span style={{display: 'block', textAlign: 'right' }}>
        {props.children}
    </span>
}

export default RightAlign;