import React from 'react';
import { MarkLimiter } from '../ultils/MarkLimiter';

const JustifyAlign = (props) => {
    MarkLimiter(props);

    return <span style={{display: 'block', textAlign: 'justify' }}>
        {props.children}
    </span>
}

export default JustifyAlign;