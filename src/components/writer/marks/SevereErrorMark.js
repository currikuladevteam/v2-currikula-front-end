import React, { Component } from 'react';


export class SevereErrorMark extends Component {
    constructor(props) {
        super(props)
        this.state = {
            css: {
                borderBottom: 'red solid 1px',
                transition: '0.3s'
            }
        }
    }
    hoverOver = () => {
        this.setState({
            css: {
                borderBottom: 'red solid 2px',
                transition: '1s'
            }
        });
    }

    hoverAway = () => {
        this.setState({
            css: {
                borderBottom: 'red solid 1px',
                transition: '0.3s'
            }
        });
    }
    render() {
        const { props } = this;
        return (
            <span
                style={this.state.css}
                onMouseEnter={this.hoverOver}
                onMouseLeave={this.hoverAway}
            >
                {props.children}
            </span>
        )
    }
}

export default SevereErrorMark;