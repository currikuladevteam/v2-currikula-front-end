import React from 'react';


const StrikethroughMark = (props) => {
   return<span style={{textDecoration: 'line-through'}}>
        {props.children}
    </span>
}

export default StrikethroughMark;