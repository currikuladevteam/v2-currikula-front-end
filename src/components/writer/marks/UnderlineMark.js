import React from 'react';

const UnderlineMark = (props) => {
   return<span {... props} style={{borderBottom: '1px black solid'}}>
        {props.children}
    </span>
}

export default UnderlineMark;